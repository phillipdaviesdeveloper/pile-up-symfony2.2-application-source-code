<?php
// src/PileUp/TwitterBundle/DataFixtures/ORM/LoadTwitterAccounts.php

namespace PileUp\ImageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\TwitterBundle\Entity\TwitterAccount;

/**
 *
 * Twitter Account fixture
 *
 * Generates the authentication keys required for assigning a valid test account.
 *
 * note: The keys set in this fixture may change if the test account is removed and re authenticated at any point.
 *
 * To generate fixtures cd to the application directory in terminal and run: php app/console doctrine:fixtures:load
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class LoadTwitterAccounts extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //make some users
        $account = new TwitterAccount();

        $account->setAccountId($this->getReference('user'));
        $account->setUserId('1466087557');
        $account->setScreenName('PhillipDaviesT');
        $account->setOauthToken('1466087557-RcYxA7B4evc8LmuP5UbIl0kDJ7iGdbDMtVVEi4A');
        $account->setOauthTokenSecret('vD2KR0wdAEfDzzy3dpqUZSzAtW940X1vaKAOhfUDg');
        $account->setAccountAvatar('http://a0.twimg.com/sticky/default_profile_images/default_profile_4_normal.png');

        $manager->persist($account);
        $manager->flush();

        $this->addReference('twitter-account', $account);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

}