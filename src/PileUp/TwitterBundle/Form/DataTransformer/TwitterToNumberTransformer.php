<?php

// src/PileUp/TwitterBundle/Form/DataTransformer/IssueToNumberTransformer.php
namespace PileUp\TwitterBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\TwitterBundle\Entity\TwitterAccount;

class TwitterToNumberTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (twitterAccount) to a string (number).
     *
     * @param  TwitterAccount|null $twitter
     * @return string
     */
    public function transform($twitter)
    {
        if (null === $twitter) {
            return "";
        }

        return $twitter->getId();
    }

    /**
     * Transforms a string (number) to an object (Slot).
     *
     * @param  string $number
     *
     * @return TwitterAccount|null
     *
     * @throws TransformationFailedException if object (slot) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $twitter = $this->om
            ->getRepository('PileUpTwitterBundle:TwitterAccount')
            ->findOneById($number)
        ;

        if (null === $twitter) {
            throw new TransformationFailedException(sprintf(
                'An issue with Slot, id "%s" does not exist!',
                $number
            ));
        }

        return $twitter;
    }
}