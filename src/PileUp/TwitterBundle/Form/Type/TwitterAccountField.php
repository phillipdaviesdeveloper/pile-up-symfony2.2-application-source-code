<?php

namespace PileUp\TwitterBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use PileUp\TwitterBundle\Form\DataTransformer\TwitterToNumberTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Twitter Account hidden field
 *
 * Custom form hidden field which is defined as a service.
 *
 * Used in conjunction with the Twitter to number transformer to take an integer (the slot item id) and
 * convert it to the required entity before any binds / validation takes place. This prevents data typing issues
 * where an entity that the form is linked to expects to be passed an object instance but instead receives an integer.
 *
 * The field does not require a data class to be assigned as all entity linking is handled by the transformer, to this
 * end when using the custom field in a form builder you are required to create the builder method in a similar fashion
 * to:
 *
 *         $builder->add('slot_id', 'slot_selector',
 *               array(
 *               'data'   => $options['slots'],
 *               'data_class' => null
 *               )
 *          );
 *
 *
 * Which includes the entity property, the reference to the custom field and the setting of a null data class. The data
 * property is also set in this case as we are directly assigning the field's value on creation of the form.
 *
 * The custom field is defined in the services.yml file (/PileUp/TwitterBundle/Resources/config/services.yml) and
 * the doctrine entity manager is passed as an argument for the constructor.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class TwitterAccountField extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new TwitterToNumberTransformer($this->om);
        $builder->addModelTransformer($transformer);
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'invalid_message' => 'The selected account does not exist'
        ));
    }

    /**
     *
     * References the parent form builder method
     *
     * Allows you to return the type of field that should be displayed
     *
     */
    public function getParent()
    {
        return 'hidden';
    }

    /**
     *
     * Sets the field name reference
     *
     * Must match the service alias reference for the form builder to be able relate the service to the field
     * when adding the field in the form builder.
     *
     */
    public function getName()
    {
        return 'twitter_selector';
    }

}