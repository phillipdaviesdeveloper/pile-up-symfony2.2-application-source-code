PileUp - Twitter Bundle
========================

The twitter bundle controls all of the account authentication, scheduling and sending of tweets, editing and removal of tweets,
scheduling and sending of retweets along with removal and editing of retweets.

The separation concept was to keep scheduling completely separate from the social network so that in theory other social networks
could be added as a bundle to leverage the schedule bundle. This concept may need to be refactored in future versions as schedule controllers
currently do a lot of logic for schedule views and a better solution needs to be found.

The main service available is the OAuthController which provides functions linked to the tmhOAuth library (/vendor/themattharris/tmhoauth/tmhOAuth.php)
which allows the application to generate signed requests to post to the twitter api.






