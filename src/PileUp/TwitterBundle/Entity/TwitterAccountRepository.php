<?php

namespace PileUp\TwitterBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Twitter Account repository for custom queries
 *
 * Provides additional entity methods to pull filtered results from the persistence layer.
 *
 * Repositories act like the 'model' layer in MVC, in that entities describe structure and implement events / data
 * transformations where repositories provide logic for more specific data requests.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class TwitterAccountRepository extends EntityRepository
{

    /**
     * Finds all twitter accounts assigned to a specific user
     *
     * @param Integer $userId The id of the required user account
     *
     * @return array|Bool The result of the query
     */
    public function findAllFilteredByAccount($userId)
    {

        //set params
        $parameters = array(
            'userId' => $userId
        );

        try
        {
            $qb = $this->createQueryBuilder('account');

            $query = $qb->select('account.id, account.screen_name, account.account_avatar')
                ->where('account.account_id = :userId')
                ->orderBy('account.screen_name', 'DESC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {

            return false;

        }


    }


}
