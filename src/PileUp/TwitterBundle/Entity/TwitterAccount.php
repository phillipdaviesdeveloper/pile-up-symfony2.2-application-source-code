<?php

namespace PileUp\TwitterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="PileUp\TwitterBundle\Entity\TwitterAccountRepository")
 * @ORM\Table(name="app_twitter_account", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="twitter_idx", columns={"user_id", "account_id"})}))
 * @UniqueEntity(fields={"account_id","user_id"}, message="Account already authorised")
 */
class TwitterAccount
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string", length=20, nullable=false)
     *
     */
    protected $user_id;


    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    protected $screen_name;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $oauth_token;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $oauth_token_secret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $account_avatar;

    /**
     * @ORM\ManyToOne(targetEntity="PileUp\AppBundle\Entity\User", inversedBy="twitterAccounts")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     */
    protected $account_id;

    /**
     *
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Schedule", mappedBy="twitter_id", cascade={"remove"})
     */
    protected $twitterOwner;


    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\ScheduleArchive", mappedBy="archive_twitter_id", cascade={"remove"})
     *
     */
    protected $twitterArchiveOwner;


    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Slot", mappedBy="slot_twitter_id", cascade={"remove"})
     *
     */
    protected $slotOwner;

    public function __construct()
    {

        $this->twitterOwner = new ArrayCollection();
        $this->twitterArchiveOwner = new ArrayCollection();
        $this->slotOwner = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param string $userId
     * @return TwitterAccount
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return string 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set screen_name
     *
     * @param string $screenName
     * @return TwitterAccount
     */
    public function setScreenName($screenName)
    {
        $this->screen_name = $screenName;

        return $this;
    }

    /**
     * Get screen_name
     *
     * @return string 
     */
    public function getScreenName()
    {
        return $this->screen_name;
    }

    /**
     * Set oauth_token
     *
     * @param string $oauthToken
     * @return TwitterAccount
     */
    public function setOauthToken($oauthToken)
    {
        $this->oauth_token = $oauthToken;

        return $this;
    }

    /**
     * Get oauth_token
     *
     * @return string 
     */
    public function getOauthToken()
    {
        return $this->oauth_token;
    }

    /**
     * Set oauth_token_secret
     *
     * @param string $oauthTokenSecret
     * @return TwitterAccount
     */
    public function setOauthTokenSecret($oauthTokenSecret)
    {
        $this->oauth_token_secret = $oauthTokenSecret;

        return $this;
    }

    /**
     * Get oauth_token_secret
     *
     * @return string 
     */
    public function getOauthTokenSecret()
    {
        return $this->oauth_token_secret;
    }

    /**
     * Set account_id
     *
     * @param \PileUp\AppBundle\Entity\User $accountId
     * @return TwitterAccount
     */
    public function setAccountId(\PileUp\AppBundle\Entity\User $accountId = null)
    {
        $this->account_id = $accountId;

        return $this;
    }

    /**
     * Get account_id
     *
     * @return \PileUp\AppBundle\Entity\User 
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * Add twitterOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $twitterOwner
     * @return TwitterAccount
     */
    public function addTwitterOwner(\PileUp\ScheduleBundle\Entity\Schedule $twitterOwner)
    {
        $this->twitterOwner[] = $twitterOwner;

        return $this;
    }

    /**
     * Remove twitterOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $twitterOwner
     */
    public function removeTwitterOwner(\PileUp\ScheduleBundle\Entity\Schedule $twitterOwner)
    {
        $this->twitterOwner->removeElement($twitterOwner);
    }

    /**
     * Get twitterOwner
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTwitterOwner()
    {
        return $this->twitterOwner;
    }

    /**
     * Add twitterArchiveOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterArchiveOwner
     * @return TwitterAccount
     */
    public function addTwitterArchiveOwner(\PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterArchiveOwner)
    {
        $this->twitterArchiveOwner[] = $twitterArchiveOwner;

        return $this;
    }

    /**
     * Remove twitterArchiveOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterArchiveOwner
     */
    public function removeTwitterArchiveOwner(\PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterArchiveOwner)
    {
        $this->twitterArchiveOwner->removeElement($twitterArchiveOwner);
    }

    /**
     * Get twitterArchiveOwner
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTwitterArchiveOwner()
    {
        return $this->twitterArchiveOwner;
    }

    /**
     * Set account_avatar
     *
     * @param string $accountAvatar
     * @return TwitterAccount
     */
    public function setAccountAvatar($accountAvatar)
    {
        $this->account_avatar = $accountAvatar;

        return $this;
    }

    /**
     * Get account_avatar
     *
     * @return string 
     */
    public function getAccountAvatar()
    {
        return $this->account_avatar;
    }

    /**
     * Add slotOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Slot $slotOwner
     * @return TwitterAccount
     */
    public function addSlotOwner(\PileUp\ScheduleBundle\Entity\Slot $slotOwner)
    {
        $this->slotOwner[] = $slotOwner;

        return $this;
    }

    /**
     * Remove slotOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Slot $slotOwner
     */
    public function removeSlotOwner(\PileUp\ScheduleBundle\Entity\Slot $slotOwner)
    {
        $this->slotOwner->removeElement($slotOwner);
    }

    /**
     * Get slotOwner
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSlotOwner()
    {
        return $this->slotOwner;
    }
}
