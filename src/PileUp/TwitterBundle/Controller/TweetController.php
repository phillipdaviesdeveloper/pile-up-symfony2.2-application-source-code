<?php

namespace PileUp\TwitterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use PileUp\ScheduleBundle\Entity\ScheduleArchive;
use PileUp\ImageBundle\Entity\TwitterImage;

/**
 * Functionality to schedule, remove or process tweets
 *
 * Sets up and handles schedule form posting.
 *
 * Provides methods to remove, edit or send tweets.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class TweetController extends Controller
{

    /**
     * Stores the posted tweet form data
     * @var array
     */
    private $tweet;

    /**
     * Stores the type of request made
     * @var bool
     */
    private $ajax = false;

    /**
     * Stores the image uploader service
     * @var Object
     */
    private $uploader;

    /**
     * Stores the schedule service
     * @var Object
     */
    private $scheduler;


    /**
     * Adds the tweet to the schedule
     *
     * Handles posts from the schedule forms
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     *
     */
    public function stackTweetAction()
    {

        //set the request type
        $this->ajax = $this->getRequest()->isXmlHttpRequest();

        //create the form and pass the options
        $form = $this->createForm('ScheduleForm', null);

        // if not posted, return an error
        if ($this->getRequest()->getMethod() == 'POST') {

            $form->bind($this->getRequest());

            //get the current user
            $user  = $this->getUser();

            // if form is valid
            if ($form->isValid()) {

                // form is valid so try to process the tweet and add it to the schedule
                try{

                    //get the posted data object
                    $this->tweet = $form->getData();

                    // set the non provided entity information
                    $this->tweet->setAccountId($user);

                    $this->tweet->setTweetDate(new \DateTime($form->get('tweetDate')->getData()));

                    // get the tweet service
                    $this->scheduler = $this->get('my_twitter_tweet');


                    // If a image was uploaded
                    if($this->tweet->getTwitterImage()->getTwitterImage() != null)
                    {
                        // try to upload the image and schedule the the tweet
                        $this->saveTweetWithImage();

                    }
                    else
                    {
                        // try to schedule the text tweet
                        $this->saveTweetWithoutImage();

                    }


                }
                catch(\Exception $e)
                {
                    // display the error message
                    $this->errorTweetAction($e->getMessage(), $form);
                }


                // POST was valid and the tweet has been saved
                return $this->successTweetAction();

            }
            else
            {
                // sets the error message and form
                return $this->errorTweetAction('Sorry, you\'re tweet was not saved, please check your image is under 3MB in size and jpeg, jpg, png or gif format', $form);
            }

        }


        // request was not a post so show the error response and redirect the user
        $message = 'Sorry there was a problem saving your tweet';


        //set the flash message
        $this->get('session')->getFlashBag()->add('twitterScheduleMessage', $message);

        //redirect
        // was a session id avaialble for a twitter account?
        if($this->get('session')->get('accountId'))
            return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
        else
            return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

    }

    /**
     * Functionality to store a tweet with an image in the schedule, called by stackTweetAction()
     *
     * Uses the my_twitter_image_upload service to attempt to upload and store the image.
     *
     * If the image is uploaded successfully then the tweet is passed to be persisted via the schedule service.
     *
     * If the scheduler service cannot save the tweet then the image record and image file are removed from the system.
     *
     * @throws \Exception Throws error and message if upload / saving process fails, caught in the stackTweetAction method
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     *
     */
    private function saveTweetWithImage()
    {

        //get the image upload service
        $this->uploader = $this->get('my_twitter_image_upload');

        // upload the image, will throw an error if there is an upload issue
        $imageId = $this->uploader->uploadAction($this->tweet->getTwitterImage());

        if($imageId != null)
        {

            //set the image relation
            $this->tweet->setTwitterImage($imageId);

            // attempt to schedule the tweet
            if(!$this->scheduler->saveData($this->tweet))
            {

                //if the tweet did not save we should remove the now spare image
                $this->uploader->removeAction($this->tweet->getTwitterImage());

                throw new \Exception('Could not save your tweet');
            }
        }
        else
        {
            throw new \Exception('Problem uploading your image');
        }

    }


    /**
     * Functionality to store a tweet without an image in the schedule, called by stackTweetAction()
     *
     * Attempts to store the tweet in the schedule
     *
     * @throws \Exception Throws error and message if saving process fails, caught in the stackTweetAction method
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     *
     */
    private function saveTweetWithoutImage()
    {

        // set the tweet image to null
        $this->tweet->setTwitterImage(null);

        // attempt to schedule the tweet
        if(!$this->scheduler->saveData($this->tweet))
            throw new \Exception('Could not save your tweet');

    }


    /**
     * Returns the response object on a successful processing
     *
     * Attempts to store the tweet in the schedule
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns either JSON encoded header or redirects the user to the dashboard depending on the request type
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     *
     */
    private function successTweetAction()
    {

        //set the request type
        $message = 'Tweet Scheduled!';

        if($this->ajax)
        {

            // If a image was uploaded
            if($this->tweet->getTwitterImage() != null)
                $img = $this->tweet->getTwitterImage()->getTotalWebPath();
            else
                $img = null;

            $tweet = array(
                'img' => $img,
                'content' => $this->tweet->getTweetContent(),
                'id' => $this->tweet->getId(),
                'slot_id' => $this->tweet->getSlotId()->getId(),
                'retweet' => false
            );

            $response = new Response(json_encode(
                    array(
                        'result' => true,
                        'content' => $this->renderView('PileUpScheduleBundle:Layout:Partials/individualTweet.html.twig',
                            array('tweet' => $tweet, 'ajax' => true))
                    ))
            );

            $response->headers->set('Content-Type', 'application/json');

            return $response;

        }
        else
        {

            //set the flash message
            $this->get('session')->getFlashBag()->add('twitterScheduleMessage', $message);

            //redirect
            // was a session id avaialble for a twitter account?
            if($this->get('session')->get('accountId'))
                return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
            else
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));
        }
    }


    /**
     * Returns the response object when processing is incomplete
     *
     *
     * @param $message String The message to be sent to the user
     * @param $form Object The form view to be generated to display the form errors
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns either JSON encoded header or renders the addTweet form view depending on the request type
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     */
    private function errorTweetAction($message, $form)
    {

        if($this->ajax)
        {

            $response = new Response(json_encode(
                    array(
                        'result' => false,
                        'content' => $message
                    ))
            );

            return $response;

        }

        //show form and the form errors
        return $this->render('PileUpTwitterBundle:Layout:addTweet.html.twig', array('form' => $form->createView()));
    }


    /**
     * Removes the specified tweet from the schedule
     *
     * @param $tweet Integer The ID of the tweet to be removed
     *
     * @throws \Exception If supplied tweet argument is not a number
     *
     * @return Void
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     */
    public function deleteTweetAction($tweet)
    {

        if(is_nan($tweet))
            throw new \Exception('Sorry, incorrect tweet id');

        // call the removal helper function
        if($this->removeTweetAction($tweet))
        {
            $message = "Tweet Removed";
            $result = true;
        }
        else
        {
            $message = "Sorry there was a problem removing your tweet";
            $result = false;
        }

        return $this->deleteTweetResponseAction($result, $message);

    }


    /**
     * Removes the specified tweet from the schedule
     *
     * @param $result Bool The flag to show if the tweet was removed or not
     * @param $message String The message to be passed to the user
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns either JSON encoded header or renders the addTweet form view depending on the request type
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     */
    private function deleteTweetResponseAction($result, $message)
    {
        if($this->getRequest()->isXmlHttpRequest())
        {
            //check if the tweet was deleted on the specific account dashboard
            if($this->get('session')->get('accountId'))
            {
                $response = new Response(json_encode(
                        array(
                            'type' => 'account',
                            'result' => $result,
                            'content' => $message
                        ))
                );

                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
            else
            {
                $response = new Response(json_encode(
                        array(
                            'type' => 'dash',
                            'result' => $result,
                            'content' => $message
                        ))
                );

                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        }
        else
        {

            $this->get('session')->getFlashBag()->add('tweetRemovalMessage', $message);

            // was a session id avaialble for a twitter account?
            if($this->get('session')->get('accountId'))
                return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
            else
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));
        }
    }


    /**
     * Calls the service to remove the tweet
     *
     * @param $data Integer The ID of the tweet to be removed
     *
     * @return Bool The flag to show if the tweet was removed or if an error occoured
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     */
    private function removeTweetAction($data)
    {
        //get the store twitter data service
        $removeTweet = $this->get('my_twitter_tweet');
        $response = $removeTweet->removeData($data);

        return $response;
    }


    /**
     * Attempts to process the selected tweet
     *
     * Calls the sendTweetAction() method which uses the tweet service to attempt to send the tweet.
     *
     * If the tweet is successfully sent then the tweet is archived in the system.
     *
     * @param $tweet Integer The ID of the tweet to send
     *
     * @throws \Exception If supplied tweet argument is not a number
     *
     * @return \Symfony\Component\HttpFoundation\Response Redirects the user to the relevant dashboard
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     */
    public function processTweetAction($tweet)
    {

        if(is_nan($tweet))
            throw new \Exception('Sorry, incorrect tweet id');

        $sendTweet = $this->sendTweetAction($tweet);

        //call the send tweet helper function
        if($sendTweet != null)
        {


            // catch the returned response object from the posted tweet
            $response = json_decode($sendTweet['response']);

            //move tweet to the archive
            $this->archiveTweetAction($tweet, $response->id_str);

            $message = "Tweet Sent";

        }
        else
        {
            $message = "Sorry there was a problem sending your tweet";
        }

        $this->get('session')->getFlashBag()->add('tweetSendMessage', $message);

        // was a session id avaialble for a twitter account?
        if($this->get('session')->get('accountId'))
            return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
        else
            return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

    }


    /**
     * Calls the service to send the tweet
     *
     * @param $data Integer The ID of the tweet to be sent
     *
     * @return Bool The flag to show if the tweet was sent or if an error occurred
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     */
    private function sendTweetAction($data)
    {
        //get the store twitter data service
        $sendTweet = $this->get('my_twitter_tweet');
        $response = $sendTweet->sendData($data);

        return $response;
    }


    /**
     * Calls the service to archive the tweet
     *
     * @param $data Integer The ID of the tweet to be sent
     * @param $tweetId Integer The tweet id on twitter, to be used when retweets are set
     *
     * @return Bool The flag to show if the tweet was archived or if an error occurred
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     */
    private function archiveTweetAction($data, $tweetId)
    {
        //get the store twitter data service
        $archiveTweet = $this->get('my_twitter_tweet');

        //send the archive function the tweet reference and schedulearchive object
        $response = $archiveTweet->archiveData($data, $tweetId, new ScheduleArchive());

        return $response;
    }


    /**
     * Edits the tweet in the schedule
     *
     * Creates the edit tweet form and handles posts from the edit tweet form.
     *
     * Essentially this method provides the same functionality as the stackTweetAction method although the
     * service calls are to Update methods rather than create methods.
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.1
     *
     */
    public function editTweetAction($slot, $tweet)
    {

        // set the request type
        $this->ajax = $this->getRequest()->isXmlHttpRequest();

        //get the store twitter data service
        $tweetService = $this->get('my_twitter_tweet');

        // get the slot service
        $slotService = $this->get('my_slots');

        // get the image service
        $imageService = $this->get('my_twitter_image_upload');

        //get the slot
        $slotData = $slotService->findSlot($slot);



        // get the twitter account object
        $twitterAccount = $this->getDoctrine()
            ->getRepository('PileUpTwitterBundle:TwitterAccount')
            ->findOneById($this->get('session')->get('accountId'));

        // get the tweet object
        $tweetData = $this->getDoctrine()
            ->getRepository('PileUpScheduleBundle:Schedule')
            ->findOneById($tweet);


        // check to see if the image was set, if so save the path
        if($tweetData->getTwitterImage() != null)
        {
            //get the image path
            $imageLoc = new TwitterImage();
            $path = $imageLoc->getPlainPath().$tweetData->getTwitterImage()->getPath().'.'.$tweetData->getTwitterImage()->getType();
        }
        else
        {
            $path = false;
        }


        //set up the form
        $form = $this->createForm('ScheduleForm', null,
            array(
                'accounts' => $twitterAccount,
                'slots' => $slotData,
                'date' => $tweetData->getTweetDate()->format('Y-m-d'),
                'tweetContent' => $tweetData->getTweetContent()
            ));



        //if the form has been posted
        if ($this->getRequest()->getMethod() == 'POST') {

            $form->bind($this->getRequest());

            //get the current user
            $user  = $this->getUser();

            if ($form->isValid()) {

                try{

                    //set success message
                    $message = 'Tweet Edited';

                    //get the posted data object
                    $this->tweet = $form->getData();

                    // set the non provided entity information
                    $this->tweet->setAccountId($user);

                    $this->tweet->setTweetDate(new \DateTime($form->get('tweetDate')->getData()));

                    // If a image was uploaded
                    if($this->tweet->getTwitterImage()->getTwitterImage() != null)
                    {

                        // upload the image, will throw an error if there is an upload issue
                        $imageId = $imageService->uploadAction($this->tweet->getTwitterImage());


                        if($imageId != null)
                        {

                            //set the image relation
                            $this->tweet->setTwitterImage($imageId);

                            // attempt to schedule the tweet
                            if(!$tweetService->updateData($this->tweet, $tweetData))
                            {

                                //if the tweet did not save we should remove the now spare image
                                $tweetService->removeAction($this->tweet->getTwitterImage());

                                throw new \Exception('Sorry, there was a problem editing your tweet');
                            }
                        }
                        else
                        {
                            throw new \Exception('Problem uploading your image');
                        }

                    }
                    else
                    {

                        // if the tweet had an image
                        if($tweetData->getTwitterImage() != null)
                        {
                            $this->tweet->setTwitterImage($tweetData->getTwitterImage());
                        }
                        else
                        {
                            $this->tweet->setTwitterImage(null);
                        }

                        // attempt to schedule the tweet
                        if(!$tweetService->updateData($this->tweet, $tweetData))
                            throw new \Exception('Sorry, there was a problem editing your tweet');
                    }


                }
                catch(\Exception $e)
                {

                    $message = 'Sorry, there was a problem editing your tweet';

                    //set the flash message
                    $this->get('session')->getFlashBag()->add('twitterScheduleMessage', $message);

                    return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));
                }




                //set the flash message
                $this->get('session')->getFlashBag()->add('twitterScheduleMessage', $message);

                //redirect
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));


            }
            else
            {
                //show form and the form errors
                return $this->render('PileUpTwitterBundle:Layout:editTweet.html.twig',
                    array(
                        'form' => $form->createView(),
                        'path' => $path,
                        'ajax' => false,
                        'flashMessages' => null,
                        'slot' => $slot,
                        'tweet' => $tweet
                    ));
            }
        }


        return $this->render('PileUpTwitterBundle:Layout:editTweet.html.twig',
            array(
                'form' => $form->createView(),
                'path' => $path,
                'ajax' => $this->ajax,
                'flashMessages' => null,
                'slot' => $slot,
                'tweet' => $tweet
            ));

    }

}
