<?php

namespace PileUp\TwitterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Twitter account authentication and removal tool
 *
 * Provides functionality to add or remove a twitter account to / from the currently authenticated user account
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class AccountController extends Controller
{

    /**
     * Generates and redirects the user to the twitter authentication page
     *
     * Uses the OAuthController service which in turn uses the tmhOauth vendor library to generate a
     * url with the required callback.
     *
     * The call to the Request action makes the request to provide tokens to link the twitter account and the
     * application.
     *
     * If the request call fails then an exception is thrown and the user is redirected to the dashboard with an
     * error message.
     *
     * If the request is successful then the user is redirected to the twitter authentication screen and specific
     * application / account access tokens are stored in the session to store them over a redirect.
     *
     * @throws \Exception When response returns false
     * @return Void
     */
    public function addAccountAction()
    {

        try
        {
            //get the oAuth Service
            $authTwitter = $this->get('my_twitter_auth');

            //gets the response object from twitter and sets the redirect url once authentication is verified
            // to hit the getAccessAction method in this controller
            $response = $authTwitter->getRequestAction('pile_up_schedule_set_auth');

            //init the session object
            $session = $this->getRequest()->getSession();

            if(!$response)
                throw new \Exception();

            //set auth session before redirecting to the url
            $session->set("authtoken", $response["authtoken"]);
            $session->set("authsecret", $response["authsecret"]);

            //get the url to redirect to auth process
            $url = $authTwitter->getRequestUrlAction($response["response"]["oauth_token"]);

        }
        catch(\Exception $e)
        {
            $message = 'Sorry, there was a problem connecting to the twitter API, please try again later.';

            //setup flash message
            $this->get('session')->getFlashBag()->add('twitterAuthMessage', $message);

            return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
        }

        // redirect the user to Twitter to authorize
        header("Location: " . $url);
        exit;

    }

    /**
     * Called once the user has been redirected from the twitter auth process
     *
     * Calls the oAuthController service to retrieve all of the authentication details.
     *
     * Attempts to call the saveAccessAction method of this class to store the account data.
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the authenticated dashboard
     */
    public function getAccessAction()
    {

        //get the oAuth Service
        $authTwitter = $this->get('my_twitter_auth');
        $response = $authTwitter->getAccessAction();

        $message = 'Sorry we couldn\'t add your account';

        if($response != false)
        {
            if($this->saveAccessAction($response))
            {
               $message = 'Awesome, we have linked your Twitter account to PileUp, get tweeting!';
            }

        }

        //setup flash message
        $this->get('session')->getFlashBag()->add('twitterAuthMessage', $message);

        //redirect to clear get variables and display flash data
        return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
    }

    /**
     * Attempts to persist the account authorisation to the user account
     *
     * Calls the Account service (/PileUp/TwitterBundle/DependencyInjection/Account.php) and
     * persists the authenticated account information to the data layer.
     *
     * @param array $data the response data from the authentication call
     *
     * @return Bool the response from the persistence attempt.
     */
    private function saveAccessAction($data)
    {
        //get the store twitter data service
        $storeTwitter =  $this->get('my_twitter_account');
        $response = $storeTwitter->storeData($data);

        return $response;
    }


    /**
     * Attempts to remove the twitter account from the user
     *
     * Will call the removeAccountAction method in this class, remove the twitter account from the user
     * and cascade delete all related items including scheduled tweets, slots, archive items and retweets.
     *
     * @param integer $account The id of the twitter account to be removed
     *
     * @throws \Exception If account id is not a number
     *
     * @return \Symfony\Component\HttpFoundation\Response Redirects the user to their dashboard
     */
    public function deleteAccountAction($account)
    {

        //TODO-Phill: Add proper error page
        if(is_nan($account))
           throw new \Exception('Incorrect account id format');

        // call the removal helper function
        if($this->removeAccountAction($account))
        {
            $message = "Account Removed";
        }
        else
        {
            $message = "Sorry there was a problem removing your account";
        }

        $this->get('session')->getFlashBag()->add('accountRemovalMessage', $message);

        return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));

    }


    /**
     * Attempts to persist the account authorisation to the user account
     *
     * Calls the Account service (/PileUp/TwitterBundle/DependencyInjection/Account.php) and
     * attempts to remove the account from the user account and cascade delete all relevant data.
     *
     * @param integer $data the id of the account to be removed
     *
     * @return Bool the response from the persistence attempt.
     */
    private function removeAccountAction($data)
    {
        //get the store twitter data service
        $removeTwitter = $this->get('my_twitter_account');
        $response = $removeTwitter->removeData($data);

        return $response;
    }

}
