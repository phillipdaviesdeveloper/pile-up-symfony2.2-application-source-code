<?php

namespace PileUp\TwitterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

use PileUp\ImageBundle\Entity\TwitterImage;

/**
 * Functionality to schedule, remove or process retweets
 *
 * Sets up and handles schedule form posting.
 *
 * Provides methods to remove or send retweets.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class RetweetController extends Controller
{

    /**
     * Stores the slot that should be related to the retweet
     * @var array
     */
    private $slots = array();

    /**
     * Stores the archive item instance that should be retweeted
     * @var array
     */
    private $archive;

    /**
     * Stores the posted retweet form data
     * @var array
     */
    private $tweet;

    /**
     * Stores the type of request made
     * @var bool
     */
    private $ajax = false;


    /**
     * Generates the calendar using the CalendarService (Dependencyinjection/CalendarService.php)
     *
     * Returns a complete calendar for the month / year sent along with controls for pagination and
     * a colour coded display for quick reference.
     *
     * @param integer $slot The slot that is
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the generated calendar view
     */
    public function setUpFormData($slot)
    {

        //hydrate with the available slots
        $this->slots = $this->getDoctrine()
            ->getRepository('PileUpScheduleBundle:Slot')
            ->findOneById($slot);

        //hydrate retweet variable with the tweet data
        $this->archive = $this->getDoctrine()
            ->getRepository('PileUpScheduleBundle:ScheduleArchive')
            ->findAllOppositeFilteredByAccount($this->getUser(), $this->get('session')->get('accountId'));

    }


    /**
     * Generates the retweet form and handles the post submission
     *
     * Generates a single form containing the slot id and slot time, view renders all available archive items
     * with a button which sets the single form's archive id and posts the form.
     *
     * This allows a single form to dynamically select from a preview of all the available archive items.
     *
     * @param integer $slot The slot that is to be filled with the retweet
     *
     * @param $day
     * @param $month
     * @param $year
     * @throws \Exception on supply of bad user credentials
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the generated calendar view
     */
    public function selectRetweetAction($slot, $day, $month, $year)
    {

        // set the request type
        if($this->getRequest()->isXmlHttpRequest())
            $this->ajax = true;


        // if no session id is set redirect to the generic homepage
        if($this->get('session')->get('accountId') == null)
            throw new \Exception('Sorry, incorrect id supplied');


        // if no slot supplied then throw an exception
        if(is_nan($slot))
            throw new \Exception('Sorry, incorrect slot id');


        //setup the form data
        $this->setUpFormData($slot);

        //create the form and pass the options
        $form = $this->createForm('RetweetForm', null,
            array(
                'slots' => $this->slots,
                'date'=> date('Y-m-d', mktime(0,0,0,$month, $day, $year))
            )
        );


        if ($this->getRequest()->getMethod() == 'POST') {

            $message = 'Sorry there was a problem saving your retweet';

            $form->bind($this->getRequest());

            //if the form is not valid
            if ($form->isValid()) {

                try{

                    //set success message
                    $message = 'Retweet Scheduled';

                    //get the posted data object
                    $this->tweet = $form->getData();

                    //get the related twitter account
                    $twitterAccount = $this->getDoctrine()
                        ->getRepository('PileUpTwitterBundle:TwitterAccount')
                        ->findOneById($this->get('session')->get('accountId'));

                    // the id for the archived item is set with javascript by the user
                    $twitterArchive = $this->getDoctrine()
                        ->getRepository('PileUpScheduleBundle:ScheduleArchive')
                        ->findOneById($form->get('retweet_archive_id')->getData());

                    // set the non provided entity information
                    $this->tweet->setAccountId($this->getUser());
                    $this->tweet->setTwitterId($twitterAccount);

                    //set the retweet content from the archive
                    $this->tweet->setTweetContent($twitterArchive->getTweetContent());
                    $this->tweet->setTwitterImage($twitterArchive->getArchiveTwitterImage());

                    //set the date to send
                    $this->tweet->setTweetDate(new \DateTime($form->get('tweetDate')->getData()));


                    // get the tweet service
                    $retweet = $this->get('my_twitter_tweet');


                    //attempt to schedule the tweet
                    if(!$retweet->saveRetweetData($this->tweet))
                        throw new \Exception('Could not save your tweet');

                }
                catch(\Exception $e)
                {
                    //TODO-Phill: need proper error reporting here
                    $message = 'Sorry there was a problem saving your retweet';

                }

            }


            //set the flash message
            $this->get('session')->getFlashBag()->add('twitterScheduleMessage', $message);

            //redirect
            // was a session id available for a twitter account?
            if(!$this->get('session')->get('accountId'))
                return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
            else
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

        }


        //set up the image path
        $path = new TwitterImage();
        $path = $path->getPlainPath();

        $sessionMessages = array();

        foreach ($this->get('session')->getFlashBag()->all() as $type => $messages) {
            foreach ($messages as $message) {
                $sessionMessages[] = $message;
            }
        }

        //if the form was not posted show the view
        return $this->render('PileUpTwitterBundle:Layout:addRetweet.html.twig',
            array(
                'form' => $form->createView(),
                'flashMessages' => $sessionMessages,
                'path' => $path,
                'tweetpreview' => $this->archive,
                'ajax' => $this->ajax
            )
        );



    }







    // Removal actions


    /**
     * Removes the retweet from the schedule
     *
     * Calls the removeRetweet private method
     *
     * @param integer $tweet The id of the tweet in the schedule
     *
     * @throws \Exception If passed tweet id type is invalid
     *
     * @return \Symfony\Component\HttpFoundation\Response Redirects the user to the generic or specific dashboard
     */
    public function deleteRetweetAction($tweet)
    {

        // if tweet supplied is not a number (ID)
        if(is_nan($tweet))
            throw new \Exception('Sorry, incorrect tweet id');

        // call the removal helper function
        if($this->removeRetweetAction($tweet))
        {
            $message = "Tweet Removed";
        }
        else
        {
            $message = "Sorry there was a problem removing your tweet";
        }

        $this->get('session')->getFlashBag()->add('tweetRemovalMessage', $message);

        // was a session id available for a twitter account?
        if(!$this->get('session')->get('accountId'))
            return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
        else
            return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

    }


    /**
     * Calls the tweet service to remove the retweet from the schedule
     *
     * @param integer $data The id of the tweet in the schedule
     *
     * @return bool
     */
    private function removeRetweetAction($data)
    {
        //get the store twitter data service
        $removeTweet = $this->get('my_twitter_tweet');
        $response = $removeTweet->removeRetweetData($data);

        return $response;
    }



    // Processing actions

    /**
     * Sends the specified tweet
     *
     * Attempts to send the tweet via the SendRetweet private method
     *
     * @param integer $tweet The id of the tweet in the schedule
     *
     * @return \Symfony\Component\HttpFoundation\Response Redirects the user to the generic or specific dashboard
     */
    public function processRetweetAction($tweet)
    {

        $sendTweet = $this->sendRetweetAction($tweet);

        //call the send tweet helper function
        if($sendTweet != null)
        {

            //remove scheduled retweet
            $this->removeRetweetAction($tweet);

            $message = "Retweet Sent";

        }
        else
        {
            $message = "Sorry there was a problem sending your retweet";
        }

        $this->get('session')->getFlashBag()->add('tweetSendMessage', $message);

        // was a session id avaialble for a twitter account?
        if(!$this->get('session')->get('accountId'))
            return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
        else
            return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

    }


    /**
     * Calls the tweet service to send the selected tweet
     *
     * If the tweet was sent then the tweet is removed from the schedule, the tweet service
     * checks that the tweet is linked to the user account before sending to prevent
     * erroneous sending of tweets.
     *
     * @param $data Id of the tweet to send
     * @return bool returns the status of the attempted send action
     */
    private function sendRetweetAction($data)
    {
        //get the store twitter data service
        $sendTweet = $this->get('my_twitter_tweet');
        $response = $sendTweet->sendRetweetData($data);

        return $response;
    }


}
