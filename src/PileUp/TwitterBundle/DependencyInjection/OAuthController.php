<?php

namespace PileUp\TwitterBundle\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

//load tmhOAuth vender functionality (vender/themattharris/tmhoauth)
use tmhOAuth;

/**
 * Tools to link to the tmhOAuth vendor library
 *
 * Uses /vendor/themattharris/tmhoauth/tmhOAuth.php to construct requests to the twitter API via OAUTH
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class OAuthController extends Controller
{

    /**
     * Stores the application's private and public key used by the tmhOAuth library
     * @var array
     */
    protected $config = array();

    /**
     * Stores an instance of the tmhOAuth class
     * @var Object The instance of the tmhOAuth class
     */
    protected $authLib;

    /**
     * Stores the returned response from the api call
     * @var Array The container for API responses
     */
    protected $requestRespose = array();

    /**
     * Stores the access call response from the API
     * @var Array The container for the API response
     */
    protected $accessResponse = array();

    /**
     * Stores the user details returned by the API on verification
     * @var Array The container for the API response
     */
    protected $userDetails;

    /**
     * Sets the application keys and stores the initiated tmhOAuth library
     *
     * @param string $privateKey The application private key, set in the services config
     * @param string $publicKey Month The application public key, set in the services config
     *
     * @return \PileUp\TwitterBundle\DependencyInjection\OAuthController
     */
    public function __construct($privateKey, $publicKey)
    {
        $this->config['consumer_key'] = $publicKey;
        $this->config['consumer_secret'] = $privateKey;

        $this->authLib = new tmhOAuth($this->config);
    }

    /**
     * Requests access tokens for the twitter account
     *
     * @param string $route The redirection route to be generated on a successful authentication
     *
     * @return Bool | Array Returns the response information including the access keys for the account or false if the request fails
     */
    public function getRequestAction($route)
    {

        // send request for a request token
        $this->authLib->request("POST", $this->authLib->url("oauth/request_token", ""), array(
            // pass a variable to set the callback
            'oauth_callback'    => $this->generateUrl($route, array(), true)
        ));


        if($this->authLib->response["code"] == 200) {

            // get and store the request token
            $response = $this->authLib->extract_params($this->authLib->response["response"]);

            $this->requestRespose["response"] = $response;
            $this->requestRespose["authtoken"] = $response["oauth_token"];
            $this->requestRespose["authsecret"] = $response["oauth_token_secret"];

            return $this->requestRespose;
        }

        return false;
    }



    /**
     * Requests the authentication url to send the user to
     *
     * @param array $token Access token provided by the getRequestAction method
     *
     * @return String The url to redirect the user to for authentication
     */
    function getRequestUrlAction($token)
    {
        return $this->authLib->url("oauth/authorize", "") . '?oauth_token=' . $token;
    }


    /**
     * Requests authentication information from a authenticated user
     *
     * Takes the session data stored once the user has authenticated and been redirected to the application to
     * access the authentication information to be persisted to the user.
     *
     * @return Bool | Array Returns the access response array if the call was successful or false if there was an error
     */
    public function getAccessAction()
    {
        //checks if the request to authorise was cancelled
        if($this->getRequest()->query->get('denied') != null)
        {
            return false;
        }

        $session = $this->getRequest()->getSession();

        // set the request token and secret we have stored
        $this->authLib->config["user_token"] = $session->get("authtoken");
        $this->authLib->config["user_secret"] = $session->get("authsecret");

        // send request for an access token
        $this->authLib->request("POST", $this->authLib->url("oauth/access_token", ""), array(
            // pass the oauth_verifier received from Twitter
            'oauth_verifier'    => $_GET["oauth_verifier"]
        ));

        if($this->authLib->response["code"] == 200) {

            // get the access token and store it in a cookie
            $response = $this->authLib->extract_params($this->authLib->response["response"]);

            $this->accessResponse['access_token'] = $response["oauth_token"];
            $this->accessResponse['access_token_secret'] = $response["oauth_token_secret"];

            if($this->verifyAccessToken())
            {
                $this->accessResponse['user_details'] = $this->userDetails;
                return $this->accessResponse;
            }

            return false;
        }
        return false;

    }



    /**
     * Verify the validity of our access token
     *
     * @return bool Access token verified
     */
    private function verifyAccessToken() {

        $this->authLib->config["user_token"] = $this->accessResponse['access_token'];
        $this->authLib->config["user_secret"] = $this->accessResponse["access_token_secret"];

        // send verification request to test access key
        $this->authLib->request("GET", $this->authLib->url("1.1/account/verify_credentials"));

        // store the user data returned from the API
        $this->userDetails = json_decode($this->authLib->response["response"]);

        // HTTP 200 means we were successful
        return ($this->authLib->response["code"] == 200);
    }

    /**
     *
     * Check the validity of our access token before doing an action
     *
     * @param array $token Authorisation details
     * @return bool Access token verified
     */
    public function checkAccessToken(array $token)
    {

        $this->authLib->config["user_token"] = $token["user_token"];
        $this->authLib->config["user_secret"] = $token["user_secret"];

        $this->authLib->request("GET", $this->authLib->url("1.1/account/verify_credentials"));

        return ($this->authLib->response["code"] == 200);
    }


    /**
     * Send a text tweet on the user's behalf
     *
     * @param string $tweet Text to tweet
     * @return bool Tweet successfully sent
     */
    public function sendTextTweet($tweet) {

        // limit the string to 140 characters
        $tweet = substr($tweet, 0, 140);

        // POST the text to the statuses/update method
        $this->authLib->request("POST", $this->authLib->url("1.1/statuses/update"), array(
            'status' => $tweet
        ));

        return ($this->authLib->response["code"] == 200);

    }

    /**
     * Send a tweet w/image on the user's behalf
     *
     * @param string $tweet Content to tweet
     * @param array $image Image binary information
     * @return bool Tweet successfully sent
     */
    public function sendImageTweet($tweet, $image) {

        // limit the string to 140 characters
        $tweet = substr($tweet, 0, 140);

        // POST the text and image data to the statuses/update_with_media method
        $this->authLib->request("POST", $this->authLib->url('1.1/statuses/update_with_media'),
            array(
                'status' => $tweet,
                'media[]' => $image,
                'possibly_sensitive' => true
            ),
            true,
            true
        );

        return ($this->authLib->response["code"] == 200);

    }


    /**
     * Send a retweet on the user's behalf
     *
     * @param string $tweet ID of the tweet to be retweeted
     * @return bool Tweet successfully sent
     */
    public function sendRetweet($tweet) {

        // POST the retweet ID
        $this->authLib->request("POST", 'https://api.twitter.com/1.1/statuses/retweet/'.$tweet.'.json');

        return ($this->authLib->response["code"] == 200);

    }

    /**
     * Retrieve the latest response
     *
     * @return Object latest response object
     */
    public function getResponse() {

        return $this->authLib->response;

    }


}
