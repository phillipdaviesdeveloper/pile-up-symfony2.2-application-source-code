<?php

namespace PileUp\TwitterBundle\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use PileUp\TwitterBundle\Entity\TwitterAccount;

/**
 * Provides tools to persist and remove twitter accounts from user accounts
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class Account extends Controller
{

    /**
     * Stores the authenticated twitter account
     *
     * Takes the access details for the twitter account and persists a record for the account that is
     * related to the user.
     *
     * @param array $accountData The twitter account to be assigned to the user
     *
     * @return Bool Returns true if the record was persisted to the data layer, false if an error occoured
     */
    public function storeData(array $accountData)
    {

        try{
            //get the current user id
            $user  = $this->getUser();



            $twitterAccount = new TwitterAccount();
            $twitterAccount->setScreenName($accountData['user_details']->screen_name);
            $twitterAccount->setUserId($accountData['user_details']->id);
            $twitterAccount->setOauthToken($accountData['access_token']);
            $twitterAccount->setOauthTokenSecret($accountData['access_token_secret']);
            $twitterAccount->setAccountAvatar($accountData['user_details']->profile_image_url);

            $twitterAccount->setAccountId($user);

            $em = $this->getDoctrine()->getManager();

            $em->persist($twitterAccount);

            $em->flush();

        }
        catch(\Exception $e)
        {
            return false;
        }


        return true;

    }


    /**
     * Stores the authenticated twitter account
     *
     * Takes the access details for the twitter account and persists a record for the account that is
     * related to the user.
     *
     * @param array $accountData The twitter account to be assigned to the user
     *
     * @throws \Exception When the account could not be found or if the user did not own the account.
     *
     * @return array $calendar Returns the calender as an array
     */
    public function removeData($accountData)
    {

        try{

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $accountData = $em
                ->getRepository('PileUpTwitterBundle:TwitterAccount')
                ->find($accountData);

            //did we (not) find a account for the ID?
            if(!$accountData)
                throw new \Exception('Account not found');


            // If the current user has access to remove the account
            if($this->getUser()->getId() == $accountData->getAccountId()->getId())
            {

                $em->remove($accountData);
                $em->flush();

            }
            else
            {
                throw new \Exception('User permission issue');
            }

        }
        catch(\Exception $e)
        {
            return false;
        }


        return true;

    }

}
