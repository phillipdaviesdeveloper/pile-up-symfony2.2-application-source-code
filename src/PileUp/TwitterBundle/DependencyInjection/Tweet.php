<?php

namespace PileUp\TwitterBundle\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use PileUp\ScheduleBundle\Entity\ScheduleArchive;


/**
 * Provides tools to update, remove, archive and send tweets and retweets
 *
 * Links into the OAuthController for calls to the twitter api, also handles persistence calls
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class Tweet extends Controller
{

    /**
     * Stores tokens for oauth access on the related twitter account
     * @var array
     */
    private $token = array();

    /**
     * Attempts to save the tweet generated in the schedule
     *
     * @param Object $tweet The generated tweet for the schedule
     *
     * @return Bool if the tweet was persisted to the data layer return true, otherwise return false
     */
    public function saveData($tweet)
    {

        //connect to doctrine and update (persist) the new $tweet object in the database
        try
        {

            $em = $this->getDoctrine()->getManager();
            $em->persist($tweet);
            $em->flush();
        }
        catch(\Exception $e)
        {

            return false;
        }

        return true;
    }

    /**
     * Attempts to update the specific tweet in the schedule
     *
     * @param Object $tweet The generated tweet for the schedule
     * @param Object $tweetId The tweet to update in the database
     *
     * @throws \Exception If the user does not own the tweet or if the tweetId type is incorrect an error is thrown
     *
     * @return Bool if the tweet was persisted to the data layer return true, otherwise return false
     */
    public function updateData($tweet, $tweetId)
    {

        //connect to doctrine and update (persist) the new $user object in the database
        try
        {

            $em = $this->getDoctrine()->getManager();
            $tweetData = $em->getRepository('PileUpScheduleBundle:Schedule')->findOneById($tweetId);

            // If the current user has access to send the tweet
            if($this->getUser()->getId() != $tweetData->getAccountId()->getId())
                throw new \Exception('User permission issue');

            $tweetData->setTweetContent($tweet->getTweetContent());
            $tweetData->setTwitterImage($tweet->getTwitterImage());

            $em->flush();
        }
        catch(\Exception $e)
        {
            return false;
        }

        return true;
    }


    /**
     * Attempts to send a tweet via the twitter API
     *
     * Used by the cron job command (/PileUp/ScheduleBundle/Command/TwitterBotCommand.php)
     * and as such performs the same task as the sendData method without the user permission check
     *
     * @param Object $tweetData The tweet to be sent via the API
     *
     * @throws \Exception If the tweet was not found
     *
     * @return Null|Array returns null if the tweet was not sent, otherwise provides the response information from the API
     */
    public function commandSendData($tweetData)
    {
        $em = $this->getDoctrine()->getManager();

        // Find and return the unique object
        $tweetData = $em
            ->getRepository('PileUpScheduleBundle:Schedule')
            ->find($tweetData);

        //did we (not) find a account for the ID?
        if(!$tweetData)
            throw new \Exception('Tweet not found');

        //TODO-Phill: Add error reporting from the api
        return $this->sendDataProcess($tweetData);
    }


    /**
     * Attempts to send a tweet via the twitter API
     *
     * @param Object $tweetData The tweet to be sent via the API
     *
     * @throws \Exception If the tweet was not found or if the user does not have permission to send
     *
     * @return Null|Array returns null if the tweet was not sent, otherwise provides the response information from the API
     */
    public function sendData($tweetData)
    {

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->find($tweetData);


            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Tweet not found');

            // If the current user has access to send the tweet
            if($this->getUser()->getId() != $tweetData->getAccountId()->getId())
                throw new \Exception('User permission issue');

            return $this->sendDataProcess($tweetData);

    }

    /**
     * Called by the sendData and commandSendData methods to interact with the Oauth service
     *
     * @param Object $tweetData The tweet to be sent via the API
     *
     * @throws \Exception If the tweet was not found or if the user does not have permission to send
     *
     * @return Null|Array returns null if the tweet was not sent, otherwise provides the response information from the API
     */
    private function sendDataProcess($tweetData)
    {
        try
        {
            // Set the oauth tokens
            $this->token['user_token'] = $tweetData->getTwitterId()->getOauthToken();
            $this->token['user_secret'] = $tweetData->getTwitterId()->getOauthTokenSecret();

            //get the oauth service
            $tweeter = $this->get('my_twitter_auth');

            //verify the user's access tokens
            if(!$tweeter->checkAccessToken($this->token))
                throw new \Exception('Linked account not verified');


            //check if the tweet contains an image
            if($tweetData->getTwitterImage() != null)
            {

                //set the related image object
                $imageData = $tweetData->getTwitterImage();

                // get the image file from the budle system
                $image = file_get_contents($imageData->getTotalAbsolutePath());

                if(!$tweeter->sendImageTweet($tweetData->getTweetContent(), $image))
                    throw new \Exception('Could not send your tweet');

            }
            else
            {

                //try to send the tweet
                if(!$tweeter->sendTextTweet($tweetData->getTweetContent()))
                    throw new \Exception('Could not send the text tweet');

            }

        }
        catch(\Exception $e)
        {
            //tweet was not sent
            return null;
        }


        //tweet was sent, send the response for further processing
        return $tweeter->getResponse();

    }


    /**
     * Attempts store a sent tweet in the archive
     *
     * Used by the cron job command (/PileUp/ScheduleBundle/Command/TwitterBotCommand.php)
     * and as such performs the same task as the archiveData method without the user permission check
     *
     * @param Object $tweetData The tweet to be archived
     * @param $tweetId Integer The id of the tweet in twitter (used for retweets)
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $archive Instance of the schedule archive entity
     *
     * @throws \Exception If the tweet was not found
     *
     * @return Bool Returns if the tweet was added to the archive successfully or not
     */
    public function commandArchiveData($tweetData, $tweetId, ScheduleArchive $archive)
    {

        try{

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->find($tweetData);

            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Tweet not found');

            // set the entity information
            $archive->setArchiveAccountId($tweetData->getAccountId());
            $archive->setArchiveTwitterId($tweetData->getTwitterId());
            $archive->setTweetContent($tweetData->getTweetContent());

            $tweetTime = $tweetData->getSlotId()->getSlotTime();

            $archive->setTweetTime($tweetTime);
            $archive->setArchiveTwitterImage($tweetData->getTwitterImage());
            $archive->setTweetedId($tweetId);

            // persist the tweet to the archive
            $em->persist($archive);
            $em->flush();

            // delete the sent tweet from the schedule
            $this->commandRemoveData($tweetData->getId());


        }
        catch(\Exception $e)
        {
            //tweet was not archived
            return false;
        }

        return true;
    }

    /**
     * Attempts store a sent tweet in the archive
     *
     * Used by the cron job command (/PileUp/ScheduleBundle/Command/TwitterBotCommand.php)
     * and as such performs the same task as the archiveData method without the user permission check
     *
     * @param Object $tweetData The tweet to be archived
     * @param $tweetId Integer The id of the tweet in twitter (used for retweets)
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $archive Instance of the schedule archive entity
     *
     * @throws \Exception If the tweet was not found or if the user does not have permission to send
     *
     * @return Bool Returns if the tweet was added to the archive successfully or not
     */
    public function archiveData($tweetData, $tweetId, ScheduleArchive $archive)
    {


        try{

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->find($tweetData);

            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Tweet not found');

            // set the entity information
            $archive->setArchiveAccountId($tweetData->getAccountId());
            $archive->setArchiveTwitterId($tweetData->getTwitterId());
            $archive->setTweetContent($tweetData->getTweetContent());

            $tweetTime = $tweetData->getSlotId()->getSlotTime();

            $archive->setTweetTime($tweetTime);
            $archive->setArchiveTwitterImage($tweetData->getTwitterImage());
            $archive->setTweetedId($tweetId);

            // If the current user has access to archive the tweet
            if($this->getUser()->getId() == $tweetData->getAccountId()->getId())
            {

                // persist the tweet to the archive
                $em->persist($archive);
                $em->flush();

                // delete the sent tweet from the schedule
                $this->removeData($tweetData->getId());

            }
            else
            {
                throw new \Exception('User permission issue');
            }

        }
        catch(\Exception $e)
        {
            //tweet was not archived
            return false;
        }

        return true;
    }


    /**
     * Attempts to remove a tweet from the schedule
     *
     * Used by the cron job command (/PileUp/ScheduleBundle/Command/TwitterBotCommand.php)
     * and as such performs the same task as the removeData method without the user permission check
     *
     * @param Integer $tweetData The tweet to be removed
     *
     * @throws \Exception If the tweet was not found or if the passed data is of the wrong type
     *
     * @return Bool Returns if the tweet was removed or not
     */
    public function commandRemoveData($tweetData)
    {
        try{

            if(is_nan($tweetData))
                throw new \Exception('Sorry, incorrect tweet id');


            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->find($tweetData);

            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Tweet not found');


            $em->remove($tweetData);
            $em->flush();


        }
        catch(\Exception $e)
        {
            return false;
        }


        return true;
    }

    /**
     * Attempts to remove a tweet from the schedule
     *
     * @param Integer $tweetData The tweet to be removed
     *
     * @throws \Exception If the tweet was not found, the user does not own the tweet or if the passed data is of the wrong type
     *
     * @return Bool Returns if the tweet was removed or not
     */
    public function removeData($tweetData)
    {

        try{

            if(is_nan($tweetData))
                throw new \Exception('Sorry, incorrect tweet id');

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->find($tweetData);

            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Tweet not found');


            // If the current user has access to remove the account
            if($this->getUser()->getId() == $tweetData->getAccountId()->getId())
            {

                $em->remove($tweetData);
                $em->flush();

            }
            else
            {
                throw new \Exception('User permission issue');
            }

        }
        catch(\Exception $e)
        {
            return false;
        }


        return true;

    }


    //Retweets

    /**
     * Attempts to save the tweet generated in the schedule
     *
     * @param Object $retweetData The generated tweet for the schedule
     *
     * @return Bool if the retweet was persisted to the data layer return true, otherwise return false
     */
    public function saveRetweetData($retweetData)
    {

        //connect to doctrine and update (persist) the new $user object in the database
        try
        {

            $em = $this->getDoctrine()->getManager();
            $em->persist($retweetData);
            $em->flush();
        }
        catch(\Exception $e)
        {
            return false;
        }

        return true;

    }

    /**
     * Attempts to send a retweet via the twitter API
     *
     * Used by the cron job command (/PileUp/ScheduleBundle/Command/TwitterBotCommand.php)
     * and as such performs the same task as the sendRetweetData method without the user permission check
     *
     * @param Object $tweetData The tweet to be sent via the API
     *
     * @throws \Exception If the retweet was not found
     *
     * @return Null|Array returns null if the retweet was not sent, otherwise provides the response information from the API
     */
    public function commandSendRetweetData($tweetData)
    {
        $em = $this->getDoctrine()->getManager();

        // Find and return the unique object
        $tweetData = $em
            ->getRepository('PileUpScheduleBundle:Schedule')
            ->findOneById($tweetData);


        //did we (not) find a account for the ID?
        if(!$tweetData)
            throw new \Exception('Retweet not found');

        return $this->sendRetweetDataProcess($tweetData);
    }


    /**
     * Attempts to send a retweet via the twitter API
     *
     *
     * @param Object $tweetData The tweet to be sent via the API
     *
     * @throws \Exception If the retweet was not found or if the user does not have permission to send the retweet
     *
     * @return Null|Array returns null if the retweet was not sent, otherwise provides the response information from the API
     */
    public function sendRetweetData($tweetData)
    {

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->findOneById($tweetData);


            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Retweet not found');

            // If the current user has access to send the tweet
            if($this->getUser()->getId() != $tweetData->getAccountId()->getId())
                throw new \Exception('User permission issue');

            return $this->sendRetweetDataProcess($tweetData);

    }

    /**
     * Called by the sendRetweetData and commandSendRetweetData methods to interact with the Oauth service
     *
     * @param Object $tweetData The tweet to be sent via the API
     *
     * @throws \Exception If the tweet was not found or if the user does not have permission to send
     *
     * @return Null|Array returns null if the tweet was not sent, otherwise provides the response information from the API
     */
    private function sendRetweetDataProcess($tweetData)
    {
        try
        {

            // Set the oauth tokens
            $this->token['user_token'] = $tweetData->getTwitterId()->getOauthToken();
            $this->token['user_secret'] = $tweetData->getTwitterId()->getOauthTokenSecret();

            //get the oauth service
            $tweeter = $this->get('my_twitter_auth');

            //verify the user's access tokens
            if(!$tweeter->checkAccessToken($this->token))
                throw new \Exception('Linked account not verified');


            //try to send the tweet
            if(!$tweeter->sendRetweet($tweetData->getRetweetArchiveId()->getTweetedId()))
                throw new \Exception('Could not send the retweet');


        }
        catch(\Exception $e)
        {
            //tweet was not sent
            return null;
        }

        return $tweeter->getResponse();

    }


    /**
     * Attempts to remove a retweet from the schedule
     *
     * Used by the cron job command (/PileUp/ScheduleBundle/Command/TwitterBotCommand.php)
     * and as such performs the same task as the removeRetweetData method without the user permission check
     *
     * @param Integer $tweetData The retweet to be removed
     *
     * @throws \Exception If the retweet was not found or if the passed data is of the wrong type
     *
     * @return Bool Returns if the retweet was removed or not
     */
    public function commandRemoveRetweetData($tweetData)
    {

        try{

            if(is_nan($tweetData))
                throw new \Exception('Sorry, incorrect tweet id');

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->findOneById($tweetData);

            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Tweet not found');

            $em->remove($tweetData);
            $em->flush();


        }
        catch(\Exception $e)
        {
            return false;
        }


        return true;

    }

    /**
     * Attempts to remove a retweet from the schedule
     *
     * @param Integer $tweetData The retweet to be removed
     *
     * @throws \Exception If the retweet was not found, the user does not have permission to remove the retweet or if the passed data is of the wrong type
     *
     * @return Bool Returns if the retweet was removed or not
     */
    public function removeRetweetData($tweetData)
    {

        try{

            if(is_nan($tweetData))
                throw new \Exception('Sorry, incorrect tweet id');

            $em = $this->getDoctrine()->getManager();

            // Find and return the unique object
            $tweetData = $em
                ->getRepository('PileUpScheduleBundle:Schedule')
                ->findOneById($tweetData);

            //did we (not) find a account for the ID?
            if(!$tweetData)
                throw new \Exception('Tweet not found');


            // If the current user has access to remove the account
            if($this->getUser()->getId() == $tweetData->getAccountId()->getId())
            {

                $em->remove($tweetData);
                $em->flush();

            }
            else
            {
                throw new \Exception('User permission issue');
            }

        }
        catch(\Exception $e)
        {
            return false;
        }


        return true;

    }

}
