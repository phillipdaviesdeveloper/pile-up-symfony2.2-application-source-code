<?php

namespace PileUp\ScheduleBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ScheduleBundle\Entity\ScheduleArchive;

/**
 * Archive to number transformer
 *
 * Allows a hidden field to be transformed into an entity before the bind action is called in a controller.
 *
 * This allows data typing in entities when creating collections for relations and allows you to pass an id
 * without having to extract the id from an object (a db call usually) or manually transforming the id
 * into an entity before the bind on submission of a form.
 *
 * This transformer is used whenever a related archive item is required as part of a form submission.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class ArchiveToNumberTransformer implements DataTransformerInterface
{
    /**
     *
     * Holds the entity manager passed by the service container (configured in services.yml)
     *
     * @var ObjectManager
     */
    private $om;

    /**
     *
     * On creation of the service passes the entity manager to the class property
     *
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (archive) to a string (number).
     *
     * @param  ScheduleArchive|null $archive
     * @return string
     */
    public function transform($archive)
    {
        if (null === $archive) {
            return "";
        }

        return $archive->getId();
    }

    /**
     * Transforms a string (number) to an object (ScheduleArchive).
     *
     * @param  string $number
     *
     * @return ScheduleArchive|null
     *
     * @throws TransformationFailedException if object (archive) is not found.
     */
    public function reverseTransform($number)
    {

        if (!$number) {
            return null;
        }

        $archive = $this->om
            ->getRepository('PileUpScheduleBundle:ScheduleArchive')
            ->findOneById($number)
        ;

        if (null === $archive) {
            throw new TransformationFailedException(sprintf(
                'An issue with ScheduleArchive, id "%s" does not exist!',
                $number
            ));
        }

        return $archive;
    }
}