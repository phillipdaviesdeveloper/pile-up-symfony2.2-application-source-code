<?php

namespace PileUp\ScheduleBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ScheduleBundle\Entity\Slot;

/**
 * Slot to number transformer
 *
 * Allows a hidden field to be transformed into an entity before the bind action is called in a controller.
 *
 * This allows data typing in entities when creating collections for relations and allows you to pass an id
 * without having to extract the id from an object (a db call usually) or manually transforming the id
 * into an entity before the bind on submission of a form.
 *
 * This transformer is used whenever a related slot item is required as part of a form submission.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class SlotToNumberTransformer implements DataTransformerInterface
{
    /**
     *
     * Holds the entity manager passed by the service container (configured in services.yml)
     *
     * @var ObjectManager
     */
    private $om;

    /**
     *
     * On creation of the service passes the entity manager to the class property
     *
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (slot) to a string (number).
     *
     * @param  Slot|null $slot
     * @return string
     */
    public function transform($slot)
    {
        if (null === $slot) {
            return "";
        }

        return $slot->getId();
    }

    /**
     * Transforms a string (number) to an object (Slot).
     *
     * @param  string $number
     *
     * @return Slot|null
     *
     * @throws TransformationFailedException if object (slot) is not found.
     */
    public function reverseTransform($number)
    {

        if (!$number) {
            return null;
        }

        $slot = $this->om
            ->getRepository('PileUpScheduleBundle:Slot')
            ->findOneById($number)
        ;

        if (null === $slot) {
            throw new TransformationFailedException(sprintf(
                'An issue with Slot, id "%s" does not exist!',
                $number
            ));
        }

        return $slot;
    }
}