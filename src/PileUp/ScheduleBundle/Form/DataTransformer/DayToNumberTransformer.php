<?php

namespace PileUp\ScheduleBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ScheduleBundle\Entity\Day;

/**
 * Day to number transformer
 *
 * Allows a hidden field to be transformed into an entity before the bind action is called in a controller.
 *
 * This allows data typing in entities when creating collections for relations and allows you to pass an id
 * without having to extract the id from an object (a db call usually) or manually transforming the id
 * into an entity before the bind on submission of a form.
 *
 * This transformer is used whenever a related day item is required as part of a form submission.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class DayToNumberTransformer implements DataTransformerInterface
{
    /**
     * Holds the entity manager passed by the service container (configured in services.yml)
     *
     * @var ObjectManager
     */
    private $om;

    /**
     *
     * On creation of the service passes the entity manager to the class property
     *
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (day) to a string (number).
     *
     * @param  Day|null $day
     * @return string
     */
    public function transform($day)
    {
        if (null === $day) {
            return "";
        }

        return $day->getDayCode();
    }

    /**
     * Transforms a string (number) to an object (Day).
     *
     * @param  string $number
     *
     * @return Day|null
     *
     * @throws TransformationFailedException if object (day) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $day = $this->om
            ->getRepository('PileUpScheduleBundle:Day')
            ->findOneBy(array('dayCode' => $number))
        ;

        if (null === $day) {
            throw new TransformationFailedException(sprintf(
                'An issue with Day, id "%s" does not exist!',
                $number
            ));
        }

        return $day;
    }
}