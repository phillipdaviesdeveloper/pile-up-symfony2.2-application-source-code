<?php

namespace PileUp\ScheduleBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Slot form
 *
 * Class used to generate a form for adding slots on either a specific or all days
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class SlotForm extends AbstractType
{

    /**
     * Sets the default options for the form class
     *
     * Defaults include:
     *
     *  - the entity that the form is linked to
     *  - if validation cascades through the included field classes
     *  - setting of accounts holder for data to be passed on form creation
     *  - setting of day that the slot should be assigned, can be overridden in passed options on creation or left as null
     *  - setting of the multi switch, tells the form which fields should be displayed, can be overridden in passed options on creation
     *
     * @param OptionsResolverInterface $resolver Sets the default options for the field, these can be overridden on init but must be defined first
     *
     * @return void
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        //setting defaults, including the custom default for account data
        $resolver->setDefaults(array(
            'data_class' => 'PileUp\ScheduleBundle\Entity\Slot',
            'cascade_validation' => true,
            'accounts' => null,
            'days' => null,
            'multi' => false,
            'intention'  => 'slot-form'
        ));
    }

    /**
     *
     * Generates the form fields using the form builder interface
     *
     * Generates the required fields for the entity persistence, takes passed options and adds the data
     * to the relevant fields.
     *
     * Loads in required field classes such as:
     *
     *  - day_selector (/PileUp/ScheduleBundle/Form/Type/DayField.php) - when option multi is set to false
     *  - twitter_selector (/PileUp/TwitterBundle/Form/Type/TwitterAccountField.php)
     *
     * Generates the 5 minute intervals which can be selected by the user when adding a slot.
     *
     *
     * @param FormBuilderInterface $builder Passes the reference to the parent builder
     * @param array $options Passes in the options set when the field is initiated in the parent builder
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('slot_twitter_id', 'twitter_selector',
            array(
                'data'   => $options['accounts'],
                'data_class' => null
            )
        );

        // if the form has a day selector or not
        if($options['multi'])
        {
            $builder->add('slot_day', 'entity',
                array(
                    'class' => 'PileUpScheduleBundle:Day',
                    'property' => 'dayName'
                )
            );
        }
        else
        {
            $builder->add('slot_day', 'day_selector',
                array(
                    'data'   => $options['days'],
                    'data_class' => null,
                )
            );
        }


        $minutes = array();

        // loop through 60 minutes adding to the minutes array every 5 minutes
        for($x=0; $x < 60; $x += 5)
           $minutes[$x] = $x;

        $builder->add('slotTime', 'time', array(
            'label' => 'When should it send?',
            'input'  => 'datetime',
            'widget' => 'choice',
            'minutes' => $minutes
        ));
    }

    /**
     *
     * Sets the 'name' of the form for reference in the createForm method e.g.
     *
     *       $form = $this->createForm('SlotForm', null,
     *           array(
     *           'accounts' => $this->twitterAccount,
     *           'days' => null,
     *           'multi' => true
     *           )
     *       );
     *
     * @return string Identifier for the form
     */
    public function getName()
    {
        return 'SlotForm';
    }

}