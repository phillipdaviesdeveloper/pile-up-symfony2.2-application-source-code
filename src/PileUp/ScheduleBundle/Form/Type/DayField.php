<?php

namespace PileUp\ScheduleBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use PileUp\ScheduleBundle\Form\DataTransformer\DayToNumberTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Day item hidden field
 *
 * Custom form hidden field which is defined as a service.
 *
 * Used in conjunction with the Day to number transformer to take an integer (the day item id) and
 * convert it to the required entity before any binds / validation takes place. This prevents data typing issues
 * where an entity that the form is linked to expects to be passed an object instance but instead receives an integer.
 *
 * The field does not require a data class to be assigned as all entity linking is handled by the transformer, to this
 * end when using the custom field in a form builder you are required to create the builder method in a similar fashion
 * to:
 *
 *         $builder->add('slot_day', 'day_selector',
 *               array(
 *               'data'   => $options['days'],
 *               'data_class' => null
 *               )
 *          );
 *
 *
 * Which includes the entity property, the reference to the custom field and the setting of a null data class. The data
 * property is also set in this case as we are directly assigning the field's value on creation of the form.
 *
 * The custom field is defined in the services.yml file (/PileUp/ScheduleBundle/Resources/config/services.yml) and
 * the doctrine entity manager is passed as an argument for the constructor.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class DayField extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // create the transformer to turn numbers into entities and visa versa, pass it the OM
        $transformer = new DayToNumberTransformer($this->om);

        // Add the transformer to the field
        $builder->addModelTransformer($transformer);
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // sets the error message if the field is invalid
        $resolver->setDefaults(array(
            'invalid_message' => 'The selected day does not exist'
        ));
    }

    /**
     *
     * References the parent form builder method
     *
     * Allows you to return the type of field that should be displayed
     *
     */
    public function getParent()
    {
        return 'hidden';
    }

    /**
     *
     * Sets the field name reference
     *
     * Must match the service alias reference for the form builder to be able relate the service to the field
     * when adding the field in the form builder.
     *
     */
    public function getName()
    {
        // must match the service reference
        return 'day_selector';
    }

}