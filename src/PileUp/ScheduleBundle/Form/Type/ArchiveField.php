<?php

namespace PileUp\ScheduleBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use PileUp\ScheduleBundle\Form\DataTransformer\ArchiveToNumberTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Archive item hidden field
 *
 * Custom form hidden field which is defined as a service.
 *
 * Used in conjunction with the Archive to number transformer to take an integer (the archive item id) and
 * convert it to the required entity before any binds / validation takes place. This prevents data typing issues
 * where an entity that the form is linked to expects to be passed an object instance but instead receives an integer.
 *
 * The field does not require a data class to be assigned as all entity linking is handled by the transformer, to this
 * end when using the custom field in a form builder you are required to create the builder method in a similar fashion
 * to:
 *
 *         $builder->add('retweet_archive_id', 'archive_selector',
 *               array(
 *               'data_class' => null
 *               )
 *          );
 *
 * Which includes the entity property, the reference to the custom field and the setting of a null data class.
 *
 * The custom field is defined in the services.yml file (/PileUp/ScheduleBundle/Resources/config/services.yml) and
 * the doctrine entity manager is passed as an argument for the constructor.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class ArchiveField extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        // store the objecgt manager for use in the class
        $this->om = $om;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // create the transformer to turn numbers into entities and visa versa, pass it the OM
        $transformer = new ArchiveToNumberTransformer($this->om);

        // add the transformer to the field
        $builder->addModelTransformer($transformer);
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // sets the default options for the field, in this case the error message that should be generated
        $resolver->setDefaults(array(
            'invalid_message' => 'The selected Archive item does not exist',
        ));
    }

    /**
     *
     * References the parent form builder method
     *
     * Allows you to return the type of field that should be displayed
     *
     */
    public function getParent()
    {
        // sets the type of field that is displayed by the form builder
        return 'hidden';
    }

    /**
     *
     * Sets the field name reference
     *
     * Must match the service alias reference for the form builder to be able relate the service to the field
     * when adding the field in the form builder.
     *
     */
    public function getName()
    {
        // must match the service reference
        return 'archive_selector';
    }

}