<?php

namespace PileUp\ScheduleBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Schedule form
 *
 * Class used to generate a form for adding tweets to a specified date and slot
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class ScheduleForm extends AbstractType
{

    /**
     * Sets the default options for the form class
     *
     * Defaults include:
     *
     *  - the entity that the form is linked to
     *  - if validation cascades through the included field classes
     *  - setting of accounts holder for data to be passed on form creation
     *  - setting of slots holder for data to be passed on form creation
     *  - setting of date to the current date, can be overridden in passed options on creation
     *  - setting of the tweet content, can be overridden in passed options on creation
     *
     * @param OptionsResolverInterface $resolver Sets the default options for the field, these can be overridden on init but must be defined first
     *
     * @return void
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        //setting defaults, including the custom default for account data
        $resolver->setDefaults(array(
            'data_class' => 'PileUp\ScheduleBundle\Entity\Schedule',
            'cascade_validation' => true,
            'accounts' => null,
            'slots' => null,
            'date' => date('Y-m-d'),
            'tweetContent' => null,
            'intention'  => 'schedule-form'
        ));

    }

    /**
     *
     * Generates the form field using the form builder interface
     *
     * Generates the required fields for the entity persistence, takes passed options and adds the data
     * to the relevant fields.
     *
     * Loads in required field classes such as:
     *  - image_selector (/PileUp/ImageBundle/Form/Type/imageUploadForm.php),
     *  - twitter_selector (/PileUp/TwitterBundle/Form/Type/TwitterAccountField.php)
     *  - slot_selector (/PileUp/ScheduleBundle/Form/Type/SlotField.php)
     *
     * @param FormBuilderInterface $builder Passes the reference to the parent builder
     * @param array $options Passes in the options set when the field is initiated in the parent builder
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // add the hidden twitter_selector field with attached transformer
        $builder->add('twitter_id', 'twitter_selector',
                array(
                    'data'   => $options['accounts'],
                    'data_class' => null
                )
        );

        // add the hidden date field
        $builder->add('tweetDate', 'hidden', array(
            'data'   => $options['date']
        ));

        // add the tweet content filed with character limitations
        $builder->add('tweetContent', 'textarea', array(
            'label' => 'Your Tweet',
            'max_length' => 140,
            'data' => $options['tweetContent']
        ));

        // add the image_selector field class
        $builder->add('twitterImage',
            'image_selector',
            array(
                'label' => false
            ));

        // adds the hidden slot_selector field class
        $builder->add('slot_id', 'slot_selector',
                array(
                    'data'   => $options['slots'],
                    'data_class' => null
                )
        );

    }

    /**
     *
     * Sets the 'name' of the form for reference in the createForm method e.g.
     *
     *       $this->createForm('ScheduleForm', null,
     *           array(
     *               'accounts' => $this->twitterAccount,
     *               'slots' => $slotData,
     *               'date' => date('Y-m-d', $this->date->getTimestamp())
     *       ))
     *       ->createView();
     *
     * @return string Identifier for the form
     */
    public function getName()
    {
        return 'ScheduleForm';
    }

}