<?php

namespace PileUp\ScheduleBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Retweet form
 *
 * Class used to generate a form for adding retweets to a specified date and slot
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class RetweetForm extends AbstractType
{

    /**
     * Sets the default options for the form class
     *
     * Defaults include:
     *
     *  - the entity that the form is linked to
     *  - if validation cascades through the included field classes
     *  - setting of the slot that the retweet should be assigned to
     *  - setting of slots holder for data to be passed on form creation
     *
     * @param OptionsResolverInterface $resolver Sets the default options for the field, these can be overridden on init but must be defined first
     *
     * @return void
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        //setting defaults, including the custom default for account data
        $resolver->setDefaults(array(
            'data_class' => 'PileUp\ScheduleBundle\Entity\Schedule',
            'cascade_validation' => true,
            'slots' => array(),
            'date' => date('Y-m-d'),
            'intention'  => 'retweet-form'
        ));
    }

    /**
     *
     * Generates the form field using the form builder interface
     *
     * Generates the required fields for the entity persistence, takes passed options and adds the data
     * to the relevant fields.
     *
     * Loads in required field classes such as:
     *  - archive_selector (/PileUp/ScheduleBundle/Form/Type/ArchiveField.php),
     *  - slot_selector (/PileUp/ScheduleBundle/Form/Type/SlotField.php)
     *
     * @param FormBuilderInterface $builder Passes the reference to the parent builder
     * @param array $options Passes in the options set when the field is initiated in the parent builder
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('tweetDate', 'hidden', array(
            'data'   => $options['date']
        ));

        $builder->add('slot_id', 'slot_selector',
            array(
                'data'   => $options['slots'],
                'data_class' => null
            )
        );

        $builder->add('retweet_archive_id', 'archive_selector',
            array(
                'data_class' => null
            )
        );

    }

    /**
     *
     * Sets the 'name' of the form for reference in the createForm method e.g.
     *
     *       $this->createForm('RetweetForm', null,
     *           array(
     *           'slots' => $this->slots,
     *           'date'=> date('Y-m-d', mktime(0,0,0,$month, $day, $year))
     *           )
     *       );
     *
     * @return string Identifier for the form
     */
    public function getName()
    {
        return 'RetweetForm';
    }

}