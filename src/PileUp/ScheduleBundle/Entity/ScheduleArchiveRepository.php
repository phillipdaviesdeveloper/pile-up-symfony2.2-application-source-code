<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Schedule Archive repository for custom queries
 *
 * Provides additional entity methods to pull filtered results from the persistence layer.
 *
 * Repositories act like the 'model' layer in MVC, in that entities describe structure and implement events / data
 * transformations where repositories provide logic for more specific data requests.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class ScheduleArchiveRepository extends EntityRepository
{

    /**
     * Finds all tweets archived by a specific user
     *
     * @param Integer $userId The id of the required user account
     *
     * @return array|Bool The result of the query
     */
    public function findAllFilteredByUser($userId)
    {

        // if account id is not a number
        if(is_nan($userId))
            return false;

        //set params
        $parameters = array(
            'userId' => $userId
        );

        try
        {
            $qb = $this->createQueryBuilder('tweets');

            $query = $qb->select('tweets.id, tweets.tweetContent, tweets.tweetTime, account.screen_name, account.account_avatar')
                ->join('tweets.archive_twitter_id', 'account')
                ->where('tweets.archive_account_id = :userId')
                ->andWhere('tweets.archive_twitter_id = account.id')
                ->orderBy('tweets.tweetTime', 'DESC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->setMaxResults(25)->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {

            return false;
            //TODO-Phill: add in better error reporting
        }


    }

    /**
     * Finds all tweets archived by a specific user and twitter account
     *
     * @param Integer $userId The id of the required user account
     * @param Integer $accountId The id of the required twitter account
     *
     * @return array|Bool The result of the query
     */
    public function findAllFilteredByAccount($userId, $accountId)
    {

        // if account id is not a number
        if(is_nan($accountId) || is_nan($userId))
            return false;

        //set params
        $parameters = array(
            'userId' => $userId,
            'accountId' => $accountId
        );

        try
        {
            $qb = $this->createQueryBuilder('tweets');

            $query = $qb->select('tweets.id, tweets.tweetContent, tweets.tweetTime, account.screen_name, account.account_avatar')
                ->join('tweets.archive_twitter_id', 'account')
                ->where('tweets.archive_account_id = :userId')
                ->andWhere('tweets.archive_twitter_id = account.id')
                ->andWhere('tweets.archive_twitter_id = :accountId')
                ->orderBy('tweets.tweetTime', 'DESC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {

            return false;
            //TODO-Phill: add in better error reporting
        }


    }


    /**
     * Finds all tweets archived by a specific user and not the provided twitter account
     *
     * @param Integer $userId The id of the required user account
     * @param Integer $accountId The id of the required twitter account
     *
     * @return array|Bool The result of the query
     */
    public function findAllOppositeFilteredByAccount($userId, $accountId)
    {

        // if account id is not a number
        if(is_nan($accountId))
            return false;

        //set params
        $parameters = array(
            'userId' => $userId,
            'accountId' => $accountId
        );

        try
        {
            $qb = $this->createQueryBuilder('tweets');

            $query = $qb->select('tweets.id, tweets.tweetContent, tweets.tweetTime, account.screen_name, account.account_avatar, image.path, image.type')
                ->join('tweets.archive_twitter_id', 'account')
                ->where('tweets.archive_account_id = :userId')
                ->andWhere('tweets.archive_twitter_id != :accountId')
                ->leftJoin('tweets.archive_twitterImage', 'image')
                ->orderBy('tweets.tweetTime', 'DESC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {

            return false;
            //TODO-Phill: add in better error reporting

        }


    }

}
