<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * @ORM\Entity(repositoryClass="PileUp\ScheduleBundle\Entity\ScheduleRepository")
 * @ORM\Table(name="app_schedule", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="twitter_tweetx", columns={"twitter_id", "slot", "tweetDate"})}))
 *
 */
class Schedule
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="PileUp\AppBundle\Entity\User", inversedBy="twitterTweets")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     *
     */
    protected $account_id;


    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\TwitterBundle\Entity\TwitterAccount", inversedBy="twitterOwner")
     * @ORM\JoinColumn(name="twitter_id", referencedColumnName="id", nullable=false)
     *
     */
    protected $twitter_id;

    /**
     * @ORM\Column(type="string", length=140, nullable=false)
     */
    protected $tweetContent;

    /**
     * @ORM\Column(type="date", length=100, nullable=false)
     */
    protected $tweetDate;


    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\ScheduleBundle\Entity\Slot", inversedBy="tweetSlot")
     * @ORM\JoinColumn(name="slot", referencedColumnName="id", nullable=false)
     *
     */
    protected $slot_id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\ImageBundle\Entity\TwitterImage", inversedBy="twitterTweet")
     * @ORM\JoinColumn(name="twitterImage", referencedColumnName="id", nullable=true)
     *
     */
    protected $twitterImage;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\ScheduleBundle\Entity\ScheduleArchive", inversedBy="archiveOwner")
     * @ORM\JoinColumn(name="retweet_archive_id", referencedColumnName="id", nullable=true)
     *
     */
    protected $retweet_archive_id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tweetContent
     *
     * @param string $tweetContent
     * @return Schedule
     */
    public function setTweetContent($tweetContent)
    {
        $this->tweetContent = $tweetContent;

        return $this;
    }

    /**
     * Get tweetContent
     *
     * @return string 
     */
    public function getTweetContent()
    {
        return $this->tweetContent;
    }

    /**
     * Set account_id
     *
     * @param \PileUp\AppBundle\Entity\User $accountId
     * @return Schedule
     */
    public function setAccountId(\PileUp\AppBundle\Entity\User $accountId = null)
    {
        $this->account_id = $accountId;

        return $this;
    }

    /**
     * Get account_id
     *
     * @return \PileUp\AppBundle\Entity\User 
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * Set twitter_id
     *
     * @param \PileUp\TwitterBundle\Entity\TwitterAccount $twitterId
     * @return Schedule
     */
    public function setTwitterId(\PileUp\TwitterBundle\Entity\TwitterAccount $twitterId = null)
    {
        $this->twitter_id = $twitterId;

        return $this;
    }

    /**
     * Get twitter_id
     *
     * @return \PileUp\TwitterBundle\Entity\TwitterAccount 
     */
    public function getTwitterId()
    {
        return $this->twitter_id;
    }


    /**
     * Set image_id
     *
     * @param \PileUp\ImageBundle\Entity\TwitterImage $twitterImage
     * @return Schedule
     */
    public function setTwitterImage(\PileUp\ImageBundle\Entity\TwitterImage $twitterImage = null)
    {
        $this->twitterImage = $twitterImage;

        return $this;
    }

    /**
     * Get image_id
     *
     * @return \PileUp\ImageBundle\Entity\TwitterImage 
     */
    public function getTwitterImage()
    {
        return $this->twitterImage;
    }



    /**
     * Set tweetDate
     *
     * @param \DateTime $tweetDate
     * @return Schedule
     */
    public function setTweetDate($tweetDate)
    {
        $this->tweetDate = $tweetDate;

        return $this;
    }

    /**
     * Get tweetDate
     *
     * @return \DateTime 
     */
    public function getTweetDate()
    {
        return $this->tweetDate;
    }

    /**
     * Set slot_id
     *
     * @param $slotId
     * @return Schedule
     */
    public function setSlotId($slotId)
    {
        $this->slot_id = $slotId;

        return $this;
    }

    /**
     * Get slot_id
     *
     * @return \PileUp\ScheduleBundle\Entity\Slot 
     */
    public function getSlotId()
    {
        return $this->slot_id;
    }


    /**
     * Set retweet_archive_id
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $retweetArchiveId
     * @return Retweet
     */
    public function setRetweetArchiveId(\PileUp\ScheduleBundle\Entity\ScheduleArchive $retweetArchiveId)
    {
        $this->retweet_archive_id = $retweetArchiveId;

        return $this;
    }

    /**
     * Get retweet_archive_id
     *
     * @return \PileUp\ScheduleBundle\Entity\ScheduleArchive
     */
    public function getRetweetArchiveId()
    {
        return $this->retweet_archive_id;
    }
}
