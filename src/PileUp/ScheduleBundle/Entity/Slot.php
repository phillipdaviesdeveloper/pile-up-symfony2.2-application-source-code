<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * @ORM\Entity(repositoryClass="PileUp\ScheduleBundle\Entity\SlotRepository")
 * @ORM\Table(name="app_schedule_slot", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="tweet_slot", columns={"slot_twitter_id", "slotTime", "slot_day"})}))
 *
 */
class Slot
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\TwitterBundle\Entity\TwitterAccount", inversedBy="slotOwner")
     * @ORM\JoinColumn(name="slot_twitter_id", referencedColumnName="id", nullable=false)
     *
     */
    protected $slot_twitter_id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\AppBundle\Entity\User", inversedBy="slotAccount")
     * @ORM\JoinColumn(name="slot_account_id", referencedColumnName="id", nullable=false)
     *
     */
    protected $slot_account_id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\ScheduleBundle\Entity\Day", inversedBy="dayOwner")
     * @ORM\JoinColumn(name="slot_day", referencedColumnName="id", nullable=false)
     *
     */
    protected $slot_day;


    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Schedule", mappedBy="slot_id")
     */
    protected $tweetSlot;

    /**
     * @ORM\Column(type="time", length=100, nullable=false)
     */
    protected $slotTime;


    public function __construct()
    {
        $this->tweetSlot = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slotTime
     *
     * @param \DateTime $slotTime
     * @return Slot
     */
    public function setSlotTime($slotTime)
    {
        $this->slotTime = $slotTime;

        return $this;
    }

    /**
     * Get slotTime
     *
     * @return \DateTime 
     */
    public function getSlotTime()
    {
        return $this->slotTime;
    }

    /**
     * Set slot_twitter_id
     *
     * @param $slotTwitterId
     * @return Slot
     */
    public function setSlotTwitterId($slotTwitterId)
    {
        $this->slot_twitter_id = $slotTwitterId;

        return $this;
    }

    /**
     * Get slot_twitter_id
     *
     * @return \PileUp\TwitterBundle\Entity\TwitterAccount 
     */
    public function getSlotTwitterId()
    {
        return $this->slot_twitter_id;
    }

    /**
     * Set slot_day
     *
     * @param \PileUp\ScheduleBundle\Entity\Day $slotDay
     * @return Slot
     */
    public function setSlotDay(\PileUp\ScheduleBundle\Entity\Day $slotDay)
    {
        $this->slot_day = $slotDay;

        return $this;
    }

    /**
     * Get slot_day
     *
     * @return \PileUp\ScheduleBundle\Entity\Day 
     */
    public function getSlotDay()
    {
        return $this->slot_day;
    }

    /**
     * Set slot_account_id
     *
     * @param \PileUp\AppBundle\Entity\User $slotAccountId
     * @return Slot
     */
    public function setSlotAccountId(\PileUp\AppBundle\Entity\User $slotAccountId)
    {
        $this->slot_account_id = $slotAccountId;

        return $this;
    }

    /**
     * Get slot_account_id
     *
     * @return \PileUp\AppBundle\Entity\User 
     */
    public function getSlotAccountId()
    {
        return $this->slot_account_id;
    }

    /**
     * Add tweetSlot
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $tweetSlot
     * @return Slot
     */
    public function addTweetSlot(\PileUp\ScheduleBundle\Entity\Schedule $tweetSlot)
    {
        $this->tweetSlot[] = $tweetSlot;

        return $this;
    }

    /**
     * Remove tweetSlot
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $tweetSlot
     */
    public function removeTweetSlot(\PileUp\ScheduleBundle\Entity\Schedule $tweetSlot)
    {
        $this->tweetSlot->removeElement($tweetSlot);
    }

    /**
     * Get tweetSlot
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTweetSlot()
    {
        return $this->tweetSlot;
    }
}
