<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity(repositoryClass="PileUp\ScheduleBundle\Entity\ScheduleArchiveRepository")
 * @ORM\Table(name="app_schedule_archive")
 *
 */
class ScheduleArchive
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="PileUp\AppBundle\Entity\User", inversedBy="archiveTwitterTweets")
     * @ORM\JoinColumn(name="archive_account_id", referencedColumnName="id", nullable=false)
     *
     */
    protected $archive_account_id;


    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\TwitterBundle\Entity\TwitterAccount", inversedBy="twitterArchiveOwner")
     * @ORM\JoinColumn(name="archive_twitter_id", referencedColumnName="id", nullable=false)
     *
     */
    protected $archive_twitter_id;

    /**
     * @ORM\Column(type="string", length=140, nullable=false)
     */
    protected $tweetContent;

    /**
     * @ORM\Column(type="datetime", length=100, nullable=false)
     */
    protected $tweetTime;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PileUp\ImageBundle\Entity\TwitterImage", inversedBy="archive_twitterTweet")
     * @ORM\JoinColumn(name="archive_twitterImage", referencedColumnName="id", nullable=true)
     *
     */
    protected $archive_twitterImage;


    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Schedule", mappedBy="retweet_archive_id", cascade={"remove"})
     *
     */
    protected $archiveOwner;


    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $tweetedId;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->archiveOwner = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tweetContent
     *
     * @param string $tweetContent
     * @return ScheduleArchive
     */
    public function setTweetContent($tweetContent)
    {
        $this->tweetContent = $tweetContent;

        return $this;
    }

    /**
     * Get tweetContent
     *
     * @return string 
     */
    public function getTweetContent()
    {
        return $this->tweetContent;
    }

    /**
     * Set tweetTime
     *
     * @param \DateTime $tweetTime
     * @return ScheduleArchive
     */
    public function setTweetTime($tweetTime)
    {
        $this->tweetTime = $tweetTime;

        return $this;
    }

    /**
     * Get tweetTime
     *
     * @return \DateTime 
     */
    public function getTweetTime()
    {
        return $this->tweetTime;
    }

    /**
     * Set tweetedId
     *
     * @param integer $tweetedId
     * @return ScheduleArchive
     */
    public function setTweetedId($tweetedId)
    {
        $this->tweetedId = $tweetedId;

        return $this;
    }

    /**
     * Get tweetedId
     *
     * @return integer 
     */
    public function getTweetedId()
    {
        return $this->tweetedId;
    }

    /**
     * Set archive_account_id
     *
     * @param \PileUp\AppBundle\Entity\User $archiveAccountId
     * @return ScheduleArchive
     */
    public function setArchiveAccountId(\PileUp\AppBundle\Entity\User $archiveAccountId)
    {
        $this->archive_account_id = $archiveAccountId;

        return $this;
    }

    /**
     * Get archive_account_id
     *
     * @return \PileUp\AppBundle\Entity\User 
     */
    public function getArchiveAccountId()
    {
        return $this->archive_account_id;
    }

    /**
     * Set archive_twitter_id
     *
     * @param \PileUp\TwitterBundle\Entity\TwitterAccount $archiveTwitterId
     * @return ScheduleArchive
     */
    public function setArchiveTwitterId(\PileUp\TwitterBundle\Entity\TwitterAccount $archiveTwitterId)
    {
        $this->archive_twitter_id = $archiveTwitterId;

        return $this;
    }

    /**
     * Get archive_twitter_id
     *
     * @return \PileUp\TwitterBundle\Entity\TwitterAccount 
     */
    public function getArchiveTwitterId()
    {
        return $this->archive_twitter_id;
    }

    /**
     * Set archive_twitterImage
     *
     * @param \PileUp\ImageBundle\Entity\TwitterImage $archiveTwitterImage
     * @return ScheduleArchive
     */
    public function setArchiveTwitterImage(\PileUp\ImageBundle\Entity\TwitterImage $archiveTwitterImage = null)
    {
        $this->archive_twitterImage = $archiveTwitterImage;

        return $this;
    }

    /**
     * Get archive_twitterImage
     *
     * @return \PileUp\ImageBundle\Entity\TwitterImage 
     */
    public function getArchiveTwitterImage()
    {
        return $this->archive_twitterImage;
    }

    /**
     * Add archiveOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $archiveOwner
     * @return ScheduleArchive
     */
    public function addArchiveOwner(\PileUp\ScheduleBundle\Entity\Schedule $archiveOwner)
    {
        $this->archiveOwner[] = $archiveOwner;

        return $this;
    }

    /**
     * Remove archiveOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $archiveOwner
     */
    public function removeArchiveOwner(\PileUp\ScheduleBundle\Entity\Schedule $archiveOwner)
    {
        $this->archiveOwner->removeElement($archiveOwner);
    }

    /**
     * Get archiveOwner
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArchiveOwner()
    {
        return $this->archiveOwner;
    }
}
