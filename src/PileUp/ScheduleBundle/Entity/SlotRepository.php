<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Slot repository for custom queries
 *
 * Provides additional entity methods to pull filtered results from the persistence layer.
 *
 * Repositories act like the 'model' layer in MVC, in that entities describe structure and implement events / data
 * transformations where repositories provide logic for more specific data requests.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class SlotRepository extends EntityRepository
{

    /**
     * Returns tweets for the provided slots between a set date range
     *
     * @param $slots
     * @param $date
     * @param $end
     *
     * @return array The result of the query
     */
    public function checkFilledSlots($slots, $date, $end)
    {

        //set params
        $parameters = array(
            'slots' => $slots,
            'date' => $date->format('Y-m-d'),
            'end' => $end->format('Y-m-d')
        );


        try
        {
            $qb = $this->createQueryBuilder('slot');

            $query = $qb->select('slot.id, slot.slotTime, tweets.id as tweet_id, tweets.tweetDate, tweets.tweetContent, tweetImage.id as image_id, tweetImage.path, tweetImage.type, archive.tweetedId')
                ->join('slot.tweetSlot', 'tweets')
                ->where($qb->expr()->in('slot.id', ':slots'))
                ->andwhere('tweets.tweetDate BETWEEN :date AND :end')
                ->leftJoin('tweets.twitterImage', 'tweetImage')
                ->leftJoin('tweets.retweet_archive_id', 'archive')
                ->orderBy('slot.slotTime', 'ASC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {

            return null;
            //TODO-Phill: add in better error reporting

        }


    }

}
