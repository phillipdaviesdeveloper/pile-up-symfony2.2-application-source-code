<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Schedule repository for custom queries
 *
 * Provides additional entity methods to pull filtered results from the persistence layer.
 *
 * Repositories act like the 'model' layer in MVC, in that entities describe structure and implement events / data
 * transformations where repositories provide logic for more specific data requests.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class ScheduleRepository extends EntityRepository
{

    /**
     * Finds all tweets scheduled by a specific user
     *
     * @param Integer $userId The id of the required user account
     *
     * @return array The result of the query
     */
    public function findAllFilteredByAccount($userId)
    {

        // if account id is not a number
        if(is_nan($userId))
            return false;

        //set params
        $parameters = array(
            'userId' => $userId
        );

        try
        {
            $qb = $this->createQueryBuilder('tweets');

            $query = $qb->select('tweets.id, tweets.tweetContent, tweets.tweetDate, archive.tweetedId, account.account_avatar, account.screen_name, slots.slotTime, slots.id as slot_id, image.path, image.type')
                ->join('tweets.twitter_id', 'account')
                ->where('tweets.account_id = :userId')
                ->andWhere('tweets.twitter_id = account.id')
                ->join('tweets.slot_id', 'slots')
                ->leftJoin('tweets.retweet_archive_id', 'archive')
                ->leftJoin('tweets.twitterImage', 'image')
                ->orderBy('tweets.tweetDate', 'ASC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {
            //TODO-Phill: add in better error reporting
            return false;

        }


    }

    /**
     * Finds all tweets for all users between a specific time range
     *
     * @param $date
     * @param $time
     *
     * @return array The result of the query
     */
    public function findAllBeforeDate($date, $time)
    {
        //set params
        $parameters = array(
            'date' => $date,
            'time' => $time
        );

        try
        {
            $qb = $this->createQueryBuilder('tweets');

            $query = $qb->select('tweets.id, tweets.tweetContent, tweets.tweetDate, archive.tweetedId, account.account_avatar, slots.slotTime, slots.id as slot_id, image.path, image.type')
                ->where('tweets.tweetDate <= :date')
                ->join('tweets.twitter_id', 'account')
                ->join('tweets.slot_id', 'slots')
                ->andWhere('slots.slotTime <= :time')
                ->leftJoin('tweets.retweet_archive_id', 'archive')
                ->leftJoin('tweets.twitterImage', 'image')
                ->orderBy('tweets.tweetDate', 'ASC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {
            //TODO-Phill: add in better error reporting
            return false;

        }
    }

}
