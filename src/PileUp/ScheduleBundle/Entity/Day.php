<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity(repositoryClass="PileUp\ScheduleBundle\Entity\DayRepository")
 * @ORM\Table(name="app_schedule_day")
 *
 */
class Day
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="integer", length=1, nullable=false)
     */
    protected $dayCode;


    /**
     * @ORM\Column(type="text", length=100, nullable=false)
     */
    protected $dayName;

    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Slot", mappedBy="slot_day", cascade={"remove"})
     *
     */
    protected $dayOwner;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dayOwner = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dayCode
     *
     * @param integer $dayCode
     * @return Day
     */
    public function setDayCode($dayCode)
    {
        $this->dayCode = $dayCode;

        return $this;
    }

    /**
     * Get dayCode
     *
     * @return integer 
     */
    public function getDayCode()
    {
        return $this->dayCode;
    }

    /**
     * Add dayOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Slot $dayOwner
     * @return Day
     */
    public function addDayOwner(\PileUp\ScheduleBundle\Entity\Slot $dayOwner)
    {
        $this->dayOwner[] = $dayOwner;

        return $this;
    }

    /**
     * Remove dayOwner
     *
     * @param \PileUp\ScheduleBundle\Entity\Slot $dayOwner
     */
    public function removeDayOwner(\PileUp\ScheduleBundle\Entity\Slot $dayOwner)
    {
        $this->dayOwner->removeElement($dayOwner);
    }

    /**
     * Get dayOwner
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDayOwner()
    {
        return $this->dayOwner;
    }

    /**
     * Set dayName
     *
     * @param string $dayName
     * @return Day
     */
    public function setDayName($dayName)
    {
        $this->dayName = $dayName;

        return $this;
    }

    /**
     * Get dayName
     *
     * @return string 
     */
    public function getDayName()
    {
        return $this->dayName;
    }
}
