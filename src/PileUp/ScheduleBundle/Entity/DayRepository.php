<?php

namespace PileUp\ScheduleBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Day repository for custom queries
 *
 * Provides additional entity methods to pull filtered results from the persistence layer.
 *
 * Repositories act like the 'model' layer in MVC, in that entities describe structure and implement events / data
 * transformations where repositories provide logic for more specific data requests.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class DayRepository extends EntityRepository
{

    // Get all the slots for a day by user and twitter account
    public function findSlotsFilteredByUserAndAccount($userId, $accountId)
    {

        // if account id is not a number
        if(is_nan($accountId) || is_nan($userId))
            return false;

        //set params
        $parameters = array(
            'userId' => $userId,
            'accountId' => $accountId
        );

        try
        {
            $qb = $this->createQueryBuilder('day');

            $query = $qb->select('slots.id, day.dayCode, slots.slotTime')
                ->join('day.dayOwner', 'slots')
                ->andWhere('slots.slot_account_id = :userId')
                ->andWhere('slots.slot_twitter_id = :accountId')
                ->orderBy('slots.slotTime', 'ASC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {

            return null;

        }


    }


    // Get all the slots for a day by user and twitter account
    public function findSlotsFilteredByUserAndAccountAndDay($userId, $accountId, $day)
    {

        // if account id is not a number
        if(is_nan($accountId) || is_nan($userId))
            return false;

        //set params
        $parameters = array(
            'userId' => $userId,
            'accountId' => $accountId,
            'day' => $day
        );

        try
        {
            $qb = $this->createQueryBuilder('day');

            $query = $qb->select('slots.id, day.dayCode, slots.slotTime')
                ->where('day.dayCode = :day')
                ->join('day.dayOwner', 'slots')
                ->andWhere('slots.slot_account_id = :userId')
                ->andWhere('slots.slot_twitter_id = :accountId')
                ->orderBy('slots.slotTime', 'ASC')
                ->setParameters($parameters)
                ->getQuery();

            return $query->getResult();

        }
        catch(\Doctrine\ORM\NoResultException $e)
        {

            return null;

        }


    }

}
