PileUp - Schedule Bundle
========================

The schedule bundle controls all of the schedule functionality in the application including creating and assigning slots,
displaying the selected twitter account's calendar, displaying the selected day's schedule, providing the sent tweet archive
and tools to amend, add, send or remove tweets and retweets.

Commands
--------

The schedule bundle includes a command line tool for checking and sending all tweets that have been scheduled in the system.

The server should be configured to call the command line script via a cron job every 5 minutes, this call would look something like the following:

crontab -e

*/5 * * * * /Applications/MAMP/bin/php/php5.4.10/bin/php /Applications/MAMP/htdocs/buffer-2/buffer-symfony/2.1.x-dev/app/console twitter:run

this will run a cron every 5 minutes although the second and third arguments should be changed to match your php directory and
your application directory on the server.

tip: you can find your server php version by doing: which php on your live environement

The commands:

 *twitter run
 This command checks the date/time of all the available tweets, if any tweets are set to be sent then the sytem will call
 the twitter bundle and fire the appropriate send functions.

 Errors will be logged although currently there is no logic to deal with the errors other than make a note of the issue.


Controllers
-----------

The schedule bundle controllers deal with the majority of the user specific and account specific dashboard views, the
calendar views and scheduling views. The controllers also deal with the creation of the required forms and the posting of
the forms to the relevant functionality across the other bundles.

* Calendar controller
    The calendar controller is called inside of the twig template /PileUp/ScheduleBundle/Resources/views/Layout/dashboardAccount.html.twig
    and contains methods to display the current, next and previous month's calendar.

    The controller calls the calendar service (/PileUp/ScheduleBundle/DependencyInjection/CalendarService.php) to generate the required
    data and then returns the rendered partial view (/PileUp/ScheduleBundle/Resources/views/Layout/Partials/scheduleCalendar.html.twig)

* Dashboard controller
    The dashboard controller provides the access point for all authenticated users as well as methods to display schedule listing,
    set the twitter account to view the specific account dashboard, display the account archive and list the available accounts
    to display in the account menu.

    Archive and tweet listing pagination is set by the KNP Pagination bundle rather than any custom repository pagination calls.

* Schedule controller
    The schedule controller loads the required schedule for the day selected and displays the tweet data or form for the avaialble
    day slots.

    Processing of posted schedule forms is handled by the twitter bundle to keep the seperation between scheduling and processing of
     tweets. This potentially should be refactored back into the schedule controller to keep the twitter bundle focused on sending and
     authentication rather than scheduling.

* Slot controller
    The slot controller handles the creation of slot forms along with the processing of slot forms as well as any editing or removal logic.

    Currently the slot controller does not contain any removal or editing logic but this is planned for future releases.

Entity
------

The schedule bundle contains the entites that store all Day, Slot and Scheduling information.

Repositories have been generated for custom query creation.


Forms
-----

The form classes for retweets, scheduling and slot creation are stored in this bundle and all are defined as services within the
services.yml file.

Data transformers have been created for hidden archive, day and slot hidden fields to allow integers (id's) to be set by the controllers
and entities to be created from the integers prior to form binding.


Services
--------

The schedule bundle contains services for creating a calendar dataset, generating a day object for every day in the calendar as well
as a set of tools for creating or editing slots in the schedule.



Public assets
-------------

The majority of the application views are stored in the schedule bundle. The main views include the ajax.html.twig file which
is extended to provide a 'chromeless' view when requesting responses via an ajax call. Controllers that can be called by ajax
usually pass a ajax variable to the twig template. This informs the template that it should extend the ajax.html.twig template
over the dashboardAccount.html.twig template which is generally used for all authenticated user views.





