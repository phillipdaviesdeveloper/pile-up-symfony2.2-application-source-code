<?php

namespace PileUp\ScheduleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Slot creation and editing tools
 *
 * Provides functions to add slots to a specific day or to all days
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class SlotController extends Controller
{

    /**
     * Stores the day object that the slot should be set to
     * @var Object PileUp\ScheduleBundle\Entity\Day
     */
    private $day = null;

    /**
     * Stores the currently selected twitter account
     * @var Object PileUp\TwitterBundle\Entity\TwitterAccount
     */
    private $twitterAccount;

    /**
     * Stores the request type
     * @var bool
     */
    private $ajax;

    /**
     * Stores the slot instance
     * @var Object PileUp\ScheduleBundle\Entity\Slot
     */
    private $slot;


    private function ajaxCheck()
    {
        $this->ajax = $this->getRequest()->isXmlHttpRequest();
    }

    /**
     * Generates the data required to setup the form
     *
     * If set returns the day reference.
     *
     * Also returns the currently selected twitter account object
     *
     * @return bool Returns if the form data could be generated or not
     */
    public function setUpFormData()
    {

        try
        {

            if($this->day != null)
            {
                //get all the day patterns in the system
                $this->day = $this->getDoctrine()
                    ->getRepository('PileUpScheduleBundle:Day')
                    ->findOneBy(array('dayCode' => $this->day));
            }

            //get the twitter account object
            $this->twitterAccount = $this->getDoctrine()
                ->getRepository('PileUpTwitterBundle:TwitterAccount')
                ->findOneById($this->get('session')->get('accountId'));

        }
        catch(\Exception $e)
        {
            return false;
        }

        return true;

    }


    /**
     * Creates and if posted to sends the add slot form functionality
     *
     * If the request method is not via post then the add slot form is generated and displayed with the options set
     * to tell the system to add the selected slot to all days.
     *
     * If posted to, the AddSlotAction will attempt to call the slotService and save the selected slot.
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the user to the account specific dashboard
     */
    public function AddSlotAction()
    {

        // check and set the request type
        $this->ajaxCheck();

        $sessionMessages = array();

        foreach ($this->get('session')->getFlashBag()->all() as $type => $messages) {
            foreach ($messages as $message) {
                $sessionMessages[] = $message;
            }
        }

        //setup the form data
        $this->setUpFormData();

        //create the form and pass the options
        $form = $this->createForm('SlotForm', null,
            array(
                'accounts' => $this->twitterAccount,
                'days' => null,
                'multi' => true
            )
        );

        //if the form was posted
        if ($this->getRequest()->getMethod() == 'POST') {

            $form->bind($this->getRequest());

            //if the posted form was valid
            if ($form->isValid()) {

                try{

                    //set success message
                    $message = 'Slot added';

                    // call the slot service and save the slot
                    $this->get('my_slots')->setSlot($form);

                }
                catch(\Exception $e)
                {

                    //TODO-Phill: add more informative error messages
                    $message = 'Sorry there was a problem saving your slot';


                    //set the flash message
                    $this->get('session')->getFlashBag()->add('slotMessage', $message);

                    //redirect
                    return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

                }


                //set the flash message
                $this->get('session')->getFlashBag()->add('slotMessage', $message);

                //redirect
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));


            }
            else
            {

                //TODO-Phill: add more informative error messages
                $message = 'Sorry there was a problem saving your slot';


                //set the flash message
                $this->get('session')->getFlashBag()->add('slotMessage', $message);

                //redirect
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

            }

        }



        return $this->render('PileUpScheduleBundle:Layout:addSlot.html.twig',
            array(
                'form' => $form->createView(),
                'flashMessages' => $sessionMessages,
                'action' => $this->generateUrl('pile_up_schedule_set_slot'),
                'ajax' => $this->ajax
            )
        );

    }


    /**
     * Creates and if posted to sends the add slot form functionality for a specific day
     *
     * Takes the specific day object transformed by PileUp/ScheduleBundle/Form/DataTransformer/DayToNumberTransformer.php
     * to assign a timeslot to the user's account and selected twitter account.
     *
     * @param $day Object PileUp\ScheduleBundle\Entity\Day The specific day object for the add slot functionality
     * @return \Symfony\Component\HttpFoundation\Response Returns the user to the account specific dashboard
     */
    public function AddSpecificSlotAction($day)
    {
        //check the request type
        $this->ajaxCheck();

        $sessionMessages = array();

        foreach ($this->get('session')->getFlashBag()->all() as $type => $messages) {
            foreach ($messages as $message) {
                $sessionMessages[] = $message;
            }
        }

        // assign day to the class property
        $this->day = $day;

        //setup the form data
        $this->setUpFormData();

        //create the form and pass the options for a specific slot creation
        $form = $this->createForm('SlotForm', null,
            array(
                'accounts' => $this->twitterAccount,
                'days' => $this->day,
                'multi' => false
            )
        );

        // if the form was posted
        if ($this->getRequest()->getMethod() == 'POST') {

            $form->bind($this->getRequest());

            //if the form was valid
            if ($form->isValid()) {

                try{

                    //set success message
                    $message = 'Slot added';

                    // call the slot service and save the slot
                    $this->get('my_slots')->setSpecificSlot($form);

                }
                catch(\Exception $e)
                {

                    //TODO-Phill: add more informative error messages
                    $message = 'Sorry there was a problem saving your slot';


                    //set the flash message
                    $this->get('session')->getFlashBag()->add('slotMessage', $message);

                    //redirect
                    return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

                }


                //set the flash message
                $this->get('session')->getFlashBag()->add('slotMessage', $message);

                //redirect
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));


            }
            else
            {

                //TODO-Phill: add more informative error messages
                $message = 'Sorry there was a problem saving your slot';


                //set the flash message
                $this->get('session')->getFlashBag()->add('slotMessage', $message);

                //redirect
                return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

            }

        }


        //sets the form with an action that points to the requested day code
        return $this->render('PileUpScheduleBundle:Layout:addSlot.html.twig',
            array(
                'form' => $form->createView(),
                'flashMessages' => $sessionMessages,
                'action' => $this->generateUrl('pile_up_schedule_set_slot_specific', array('day' => $day)),
                'ajax' => $this->ajax
            )
        );

    }


    /**
     * Removes the slot specified from the system
     *
     * Takes the slot id, finds the instance, checks the user permissions and then deletes the slot from the system.
     *
     * A slot cannot be removed if a tweet is schedule for the slot. A confirmation dialog is displayed on the frontend
     * to prevent accidental removal of slots.
     *
     * @param $slot Integer The specific slot id to be removed
     *
     * @throws \Exception if the slot cannot be found, or the argument format is invalid, or if the user does not have permission
     * to remove the slot
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the user to the account specific dashboard
     */
    public function RemoveSpecificSlotAction($slot)
    {

        $message = 'Sorry there was a problem removing your slot.';

        try{

            if(is_nan($slot))
                throw new \Exception('Sorry, incorrect slot format provided');

            //get all the day patterns in the system
            $this->slot = $this->getDoctrine()
                ->getRepository('PileUpScheduleBundle:Slot')
                ->findOneById($slot);

            // If the current user has access to send the tweet
            if($this->getUser()->getId() != $this->slot->getSlotAccountId()->getId())
                throw new \Exception('User permission issue');

            //call the slot service
            if($this->get('my_slots')->removeSlot($this->slot))
                $message = 'Slot removed';

        }
        catch(\Exception $e)
        {
            $message = 'Sorry there was a problem removing your slot.';
        }

        //set the flash message
        $this->get('session')->getFlashBag()->add('SlotMessage', $message);

        //redirect
        // was a session id available for a twitter account?
        if($this->get('session')->get('accountId'))
            return $this->redirect($this->generateUrl('pile_up_schedule_homepage'));
        else
            return $this->redirect($this->generateUrl('pile_up_schedule_view_account'));

    }

}
