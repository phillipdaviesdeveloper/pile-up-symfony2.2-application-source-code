<?php

namespace PileUp\ScheduleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;


/**
 * Calendar schedule view render class
 *
 * Generates the calendar which contains all of the available slot information as well as controls
 * to add additional slots.
 *
 * Uses the Calendar service (Dependencyinjection/CalendarService.php) to perform all schedule checking and
 * return all of the day in the viewed month along with the linked information.
 *
 * Renders the calender view (Resources/Views/Partials/scheduleCalendar) and loops through all of the provided information
 * in twig.
 *
 * Is called initially in the dashboardAccount view via the twig controller embed method.
 *
 * Pagination is controlled by the getNextMonthAction and the getPrevMonthAction methods which can be accessed via
 * paths configured in config/routing.yml. In general this functionality is called via ajax and loads in the generated
 * calendar view.
 *
 * Ajax methods are defined in AppBundle/Resources/public/javascripts/app.js and AppBundle/Resources/public/javascripts/lib/lib.js
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class CalendarController extends Controller
{

    /**
     * Stores the day that the calendar should start from
     * @var string
     */
    private $day;

    /**
     * Month the calendar should be set to, modified by the getNextMonthAction / getPrevMonthAction
     * @var string
     */
    private $month;

    /**
     * Year the calendar should be set to, modified by the getNextMonthAction / getPrevMonthAction
     * @var string
     */
    private $year;

    /**
     * Generates the calendar using the CalendarService (Dependencyinjection/CalendarService.php)
     *
     * Returns a complete calendar for the month / year sent along with controls for pagination and
     * a colour coded display for quick reference.
     *
     * @param string $year Year the calendar should be set to
     * @param string $month Month the calendar should be set to
     * @param string $day Day the calendar should start from
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the generated calendar view
     */
    public function getCalendarAction($year, $month, $day)
    {

        //store the calendar date
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;

        $calendar = $this->get('my_calendar');

        $calendar = $calendar->createCalendar($this->year, $this->month);

        return $this->render('PileUpScheduleBundle:Layout:Partials/scheduleCalendar.html.twig',
            array(
                'calendar' => $calendar,
                'day' => $this->day,
                'month' => $this->month,
                'monthstring' => date("F", mktime(0, 0, 0, $this->month)),
                'year' => $this->year,
                'monthnext' => date("m", strtotime('+1 month', mktime(0, 0, 0, $this->month))),
                'monthprev' => date("m", strtotime('-1 month', mktime(0, 0, 0, $this->month)))
            ));
    }

    /**
     * Calculates the next month's calendar display
     *
     * Takes into account if next month carries into a new year, checks for Ajax header, if ajax header is present
     * then request is forwarded to the getCalendarAction() to return a view without any 'chrome'.
     *
     * If the user navigates directly to the configured route then the dashboardAccount view is rendered an returned
     * which includes a call to the getCalendarAction(). This allows for a non js fallback in the scheduling pagination.
     *
     * @param string $year Year the calendar should be set to
     * @param string $month Month the calendar should be set to
     * @param string $day Day the calendar should start from
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the generated calendar view
     *
     */
    public function getNextMonthAction($year, $month, $day)
    {

        //get the current month and add 1
        $this->month = $month;
        $this->year = $year;
        $this->day = $day;

        // is the month now past 12, add a year
        if($month == '01')
        {
            $yearString = '1 January '.$year;
            $yearStamp = strtotime('+1 year', strtotime($yearString));
            $this->year = date("Y", $yearStamp);
        }

        // if ajax call serve just the calendar
        if($this->getRequest()->isXmlHttpRequest())
            return $this->forward('PileUpScheduleBundle:Calendar:getCalendar', array(
                'year' => $this->year,
                'month' => $this->month,
                'day' =>$this->day
            ));

        // else build the page
        return $this->render('PileUpScheduleBundle:Layout:dashboardAccount.html.twig',
            array(
                'flashMessages' => null,
                'year' => $this->year,
                'month' => $this->month,
                'day' =>$this->day
            )
        );

    }

    /**
     * Calculates the previous month's calendar display
     *
     * Takes into account if next month carries into the previous year, checks for Ajax header, if ajax header is present
     * then request is forwarded to the getCalendarAction() to return a view without any 'chrome'.
     *
     * If the user navigates directly to the configured route then the dashboardAccount view is rendered an returned
     * which includes a call to the getCalendarAction(). This allows for a non js fallback in the scheduling pagination.
     *
     * @param string $year Year the calendar should be set to
     * @param string $month Month the calendar should be set to
     * @param string $day Day the calendar should start from
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the generated calendar view
     *
     */
    public function getPrevMonthAction($year, $month, $day)
    {

        //get the current month and subtract 1
        $this->month = $month;
        $this->year = $year;
        $this->day = $day;

        // is the month now before 1, set month to 12 and subtract a year
        if($month == '12')
        {
            $yearString = '1 January '.$year;
            $yearStamp = strtotime('-1 year', strtotime($yearString));
            $this->year = date("Y", $yearStamp);
        }

        // if ajax call serve just the calendar
        if($this->getRequest()->isXmlHttpRequest())
            return $this->forward('PileUpScheduleBundle:Calendar:getCalendar', array(
                'year' => $this->year,
                'month' => $this->month,
                'day' =>$this->day
            ));

        return $this->render('PileUpScheduleBundle:Layout:dashboardAccount.html.twig',
            array(
                'flashMessages' => null,
                'year' => $this->year,
                'month' => $this->month,
                'day' =>$this->day
            )
        );

    }



}
