<?php

namespace PileUp\ScheduleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

use PileUp\ImageBundle\Entity\TwitterImage;


/**
 * Main wrapper controller for logged in users
 *
 * Generates the authenticated user views from the reouting config.
 *
 * This controller generates the main 'shell' in which links or forms are generated to access the application functionality.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class DashboardController extends Controller
{

    /**
     * Stores an array of the user's tweets across all of their available twitter accounts
     * @var array
     */
    private $tweets;

    /**
     * Stores an array of the user's assigned twitter accounts
     * @var array
     */
    private $accounts;

    /**
     * Stores an array of the user's sent tweets for the twitter account selected
     * @var array
     */
    private $archive;


    /**
     * Generates the authenticated user dashboard
     *
     * Displays the overview dashboard page and clears any currently set account session.
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the rendered generic dashboard view
     */
    public function indexAction()
    {

        $sessionMessages = array();

        // remove any set account Id session
        $this->get('session')->remove('accountId');

        foreach ($this->get('session')->getFlashBag()->all() as $type => $messages) {
            foreach ($messages as $message) {
                $sessionMessages[] = $message;
            }
        }

        return $this->render('PileUpScheduleBundle:Layout:dashboard.html.twig',
            array(
                'flashMessages' => $sessionMessages,
                'page' => $this->get('request')->query->get('page', 1),
                'ajax' => $this->getRequest()->isXmlHttpRequest()
            ));

    }

    /**
     * Sets the selected user account
     *
     * Takes the provided id, checks it is an integer and then tries to find the account in persistence level.
     *
     * If found the system checks that the current authenticated user is linked to the returned account.
     * Once found and validated the a session is set and the user is redirected to the unique account dashboard.
     *
     * @param Integer $accountId The id of the selected twitter account
     *
     * @throws \Exception Throws a critical error to prevent malicious activity
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns a redirect response to the viewAccountAction
     *
     */
    public function setAccountAction($accountId)
    {

        //make use the value passed is a number
        //TODO-phill: Redirect to an appropriate error page
        if(!is_numeric($accountId))
            throw new \Exception('Error, account ID not in acceptable format');


        $em = $this->getDoctrine()->getManager();

        // Find and return the unique object
        $accountData = $em
            ->getRepository('PileUpTwitterBundle:TwitterAccount')
            ->find($accountId);

        //did we (not) find a account for the ID?
        //TODO-phill: Redirect to an appropriate error page
        if(!$accountData)
            throw new \Exception('Account not found');


        // If the current user has access to the twitter account
        //TODO-phill: Redirect to an appropriate error page
        if($this->getUser()->getId() != $accountData->getAccountId()->getId())
            throw new \Exception('User permission issue');

        // set the account id to a session
        $this->get('session')->set('accountId', $accountId);


        return $this->redirect($this->generateUrl('pile_up_schedule_view_account').'/'.strtolower($accountData->getScreenName()));

    }


    /**
     * Displays the selected twitter account dashboard
     *
     * Checks if a accountId session is available, if not the user is redirected to the generic dashboard.
     *
     * If a valid accountId is present the specific dashboard view is displayed showing the schedule and calendar
     * for the current date.
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns specific account dashboard view
     */
    public function viewAccountAction()
    {

        // if no session id is set redirect to the generic homepage
        if($this->get('session')->get('accountId') == null)
            return $this->forward('PileUpScheduleBundle:Dashboard:index');

        $sessionMessages = array();

        foreach ($this->get('session')->getFlashBag()->all() as $type => $messages) {
            foreach ($messages as $message) {
                $sessionMessages[] = $message;
            }
        }

        // cast to integer for processing
        $year = date('Y');
        $month = date('m');
        $day = date('d');

        return $this->render('PileUpScheduleBundle:Layout:dashboardAccount.html.twig',
            array(
                'flashMessages' => $sessionMessages,
                'day' => $day,
                'month' => $month,
                'year' => $year,
                'monthnext' => date("m", strtotime('+1 month', mktime(0, 0, 0, $month))),
                'monthprev' => date("m", strtotime('-1 month', mktime(0, 0, 0, $month)))
            )
        );
    }



    /**
     * Displays all the currently attached twitter accounts
     *
     * Takes the authenticated user ID and returns the account selection menu
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the account listing menu
     */
    public function getAccountsAction()
    {

        $this->accounts = $this->getDoctrine()
            ->getRepository('PileUpTwitterBundle:TwitterAccount')
            ->findAllFilteredByAccount($this->getUser()->getId());

        return $this->render('PileUpScheduleBundle:Layout:Partials/accountListing.html.twig',
            array(
                'accounts' => $this->accounts
            ));

    }


    /**
     * Displays all the currently scheduled tweets in date order
     *
     * Takes the authenticated user ID and returns all scheduled tweets for the user as rendered in the tweetListing
     * partial (/PileUp/ScheduleBundle/Resources/views/Layout/Partials/tweetListing.html.twig)
     *
     * @param Integer $page The current pagination number
     * @return \Symfony\Component\HttpFoundation\Response Returns the generic dashboard tweet listing
     */
    public function getTweetsAction($page)
    {

        $this->tweets = $this->getDoctrine()
            ->getRepository('PileUpScheduleBundle:Schedule')
            ->findAllFilteredByAccount($this->getUser()->getId());

        $imageLoc = new TwitterImage();

        $imageLoc = $imageLoc->getPlainPath();

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $this->tweets,
            $page/*page number*/,
            10/*limit per page*/
        );

        return $this->render('PileUpScheduleBundle:Layout:Partials/tweetListing.html.twig',
            array(
                'tweets' => $pagination,
                'path' => $imageLoc
            ));

    }

    /**
     * Displays the selected twitter account's archive
     *
     * Checks that the twitter account session is still set, if so displays the archive view which in turn
     * renders the getArchiveAction controller to display the archived (sent) tweet listing.
     *
     * For pagination to work the controller is required to pass the sent get variable to the twig view so it can
     * be loaded by the twig controller as part of the result pagination argument.
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns specific account archive view
     */
    public function viewArchiveAction()
    {

        // if no session id is set redirect to the generic homepage
        if($this->get('session')->get('accountId') == null)
            return $this->forward('PileUpScheduleBundle:Dashboard:index');

        $sessionMessages = array();

        foreach ($this->get('session')->getFlashBag()->all() as $type => $messages) {
            foreach ($messages as $message) {
                $sessionMessages[] = $message;
            }
        }

        return $this->render('PileUpScheduleBundle:Layout:archive.html.twig',
            array(
                'flashMessages' => $sessionMessages,
                'page' => $this->get('request')->query->get('page', 1),
                'ajax' => $this->getRequest()->isXmlHttpRequest()
            )
        );
    }

    /**
     * Displays the selected twitter account's archive listing
     *
     * Queries the persistence layer to return the specific tweets sent by the authenticated user and the selected
     * twitter account.
     *
     * Uses the ScheduleArchive repository found in PileUp/ScheduleBundle/Entity/ScheduleArchiveRepository.php.
     *
     * Results are processed by the KnpPaginator bundle (https://github.com/KnpLabs/KnpPaginatorBundle) to provide
     * a quick pagination solution.
     *
     * @param Integer $page Page number for the pagination
     * @return \Symfony\Component\HttpFoundation\Response Returns the archive listing to load into the archive view
     */
    public function getAccountArchiveAction($page)
    {

        $this->archive = $this->getDoctrine()
            ->getRepository('PileUpScheduleBundle:ScheduleArchive')
            ->findAllFilteredByAccount($this->getUser()->getId(), $this->get('session')->get('accountId'));

        $paginator  = $this->get('knp_paginator');

        // call and set pagination limits
        $pagination = $paginator->paginate(
            $this->archive,
            $page/*page number*/,
            25/*limit per page*/
        );

        return $this->render('PileUpScheduleBundle:Layout:Partials/archiveListing.html.twig',array('archives' => $pagination));

    }


}
