<?php

namespace PileUp\ScheduleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

use PileUp\ImageBundle\Entity\TwitterImage;


/**
 * Schedule listing renderer
 *
 * Queries the data layer to return the slots available for a specific account owned by a specific user on a specific day.
 *
 * If slots are found then their availability is checked and the appropriate display / form / notice is displayed.
 *
 * If the header is an xmlHttpRequest (ajax) then the returned view is rendered chromeless.
 *
 * The current day can be displayed via the getCurrentSchedule Method or a specific day can be selected for
 * schedule pagination via the setScheduleDate Method.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class SchedulerController extends Controller
{

    /**
     * Stores the complete date string
     * @var string
     */
    private $date;

    /**
     * Stores the day for the displayed schedule
     * @var string
     */
    private $day;

    /**
     * Stores the month for the displayed schedule
     * @var string
     */
    private $month;

    /**
     * Stores year for the displayed schedule
     * @var string
     */
    private $year;

    /**
     * Stores if the controller was accessed via ajax or not
     * @var bool
     */
    private $ajax = true;

    /**
     * Contains the returned data set from the findSpecificSlots method
     * @var array | null
     */
    private $slots;

    /**
     * Contains the returned data set from the call to checkSlots method
     * @var array | null
     */
    private $tweets;

    /**
     * Contains the reference to the defined slot service
     * @var Object
     */
    private $slotService;

    /**
     * Contains the reference to the selected twitter account
     * @var Object
     */
    private $twitterAccount;

    /**
     * Contains the data for constructing the schedule view
     * @var array | null
     */
    private $scheduleData = null;


    /**
     * Sets the current date via the passed arguments and calls the main getSchedule Method
     *
     * Returns a complete schedule view of the day / date set
     *
     * @param string $year Year the schedule should be set to
     * @param string $month Month the schedule should be set to
     * @param string $day Day the schedule should start from
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the rendered schedule from the getSchedule Method
     */
    function setScheduleDateAction($day, $month, $year)
    {
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;

        //check to see if the ajax request was made
        $this->ajax = $this->getRequest()->isXmlHttpRequest();

        return $this->getSchedule();

    }


    /**
     * Sets the current date and calls the main getSchedule Method
     *
     * Returns a complete schedule view of the current day / date
     *
     * @return \Symfony\Component\HttpFoundation\Response Returns the rendered schedule from the getSchedule Method
     */
    function getCurrentScheduleAction()
    {

        $this->year = date('Y');
        $this->month = date('m');
        $this->day = date('d');

        return $this->getSchedule();
    }


    /**
     *
     * Returns a complete schedule view of the current day / date
     *
     * Requests all the slots for the set date and the current user / selected account.
     *
     * If data is returned ($slots) then the relevant view data is generated on a slot by slot basis.
     *
     * Slots are based on the day code rather than the date, but are restricted by the user and twitter account so to be
     * able to tell if the user has already added a tweet to the slot we need to find the current date / time and see if
     * any tweets in the schedule are set for the supplied date and which slot id the tweet is linked to.
     *
     *
     * @throws \Exception Throws Error when an account has not been set
     * @return \Symfony\Component\HttpFoundation\Response Returns the rendered schedule from the getSchedule Method
     */
    function getSchedule()
    {

        //TODO-phill: Redirect to an appropriate error page
        // if no session account id is set then throw an error
        if($this->get('session')->get('accountId') == null)
            throw new \Exception('Account was not selected');

        // get the slot service
        $this->slotService = $this->get('my_slots');

        //get the day code
        $dayCode = (int) date('N', mktime(0,0,0,$this->month,$this->day,$this->year));

        //set the start date
        $this->date = new \DateTime(date('Y-m-d', mktime(0,0,0,$this->month,$this->day,$this->year)));

        //get slots
        $this->slots = $this->slotService->findSpecificSlots($dayCode);

        //get the twitter account object
        $this->twitterAccount = $this->getDoctrine()
            ->getRepository('PileUpTwitterBundle:TwitterAccount')
            ->findOneById($this->get('session')->get('accountId'));

        //check slots were returned before doing anything else
        if($this->slots)
        {
            //validate the slots available to see if they contain content already
            // or if they have expired
            $this->checkScheduleSlots();

            //generate the layout array which is parsed by twig in:
            // PileUp/ScheduleBundle/Resources/views/Layout/Partials/scheduleListing.html.twig
            $this->generateSchedule();
        }


        //get the image path
        $imageLoc = new TwitterImage();
        $imageLoc = $imageLoc->getPlainPath();

        // renders the schedule view, displaying either the full layout or the partial layout depending on the request type
        return $this->render('PileUpScheduleBundle:Layout:Partials/scheduleListing.html.twig',
            array(
                'schedule' => $this->scheduleData,
                'path' => $imageLoc,
                'ajax' => $this->ajax,
                'curDay' => date('d-m-Y', mktime(0,0,0, $this->month, $this->day, $this->year)),
                'nextDay' => date('d-m-Y', strtotime('+1 day', mktime(0,0,0, $this->month, $this->day, $this->year))),
                'prevDay' => date('d-m-Y', strtotime('-1 day', mktime(0,0,0, $this->month, $this->day, $this->year))),
                'slotDay' => $dayCode
            ));

    }

    /**
     *
     * Checks the status of the slots found for the user
     *
     * Takes the slots found for the user and twitter account as well as the current date as a range.
     *
     * Checks the schedule to return any tweets that match the arguments so the system can map a currently set tweet to
     * the slots avaialble for the day.
     *
     * Sets the information in the tweets property.
     *
     */
    protected function checkScheduleSlots()
    {

        //check slots
        //end date (date + 24 hours)
        $end = mktime(0,0,0,$this->month,$this->day,$this->year);
        $end = new \DateTime(date('Y-m-d', strtotime('+1 day', $end)));

        //tweets currently set
        $this->tweets = $this->slotService->checkSlots($this->slots, $this->date, $end);

    }


    /**
     *
     * Compares the slot data to the tweet data and generates the data for parsing in the template.
     *
     * Loops through the provided slot data and provides an array which contains either null, a created form or the
     * preview information required to display a tweet already set.
     *
     * This function also checks the current time to see if any slot times have already passed. If a slot has already
     * passed then the data is nulled so an appropriate message is displayed in the template.
     *
     * Sets the information in the scheduleData property.
     *
     */
    protected function generateSchedule()
    {
        $scheduleData = array();

        // set up forms for the remaining slots
        foreach($this->slots as $slot)
        {

            //add checks for the daily schedule vs the current time
            $slotTime = $slot['slotTime']->format('H:i:s');
            $slotTime = date('Y-m-d H:i:s', strtotime($this->year.'-'.$this->month.'-'.$this->day.' '.$slotTime));

            //TODO-phill: This call should actually be made just once outside of the loop with the data stored in an object for reference
            $slotData = $this->slotService->findSlot($slot['id']);

            //create the form and pass the options
            $scheduleData[$slot['id']]['tweet'] = null;
            $scheduleData[$slot['id']]['form'] = $this->createForm('ScheduleForm', null,
                array(
                    'accounts' => $this->twitterAccount,
                    'slots' => $slotData,
                    'date' => date('Y-m-d', $this->date->getTimestamp())
                ))
                ->createView();
            $scheduleData[$slot['id']]['time'] = $slotTime;

            $scheduleData[$slot['id']]['slot_id'] = $slot['id'];


            // if the slot has already passed
            if(strtotime($slotTime) <= time())
            {
                $scheduleData[$slot['id']]['form'] = null;
                $scheduleData[$slot['id']]['tweet'] = null;
                $scheduleData[$slot['id']]['time'] = $slotTime;
                $scheduleData[$slot['id']]['slot_id'] = $slot['id'];
            }


            // for each slot, loop through and check if a tweet is assigned to that slot
            foreach($this->tweets as $tweet)
            {

                if($tweet['id'] == $slot['id'])
                {
                    // tweet already set, so pass the preview information and overwrite the form container
                    $scheduleData[$slot['id']]['form'] = null;
                    $scheduleData[$slot['id']]['tweet'] = array(
                        'tweetContent' => $tweet['tweetContent'],
                        'tweetImage' => $tweet['image_id'],
                        'path' => $tweet['path'],
                        'type' => $tweet['type'],
                        'id' => $tweet['tweet_id'],
                        'tweetedId' => $tweet['tweetedId'],
                        'date' => $tweet['tweetDate'],
                        'time' => $tweet['slotTime'],
                        'slot_id' => $slotData->getId()

                    );
                    $scheduleData[$slot['id']]['time'] = $slotTime;
                    $scheduleData[$slot['id']]['slot_id'] = $slot['id'];
                }

            }

        }

        $this->scheduleData = $scheduleData;

    }

}
