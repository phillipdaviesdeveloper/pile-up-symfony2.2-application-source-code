<?php

// src/PileUp/ScheduleBundle/Command/TwitterBotCommand.php
namespace PileUp\ScheduleBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use PileUp\ScheduleBundle\Entity\ScheduleArchive;


/**
 * Command line tool for checking the tweet send schedule
 *
 * Processes the app_schedule table checking designated slot times. If a tweet is in the system
 * and its date / time is before the date / time the tweet was sent then the tweet information
 * is sent to the tweet service to be posted to twitter.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class TwitterBotCommand extends ContainerAwareCommand
{

    /**
     * Configure the command
     *
     * Configures the call for the command as well as the command description
     *
     * @access public
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('twitter:run')
            ->setDescription('Check for scheduled tweets and send them to twitter')
        ;
    }


    /**
     * Execute the command
     *
     * Pulls the current application schedule, checks the tweet time and type to send to the
     * relevant processing script.
     *
     * @access protected
     *
     * @uses \PileUp\TwitterBundle\DependencyInjection\Tweet to send tweets to twitter
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get the entity manager service
        $em = $this->getContainer()->get('doctrine');

        // get the twitter service for sending
        $tweeter = $this->getContainer()->get('my_twitter_tweet');

        $date = new \DateTime(date('Y-m-d', time()));

        $time = new \DateTime(date('H:i', time()));

        $schedule = $em->getRepository('PileUpScheduleBundle:Schedule')
                    ->findAllBeforeDate($date, $time);

        foreach($schedule as $tweet)
        {

            //is the tweet a retweet?
            if($tweet['tweetedId'] == null)
            {
                $response = $tweeter->commandSendData($tweet['id']);

                // catch the returned response object from the posted tweet
                if($response != null)
                {
                    $response = json_decode($response['response']);

                    $tweeter->commandArchiveData($tweet, $response->id_str, new ScheduleArchive);
                    $output->writeln('Tweet Sent');

                }
                else
                {
                    $output->writeln('error sending tweet');
                }

            }
            else{

                $response = $tweeter->commandSendRetweetData($tweet['id']);

                // catch the returned response object from the posted tweet
                if($response != null)
                {

                    $tweeter->commandRemoveRetweetData($tweet['id']);
                    $output->writeln('Retweet Sent');
                }
                else
                {
                    $output->writeln('error sending retweet');
                }
            }




        }

    }
}