<?php

namespace PileUp\ScheduleBundle\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * For creation of a single calendar day object
 *
 * Generates a calendar day which is manipulated by the calendar service (/PileUp/ScheduleBundle/DependencyInjection/CalendarService.php).
 *
 * Essentially this class creates an object which is then filled with the specific information for the day within the
 * current month selected in the calendar.
 *
 * Additional helper functions can be added to this class as required to extend the container functionality.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class CalendarDay extends Controller
{
    /**
     * Stores the instances date
     * @var string
     */
    protected $date;

    /**
     * Stores the instances date number e.g. 12
     * @var string
     */
    protected $dateNum;

    /**
     * Stores the day code e.g. 2
     * @var string
     */
    protected $dayCode;

    /**
     * Stores the currentDay flag
     * @var bool
     */
    protected $currentDay;

    /**
     * Stores the completed flag
     * @var bool
     */
    protected $completed;

    /**
     * Stores the blank flag
     * @var bool
     */
    protected $blank;

    /**
     * Stores the empty flag
     * @var bool
     */
    protected $empty;

    /**
     * Stores past flag
     * @var bool
     */
    protected $past;

    /**
     * Stores slots assigned to the day
     * @var array
     */
    protected $slots;


    /**
     * Resets the day object
     *
     * Ensures all properties are reset as the service is only initiated once rather than
     * providing a new clean object via a new instance.
     *
     */
    public function reset()
    {
        $this->completed = false;
        $this->blank = false;
        $this->currentDay = false;
        $this->slots = null;
        $this->past = false;
        $this->empty = true;
    }


    //setters//

    /**
     * Sets the date of the day in Ymd format
     *
     * Allows the date of the day object to be set dynamically
     *
     * @param String $date The date of the day passed as a string
     *
     */
    public function setDate($date)
    {
        $date = date('Ymd', strtotime($date));

        $this->date = $date;
    }

    /**
     * Sets the date number
     *
     * Allows the date number of the day object to be set dynamically
     *
     * @param Integer $dateNum The current date number of the day in the month e.g. 21
     *
     */
    public function setDateNum($dateNum)
    {
        $this->dateNum = $dateNum;
    }

    /**
     * Sets the day code
     *
     * Allows the day code of the day object to be set dynamically
     *
     *  0 = sunday
     *  1 = monday
     *
     *  etc
     *
     * @param String $dayCode The current day code of the day e.g. 3
     *
     */
    public function setdayCode($dayCode)
    {
        $this->dayCode = $dayCode;
    }

    /**
     * Assigns slots found for the day
     *
     * Checks the length of the passed array, if the array is not empty then
     * the empty property of the class is set to false to indicate that the day
     * has slots available.
     *
     * @param Array $slots array of slots
     *
     */
    public function setSlots(array $slots)
    {

        if(sizeof($slots) > 0)
        {
            $this->empty = false;
        }

        $this->slots = $slots;

    }

    /**
     * Sets the completion flag for the day
     *
     * Sets true if all the slots for the day have been assigned tweets
     *
     * @param Bool $comp Completion flag
     *
     */
    public function setComplete($comp)
    {
        $this->completed = $comp;
    }

    /**
     * Sets the blank flag for the day
     *
     * Sets true if the day does not contains slots but none are filled in
     *
     * @param Bool $blank Blank flag
     *
     */
    public function setBlank($blank)
    {
        $this->blank = $blank;
    }

    /**
     * Sets the past flag for the day
     *
     * Sets true if the day is in the past of the current date
     *
     * @param Bool $past Past flag
     *
     */
    public function setPast($past)
    {
        $this->past = $past;
    }



    //getters


    /**
     * Gets the date
     *
     * Gets the date for the day instance in Ymd format
     *
     * @return String Returns the date of the day
     *
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Gets the date number
     *
     * Gets the date number for the day instance
     *
     * @return Integer Returns the date number of the day e.g. 23
     *
     */
    public function getDateNum()
    {
        return $this->dateNum;
    }

    /**
     * Gets the day code
     *
     * Gets the day code for the day instance
     *
     * @return String Returns the day code
     *
     */
    public function getDayCode()
    {
        return $this->dayCode;
    }

    /**
     * Gets the day's slots
     *
     * Gets the day's slots for the day instance
     *
     * @return Array Returns the array of slots attached to the instance
     *
     */
    public function getSlots()
    {

        return $this->slots;
    }

    /**
     * Gets the current day flag
     *
     * Displays true if the day matches the current date, false otherwise
     *
     * @return Bool Returns the current day flag
     *
     */
    function getCurrent()
    {
        return $this->currentDay;
    }

    /**
     * Gets the complete day flag
     *
     * Displays true if all slots for the day are completed, false otherwise
     *
     * @return Bool Returns the complete day flag
     *
     */
    function getComplete()
    {
        return $this->completed;
    }

    /**
     * Gets the blank day flag
     *
     * Displays true if no slots assigned to the day are filled in, false otherwise
     *
     * @return Bool Returns the blank day flag
     *
     */
    function getBlank()
    {
        return $this->blank;
    }

    /**
     * Gets the past day flag
     *
     * Displays true if the day is in the past to the current date, false otherwise
     *
     * @return Bool Returns the past day flag
     *
     */
    function getPast()
    {
        return $this->past;
    }

    /**
     * Gets the empty day flag
     *
     * Displays true if the day does not contain any slots, false otherwise
     *
     * @return Bool Returns the empty day flag
     *
     */
    function getEmpty()
    {
        return $this->empty;
    }



    // helper methods

    /**
     * Checks the current date
     *
     * If the day generated is on the same day as the current date then the
     * currentDay property is set to true
     *
     */
    public function isCurrent()
    {
        $curDate = date('Ymd');

        if($curDate == $this->date)
            $this->currentDay = true;
    }


}
