<?php

namespace PileUp\ScheduleBundle\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Generates and renders the monthly calendar data and view
 *
 * Calculates the amount of days for the specified year and month, identifies the available slots and tweets
 * and generates the required data to pass for rendering in the view.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class CalendarService extends Controller
{

    /**
     * Stores current calendar year
     * @var integer
     */
    private $year;

    /**
     * Stores current calendar month
     * @var integer
     */
    private $month;

    /**
     * Stores current calendar week, as it adds days the weeks increment.
     * @var integer
     */
    private $week;

    /**
     * Stores the total amount of days in the selected calendar month/year.
     * @var integer
     */
    private $days;

    /**
     * Stores the date to be used when creating a day instance
     * @var string
     */
    private $date;

    /**
     * Stores the current day code when checking / creating a day instance
     * @var integer
     */
    private $curDay;

    /**
     * Stores the day number of the month to be used when creating a day instance
     * @var integer
     */
    private $runningDay;

    /**
     * Stores the current day number for counting days in the week
     * @var integer
     */
    private $weekDays;

    /**
     * Stores the current day's slots
     * @var array
     */
    private $slots;

    /**
     * Stores the current day's tweets
     * @var array
     */
    private $tweets;


    /**
     * Generates the calender as an array for parsing
     *
     * Calculates the days in the month, generates a new day object instance for each day of the month using the
     * calendar day service and configures the day to display the avaialble slots and slot status.
     *
     * @param String $year The year of the calender month requested
     * @param String $month The month of the calender requested
     *
     * @return array $calendar Returns the calender as an array
     */
    public function createCalendar($year, $month)
    {

        //setup the year and month
        $this->year = $year;
        $this->month = $month;

        $this->week = 1;
        $this->weekDays = 1;

        // get the starting day code for the month for that year
        $this->curDay = $this->getStartingDay();

        // get days in the month
        $this->days = $this->getMonthDays();

        //populate the slot data
        $this->slots = $this->generateSlots();

        //check which slots have tweets
        $this->tweets = $this->validateSlots();

        //set up the calendar holder
        $calendar = array();

        for($x = 1; $x < $this->curDay; $x++):
            $this->weekDays++;
            $calendar['week'][$this->week]['blankstart'][$x] = 'blank day';
        endfor;

        //loop through all the days in the calender
        for($day = 1; $day <= $this->days; $day++)
        {

            if($this->curDay == 0)
                $this->curDay = 1;

            if($this->curDay > 7)
            {
                $this->curDay = 1;
                $this->weekDays = 0;

                $this->week++;
            }

            //add the weekday, running and current day counters
            $this->weekDays++;
            $this->curDay++;
            $this->runningDay = $day;


            //set the date for the calendar item
            $this->date = date('Y-m-d', mktime(0,0,0,$this->month,$day,$this->year));

            // generate a day object with all the slot data
            $dayData = $this->generateDay();

            //checks slots to see if current day should contains slots and how many
            $slots = $this->checkDaySlots();

            // relate the date to the current day object
            $dayData->setSlots($slots);

            // check the day instance vs time, slots and available tweets, set the required flags
            $this->configureDay($dayData);

            // create the array for twig
            $calendar['week'][$this->week]['day'][$day]['date'] = $dayData->getDate();
            $calendar['week'][$this->week]['day'][$day]['dateNum'] = $dayData->getDateNum();
            $calendar['week'][$this->week]['day'][$day]['dayCode'] = $dayData->getDayCode();
            $calendar['week'][$this->week]['day'][$day]['slots'] = $dayData->getSlots();
            $calendar['week'][$this->week]['day'][$day]['completed'] = $dayData->getComplete();
            $calendar['week'][$this->week]['day'][$day]['blank'] = $dayData->getBlank();
            $calendar['week'][$this->week]['day'][$day]['empty'] = $dayData->getEmpty();
            $calendar['week'][$this->week]['day'][$day]['past'] = $dayData->getPast();
            $calendar['week'][$this->week]['day'][$day]['current'] = $dayData->getCurrent();

        }

        //if the week was not complete add the blank days to complete the calendar
        if($this->weekDays < 7)
        {
            for($x = 1; $x <= (7 - $this->weekDays); $x++)
                $calendar['week'][$this->week]['blankend'][$x] = 'blank day';
        }

        //return the $calendar array for processing
        return $calendar;
    }


    /**
     * Generates a calender day instance
     *
     * Calls the CalendarDay service with the reset method to clear the object properties.
     *
     * Sets the initial day properties such as date, date number, day code and if the day is the current day.
     *
     * @return object /PileUp/ScheduleBundle/DependencyInjection/CalendarDay $day Returns a clean instance of the calendar day onject
     */
    private function generateDay()
    {
        $day = $this->get('my_calendar_day');

        // resets properties
        $day->reset();

        $day->setDate($this->date);
        $day->setDateNum($this->runningDay);
        $day->setDayCode(($this->curDay-1));
        $day->isCurrent();

        return $day;
    }

    /**
     * Configures the provided calender day object
     *
     * Checks the avaialble slots for the day, the tweets available, the times of the slots and sets the appropriate flags
     * on the calender day object.
     *
     * @param \PileUp\ScheduleBundle\DependencyInjection\CalenderDay $dayData Instance of calendar day object to be configured
     */
    private function configureDay($dayData)
    {

        // tweet counter for the day
        $dayTweets = array();

        // for each slot available on the day check each avaialble tweet and see if the date and id match
        foreach($dayData->getSlots() as $daySlot)
        {

            // run through the day's slots and see if we have tweets for all
            // so we can set the complete | incomplete | empty | blank flags
            foreach($this->tweets as $tweet)
            {

                // does a tweet have the same id as a slot, is the tweet for the current day?
                if($daySlot['id'] == $tweet['id'] && $this->isToday($tweet['tweetDate']->format('Y-m-d'), $this->date))
                {
                    // this tweet is assigned to the day and slot so add it to the counter
                    $dayTweets[] = $tweet['id'];
                }

            }
        }


        // check the day to see if all the slots have been filled, if so, set complete
        if(sizeof($dayData->getSlots()) == sizeof($dayTweets) && sizeof($dayData->getSlots()) != 0)
        {
            $dayData->setComplete(true);
        }

        // check the day to see if all slots are empty
        if((sizeof($dayTweets) == 0) && sizeof($dayData->getSlots()) != 0)
        {
            $dayData->setBlank(true);
        }


        // if the date was in the past
        if($this->isPast($this->date))
        {
            $dayData->setPast(true);
        }


    }



    /**
     * Generates an array containing all the user / twitter account slots
     *
     * Finds all the slots for the user and stores them in an array.
     *
     * Uses the slot service (/PileUp/ScheduleBundle/DependencyInjection/SlotService.php) to find all the avaialble slots.
     *
     * @return array Returns an array containing all the slots available to the user and their selected twitter account
     */
    private function generateSlots()
    {

        $slots = array();

        $x = 0;

        // call the slot service
        $slotData = $this->get('my_slots')->findAllSlots();

        foreach($slotData as $slot)
        {
            $slots[$x] = $slot;

            $x++;
        }

        return $slots;

    }

    /**
     * Loops through the available slots and identifies the slots available for the day
     *
     * @return array Returns an array containing all the slots available for the current day instance
     */
    private function checkDaySlots()
    {
        $slots = array();

        // if the day has slot data
        for($x = 0; $x < sizeof($this->slots); $x++)
        {

            // check if the slot is for the day or should be on all days
            if($this->slots[$x]['dayCode'] == ($this->curDay-1))
            {

                $slots[$x] = array(
                    'id' => $this->slots[$x]['id'],
                    'slotTime' => $this->slots[$x]['slotTime']
                );

            }

        }

        return $slots;
    }



    /**
     * Generates an array containing all the tweets assigned to slots
     *
     * Takes a date range of one month and searches for any scheduled tweets in that month that are assigned to the
     * slots available for the month.
     *
     * Uses the slot service (/PileUp/ScheduleBundle/DependencyInjection/SlotService.php) to check the schedule bundle
     * for linked scheduled tweets.
     *
     * @return array Returns an array containing all the tweet information found
     */
    private function validateSlots()
    {

        $date = new \DateTime(date('Y-m-d', mktime(0,0,0,$this->month,1,$this->year)));

        $end = mktime(0,0,0,$this->month,1,$this->year);
        $end = new \DateTime(date('Y-m-d', strtotime('+1 month', $end)));

        return $this->get('my_slots')->checkSlots($this->slots, $date, $end);
    }



    /**
     * Finds the first day of the month as a integer
     *
     * Finds the first day of the month as a number e.g. 1 = monday, 2 = tuesday.
     *
     * Syncs the day numbers up to 1-7 instead of 0-6 to match with the database day codes.
     *
     * @return integer $startDay Returns the first day of the month as a number
     */
    public function getStartingDay()
    {
        $startDay = (int) date('w',mktime(0,0,0,$this->month,1,$this->year));

        // if the starting day is a sunday (0) then force it to our sunday code (7)
        if($startDay === 0)
            $startDay = 7;

        return $startDay;
    }


    /**
     *
     * Finds the total number of days in the currently selected month
     *
     * @return integer Returns the total number of days in the currently selected month
     */
    public function getMonthDays()
    {
        return cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);
    }


    /**
     *
     * Finds the total number of days in the currently selected month
     *
     * @param String $date The current server date
     * @param String $calDate The current date for the day instance
     *
     * @return bool Returns true if the $calDate is the same as the server $date
     */
    private function isToday($date, $calDate) // midnight second
    {
        return ((strtotime($date) >= strtotime($calDate)) && (strtotime($date) <= strtotime('+1 day', strtotime($calDate))));

    }

    /**
     *
     * Checks to see if the day instance is in the past
     *
     * @param String $calDate The current calendar date
     *
     * @return bool Returns true if the $calDate is in the past
     */
    function isPast($calDate)
    {
        return (strtotime($calDate) < strtotime(date('Y-m-d')));
    }

}
