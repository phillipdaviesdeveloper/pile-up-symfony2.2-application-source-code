<?php

namespace PileUp\ScheduleBundle\DependencyInjection;

use PileUp\ScheduleBundle\Entity\Slot;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * For finding and creating slots
 *
 * Defined as the service "my_slots", used to create and find slots throughout the system.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class SlotService extends Controller
{

    /**
     * Stores the slot instance selected
     * @var \PileUp\ScheduleBundle\Entity\Slot
     */
    private $slot;

    /**
     * Creates a new slot for either the selected day or all days
     *
     * Accepts and processes the slotform data, checks the user has permission to add a slot and checks if
     * the slot should be added to a specific day or should be added to all days.
     *
     * @param object $form The valid form object of the slotForm type (PileUp/ScheduleBundle/Form/Type/SlotForm.php)
     *
     * @throws \Exception If a user does not have permission to add the slot
     */
    public function setSlot($form)
    {

        $em = $this->getDoctrine()->getManager();

        $this->slot = $form->getData();

        if($form->get('slot_day')->getData()->getDayCode() != 0)
        {

            // If the current user has access to send the tweet
            if($this->getUser()->getId() != $form->get('slot_twitter_id')->getData()->getAccountId()->getId())
                throw new \Exception('User permission issue');


            // set the non provided entity information
            $this->slot->setSlotAccountId($this->getUser());

            $em->persist($this->slot);
        }
        else
        {

            //TODO-Phill: need to change logic to ignore existing single day slots

            $x = 1;

            // create all the slot entries with the new day value
            while($x <= 7)
            {

                $slot = new Slot();

                $day = $this->getDoctrine()
                    ->getRepository('PileUpScheduleBundle:Day')
                    ->findOneBy(
                            array(
                                'dayCode' => $x
                            ));

                // If the current user has access to send the tweet
                if($this->getUser()->getId() != $form->get('slot_twitter_id')->getData()->getAccountId()->getId())
                    throw new \Exception('User permission issue');


                // set the non provided entity information
                $slot->setSlotTwitterId($form->get('slot_twitter_id')->getData());
                $slot->setSlotDay($day);
                $slot->setSlotAccountId($this->getUser());
                $slot->setSlotTime($form->get('slotTime')->getData());

                $em->persist($slot);

                $x++;

            }

        }



        // update the database
        $em->flush();

    }


    /**
     * Creates a new slot for the selected day
     *
     * Accepts and processes the slotform data, checks the user has permission to add a slot and checks if
     * the slot should be added to a specific day or should be added to all days.
     *
     * @param object $form The valid form object of the slotForm type (PileUp/ScheduleBundle/Form/Type/SlotForm.php)
     *
     * @throws \Exception If a user does not have permission to add the slot
     */
    public function setSpecificSlot($form)
    {

        $em = $this->getDoctrine()->getManager();

        $this->slot = $form->getData();

        // If the current user has access to send the tweet
        if($this->getUser()->getId() != $form->get('slot_twitter_id')->getData()->getAccountId()->getId())
            throw new \Exception('User permission issue');

        // set the non provided entity information
        $this->slot->setSlotAccountId($this->getUser());

        $em->persist($this->slot);

        // update the database
        $em->flush();
    }

    /**
     * Finds all slots by user and twitter account
     *
     * Calls the Day repository (/PileUp/ScheduleBundle/Entity/DayRepository.php) to find all slots attached to
     * the days available
     *
     * @return array $slotData An array containing the results from the query
     *
     */
    public function findAllSlots()
    {

        try{

            $em = $this->getDoctrine()->getManager();

            // Find and return Slots linked to the twitter account and user account
            $slotData = $em
                ->getRepository('PileUpScheduleBundle:Day')
                ->findSlotsFilteredByUserAndAccount($this->getUser()->getId(), $this->get('session')->get('accountId'));

        }
        catch(\Exception $e)
        {
            return $e;
        }

        return $slotData;

    }

    /**
     * Finds all slots by user and twitter account
     *
     * Calls the Day repository (/PileUp/ScheduleBundle/Entity/DayRepository.php) to find all slots attached to
     * the specified day
     *
     * @param Integer $day The day code (1-7) for the specific day
     *
     * @throws \Exception If supplied day is not a number
     *
     * @return array $slotData An array containing the results from the query
     */
    public function findSpecificSlots($day)
    {

        try{

            if(is_nan($day))
                throw new \Exception('Invalid day number');

            $em = $this->getDoctrine()->getManager();

            // Find and return Slots linked to the twitter account, user account and day
            $slotData = $em
                ->getRepository('PileUpScheduleBundle:Day')
                ->findSlotsFilteredByUserAndAccountAndDay($this->getUser()->getId(), $this->get('session')->get('accountId'), $day);

        }
        catch(\Exception $e)
        {
            return $e;
        }

        return $slotData;

    }


    /**
     * Finds all tweets associated to the slots provided within the date range
     *
     * Calls the Schedule repository (/PileUp/ScheduleBundle/Entity/ScheduleRepository.php) to find all the tweets
     * for the specified slots within the date range provided.
     *
     * @param Array $slots array of the slots which require checking for tweets
     * @param String $month the month that should be checked
     * @param String $end the end of the date range to be checked
     *
     * @return array $slotData An array containing the results from the query
     */
    public function checkSlots($slots, $month, $end)
    {

        try{

            $em = $this->getDoctrine()->getManager();

            // Find filled slots
            $slotData = $em
                ->getRepository('PileUpScheduleBundle:Slot')
                ->checkFilledSlots($slots, $month, $end);

        }
        catch(\Exception $e)
        {
            return false;
        }


        return $slotData;

    }


    /**
     * Finds the slot by the provided ID
     *
     * Finds a single instance of the slot entity based on ID
     *
     * @param Integer $slot ID of the slot the system is trying to find
     *
     * @return array $slotData An array containing the results from the query
     */
    public function findSlot($slot)
    {

        try{

            $em = $this->getDoctrine()->getManager();

            // Find and return Slots linked to the twitter account, user account and day
            $slotData = $em
                ->getRepository('PileUpScheduleBundle:Slot')
                ->findOneById($slot);

        }
        catch(\Exception $e)
        {
            return $e;
        }

        return $slotData;

    }


    /**
     * Finds the slot by the provided ID
     *
     * Finds a single instance of the slot entity based on ID
     *
     * @param \PileUp\ScheduleBundle\DependencyInjection\PileUp\ScheduleBundle\Entity\Slot $slot ID of the slot the system is trying to find
     *
     * @return array $slotData An array containing the results from the query
     */
    public function removeSlot($slot)
    {

        $em = $this->getDoctrine()->getManager();

        try{

            $em->remove($slot);
            $em->flush();

        }
        catch(\Exception $e)
        {
            return false;
        }

        return true;

    }

}
