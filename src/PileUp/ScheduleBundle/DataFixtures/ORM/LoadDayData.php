<?php

namespace PileUp\ImageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ScheduleBundle\Entity\Day;

/**
 *
 * Day fixture
 *
 * Generates the days and day codes avaialble for slection in the system, the day codes should never need to be
 * changed beyond 0 to 7 (as there is only 7 days in the week + an all days flag) but if required modifications
 * can be made here.
 *
 * To generate fixtures cd to the application directory in terminal and run: php app/console doctrine:fixtures:load
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class LoadDayData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $dayId = 0;

        //setup code names
        $dayNames = array(
            0 => 'Every Day',
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday'
        );

        //loop through and create all the day codes
        while($dayId <= 7)
        {
            $day = new Day();
            $day->setDayCode($dayId);
            $day->setDayName($dayNames[$dayId]);
            $manager->persist($day);
            $manager->flush();

            $this->addReference('day-'.$dayId, $day);

            $dayId++;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }

}