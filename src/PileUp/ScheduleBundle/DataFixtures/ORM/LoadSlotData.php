<?php
// src/PileUp/ScheduleBundle/DataFixtures/ORM/LoadSlotData.php

namespace PileUp\ImageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ScheduleBundle\Entity\Slot;

class LoadSlotData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //make some users
        $slot = new Slot();

        $slot->setSlotTwitterId($this->getReference('twitter-account'));
        $slot->setSlotDay($this->getReference('day-2'));
        $slot->setSlotAccountId($this->getReference('user'));

        $slot->setSlotTime(new \DateTime());

        $manager->persist($slot);
        $manager->flush();

        $this->addReference('slot', $slot);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }

}