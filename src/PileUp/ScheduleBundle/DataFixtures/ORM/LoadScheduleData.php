<?php

namespace PileUp\ImageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ScheduleBundle\Entity\Schedule;

/**
 *
 * Schedule fixture
 *
 * Generates a sample schedule fixture an assigns it to a generated slot, user and account.
 *
 * To generate fixtures cd to the application directory in terminal and run: php app/console doctrine:fixtures:load
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class LoadScheduleData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //make some users
        $schedule = new Schedule();

        //gets the references from previously constructed fixtures to form relations
        $schedule->setAccountId($this->getReference('user'));
        $schedule->setTwitterId($this->getReference('twitter-account'));


        $schedule->setTweetContent('Initial test tweet');

        $schedule->setTweetDate(new \DateTime());

        //gets the references from previously constructed fixtures to form relations
        $schedule->setSlotId($this->getReference('slot'));

        $schedule->setTwitterImage(null);

        $manager->persist($schedule);
        $manager->flush();


    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }

}