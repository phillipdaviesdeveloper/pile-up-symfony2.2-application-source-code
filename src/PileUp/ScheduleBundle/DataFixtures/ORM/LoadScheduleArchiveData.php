<?php

namespace PileUp\ImageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ScheduleBundle\Entity\ScheduleArchive;


/**
 *
 * Schedule archive fixture
 *
 * Generates an initial archived tweet item to ensure that the archive is working.
 *
 * Assigns a tweeted ID to allow retweets although id may require updating if the tweet is removed from twitter.
 *
 * To generate fixtures cd to the application directory in terminal and run: php app/console doctrine:fixtures:load
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class LoadScheduleArchiveData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //make some users
        $archive = new ScheduleArchive();

        //gets the references from previously constructed fixtures to form relations
        $archive->setArchiveAccountId($this->getReference('user'));
        $archive->setArchiveTwitterId($this->getReference('twitter-account'));

        $archive->setTweetContent('Initial test archive tweet');

        $archive->setTweetTime(new \DateTime());

        $archive->setArchiveTwitterImage($this->getReference('image'));

        $archive->setTweetedId('356110835120218112');

        $manager->persist($archive);
        $manager->flush();

        $this->setReference('archive', $archive);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 7;
    }

}