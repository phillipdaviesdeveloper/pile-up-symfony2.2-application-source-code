<?php

namespace PileUp\AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\AppBundle\Entity\User;


/**
 * User to number transformer
 *
 * Allows a hidden field to be transformed into an entity before the bind action is called in a controller.
 * This allows data typing in entities when creating collections for relations and allows you to pass an id
 * without having to extract the id from an object (a db call usually) or manually transforming the id
 * into an entity before the bind on submission of a form.
 *
 * This transfomer is used whenever a user account association is required although in general the application
 * can retrive the current user via the $this->getUser() method when in a extented controller class.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class UserToNumberTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (user) to a string (number).
     *
     * @param  User|null $user
     * @return string
     */
    public function transform($user)
    {
        if (null === $user) {
            return "";
        }

        return $user->getId();
    }

    /**
     * Transforms a string (number) to an object (user).
     *
     * @param  string $number
     *
     * @return User|null
     *
     * @throws TransformationFailedException if object (user) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $user = $this->om
            ->getRepository('PileUpAppBundle:User')
            ->findOneBy(array('id' => $number))
        ;

        if (null === $user) {
            throw new TransformationFailedException(sprintf(
                'An issue with user, id "%s" does not exist!',
                $number
            ));
        }

        return $user;
    }
}