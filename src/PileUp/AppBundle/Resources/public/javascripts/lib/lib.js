
// an ajax controller to allow targets, effects and outputs

function ajaxTool(){

            // trigger | string | the element that is the trigger for the method e.g. .trigger
            // target | string | the element that will display the resuslts e.g. #target can be null
            // time | int | the time of the required animation
            // ajax | bool | the throttle for ajax calls to prevent bubbling
            // content | string | optional hardcoded content

            this.trigger = null;
            this.target = null;
            this.time = 100;
            this.ajax = false;
            this.content = null;

            // url | string | holder for confirmation urls
            this.url = null;

            // reference to dialog box holder
            this.revealHolder = $('#revealHolder');
        }

        // set the 'class' methods
        ajaxTool.prototype = {

            constructor: ajaxTool,

            setTrigger: function (theTrigger)
            {
                this.trigger = theTrigger;
            },

            setTarget: function (theTarget)
            {
                this.target = theTarget;
            },

            setTime: function (theTime)
            {
              this.time = theTime;
            },

            setContent: function (theContent)
            {
              this.content = theContent;
            },

            // create a fade in and out transition which displays ajax data in the target
            // when the defined trigger is clicked
            fadeClickBind:function ()  {

                //set the scope to an alias to allow reference
                var that = this;

                //
                $(this.trigger).live('click', function(e){

                    e.preventDefault();

                    if(!that.ajax)
                    {
                        $.get($(this).attr('href'), function(data) {

                            $(that.target).fadeOut(that.time, function(){{

                                $(this).empty().html(data).fadeIn(that.time);
                                that.ajax = false;

                                that.initFormAjax();

                            }});

                        });
                    }

                });
            },

            // create a dialog box with content populated via ajax
            revealClickBind: function() {

                //set the scope to an alias to allow reference
                var that = this;

                $(this.trigger).live('click', function(e) {

                    e.preventDefault();

                    var size = $(this).attr('data-size');

                    var modal = that.revealHolder.removeClass('small medium large x-large').addClass(size);

                    $.get($(this).attr('href'), function(data) {
                        modal.empty().html(data)
                            .append('<a class="close-reveal-modal">&#215;</a>')
                            .foundation('reveal','open');

                        that.initFormAjax();

                    });

                });

            },

            // create a dialog box with a confirmation action
            confirmClickBind: function() {

                //set the scope to an alias to allow reference
                var that = this;

                $(this.trigger).live('click', function(e) {

                    e.preventDefault();

                    that.url = $(this).attr('href');

                    var modal = that.revealHolder.removeClass('small medium large x-large').addClass('large');

                    modal.empty().html(that.content)
                        .append('<a class="close-reveal-modal">&#215;</a>')
                        .foundation('reveal','open');
                });

            },

            executeClickBind: function ()
            {
                //set the scope to an alias to allow reference
                var that = this;

                $('a.confirm-execute').live('click', function(e) {

                    e.preventDefault();

                    window.location.href = that.url;
                });
            },

            removeClickBind: function ()
            {
                //set the scope to an alias to allow reference
                var that = this;


                //
                $(this.trigger).live('click', function(e){

                    e.preventDefault();

                    //set the target dynamically as we will not know the tweet id on init
                    that.target = '#'+$(this).attr('data-tweet');

                    if(!that.ajax)
                    {
                        $.get($(this).attr('href'), function(data) {

                            if(data.result)
                            {

                                // check if the remove was on a account or non account listing
                                if(data.type == 'dash')
                                {

                                    $(that.target).fadeOut(that.time, function(){{

                                        $(this).empty();
                                        that.ajax = false;

                                    }});
                                }
                                else
                                {
                                    $(that.target).fadeOut(that.time, function(){{

                                        $(this).empty().html(data.content).fadeIn(that.time);
                                        that.ajax = false;

                                        that.initFormAjax();

                                    }});
                                }

                            }
                            else
                            {
                                that.error.empty().html(data.content);
                                that.ajax = false;
                            }

                        });
                    }

                });
            },

            initFormAjax: function()
            {
                $.each($('.form-ajax'), function(){
                    var uploader = new ajaxFormTool(this);
                    uploader.init();
                });
            }

}



// Ajax form posting / image uploading

// tar | string / jquery object | identifier for the target object

function ajaxFormTool(tar){

        // input | string | element that holds the image upload field
        // location | string | absolute url for file upload script
        // form | string | the form targeted and wrapped in a jquery selector

        this.form = $(tar);
        this.input = this.form.find('input');
        this.location = this.form.attr('action');
        this.response = this.form.parent();
        this.error = this.form.find('.error');

    }

    // set the 'class' methods
    ajaxFormTool.prototype = {

        constructor: ajaxFormTool,


        init: function()
        {

            //set the scope to an alias to allow reference
            var that = this;

            // use the jquery form ajax plugin to serialise the form post
            this.form.unbind('submit').submit(function(evt){

                var options = {
                    dataType:  'json',
                    success:
                        function(data){

                            if(data.result)
                                that.response.empty().html(data.content);
                            else
                                that.error.empty().html(data.content);

                    }
                };

                $(this).ajaxSubmit(options);

                return false;

            });


        },

        setLocation: function(loc)
        {
            this.location = loc;
        }


}
