$(document).ready(function(){

    //init foundation js
    $(document).foundation();


    //lib object init

    //init and bind the schedule pagination functionality
    var schedulePag = new ajaxTool();
    schedulePag.setTrigger('a.schedule-pag');
    schedulePag.setTarget('#schedule');
    schedulePag.fadeClickBind();

    //init and bind the calendar pagination functionality
    var calendarPag = new ajaxTool();
    calendarPag.setTrigger('a.cal-pag');
    calendarPag.setTarget('#calendar');
    calendarPag.fadeClickBind();

    var archivePag = new ajaxTool();
    archivePag.setTrigger('#archive-holder .pagination a');
    archivePag.setTarget('#archive');
    archivePag.fadeClickBind();

    var dashPag = new ajaxTool();
    dashPag.setTrigger('#dashboard-tweets .pagination a');
    dashPag.setTarget('#dashboard-tweets-holder');
    dashPag.fadeClickBind();

    //init and bind the reveal functionality
    var reveal = new ajaxTool();
    reveal.setTrigger('a.reveal');
    reveal.revealClickBind();

    //init and bind the confirmation functionality
    var confirm = new ajaxTool();
    confirm.setContent('Are you sure? All Tweets and Slots for this ' +
        'account will be deleted.' +
        '<a href="#" title="confirm" class="medium button confirm-execute">Confirm</a>')
    confirm.setTrigger('a.confirm');
    confirm.confirmClickBind();
    confirm.executeClickBind();

    var slotConfirm = new ajaxTool();
    slotConfirm.setContent('Are you sure? If you have a tweet schedule for this slot then you will not be able to remove the slot.' +
        '<a href="#" title="confirm" class="medium button confirm-execute">Confirm</a>')
    slotConfirm.setTrigger('a.confirm-slot');
    slotConfirm.confirmClickBind();
    slotConfirm.executeClickBind();

    var remove = new ajaxTool();
    remove.setTrigger('a.remove');
    remove.removeClickBind();


    // add the ajax form init
    $.each($('.form-ajax'), function(){
        var uploader = new ajaxFormTool(this);
        uploader.init();
    });







    // standard functions

    //toggle checker
    var toggle = true;

    $('a.view-controls').bind('click', function(e){

        e.preventDefault();

        var controlPanel = $('#specific-controls');

        if(toggle)
            controlPanel.removeClass('control-panel-hide');
        else
            controlPanel.addClass('control-panel-hide');

        toggle = !toggle;

    });

    $('a.retweet-control').live('click', function(e){

        e.preventDefault();

        var retweet = $(this).attr('data-ref');

        $('#RetweetForm_retweet_archive_id').attr('value', retweet);
        $('#Retweet_form').submit();


    });

    //upload button
    $('a.upload-btn').live('click', function(e){

        e.preventDefault();

        var tar = $(this).attr('data-ref');

        // click the file uplaod button
        $('.'+tar+' input').click();

        // check for a selected file
        $('.'+tar+' input').change(function(){

            var filename = this.value.split(/(\\|\/)/g).pop();

            $('.uploaded-'+tar).html('Uploaded file: '+filename);

        });

    });

    //keyboard binds for schedule tweets
    $('.tweet-content-input').live('keypress', function(e){

        var tar = $(this).attr('data-ref');

        if(e.which === 13 && this.value.replace(/^\s*|\s*$/g,'') != '' && event.ctrlKey)
        {
            e.preventDefault();

            $('#'+tar).click();
        }

    });

    // preview image rollover
    $('.image-preview').live('click', function(e){

        var modal = $('#revealHolder').removeClass('small medium large x-large').addClass('large');

        modal.empty().html('<div class="big-image-preview"><img src="'+this.src+'"/></div>')
            .append('<a class="close-reveal-modal">&#215;</a>')
            .foundation('reveal','open');

    })

});

