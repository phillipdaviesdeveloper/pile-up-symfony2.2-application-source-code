PileUp - App bundle
========================

The pile up app bundle controls the user authentication process as well as the user entity
and any non authenticated routes / views.

The AppBundle extends the FOSuserBundle to handle authentication (which can be found in ../vendor/friendsofsymfony/user-bundle),
although the actual views for login and registration (along with the base template) can be found in ../app/Resources/views/.

This is in line with the recommended best practive for overriding vendor templates and functionality as documented here:
 http://symfony.com/doc/current/cookbook/bundles/override.html.

Controllers
-----------

Currently the app bundle contains only the default controller. Its sole role is to display the application index page as all other
functionality is controlled by the FOSuserBundle.

Entity
------

The user entity extends the FOS user bundle user entity and allows for extension specific to your application.

Security
-------

The login / security of the application is controlled in the YML format and can be found in  ../app/config/security.yml.

Routing
-------

The routes for the login, registration and other pages are overridden in the AppBundle and can be found in:
config/FOSrouting along with the homepage routing.

Fixtures
--------

The AppBundle contains the data fixture to generate a test user account and allow login to the application

Public assets
-------------

All public assets such as the site javascript, sass, outputted css and other frontend items are stored in this bundle.
The assets are symlinked to the web directory assets folder and linked in the base template located in the app folder.

Configuration assets
--------------------

Although not strictly part of the bundle as the AppBundle is the ceneral point of the application notes about the application
configuration have been added here:

    SASS configuration is stored in the config.rb file in the application root, this is configured to work with the current
    application layout.

    The general application requirements are also contained in the application root in the composer.json file.



