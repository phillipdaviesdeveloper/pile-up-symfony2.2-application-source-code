<?php

namespace PileUp\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Handles the initial pre-authenticated application responses.
 *
 * Includes index display as well as any other non auth required pages.
 * Essentially, this will be where all display methods for non authenticated users
 * will be stored including:
 *
 * * Index page
 * * Register form
 *
 * This list should be extended as the application grows (e.g. Privacy Policy, Terms)
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */

class DefaultController extends Controller
{

    /**
     * Display the index page of the application
     *
     * If the user is not authenticated in the system then the public application
     * landing page is displayed, otherwise the authenticated user interface is displayed.
     *
     * @access public
     *
     * @return Render Displays the index HTML built from the ::base.html.twig file
     */
    public function indexAction()
    {
        return $this->render('::base.html.twig');
    }

}
