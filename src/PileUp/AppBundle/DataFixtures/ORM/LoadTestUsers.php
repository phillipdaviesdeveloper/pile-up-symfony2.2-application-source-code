<?php
// src/PileUp/AppBundle/DataFixtures/ORM/LoadTestUsers.php

namespace PileUp\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * Account fixture
 *
 * Generates the initial user data for testing, this will generate a single account
 * which can then be used as the test account, to generate fixures cd to the application
 * directory in terminal and run: php app/console doctrine:fixtures:load
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{


    /**
     * Holds the service container to allow access to services such as user creation(in this instance)
     * @var ContainerInterface
     */
    private $container;


    /**
     * {@inheritDoc}
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface | null $container
     *
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    /**
     * {@inheritDoc}
     *
     * @param \Doctrine\Common\Persistence\ObjectManager | null $manager
     *
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        //make some users
        $user = $userManager->createUser();
        $user->setUsername('ilikegirlsdaily');
        $user->setEmail('test@test.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $userManager->updateUser($user);

        $this->addReference('user', $user);

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

}