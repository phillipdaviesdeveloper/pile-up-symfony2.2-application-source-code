<?php

namespace PileUp\AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="app_user")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="PileUp\TwitterBundle\Entity\TwitterAccount", mappedBy="account_id")
     *
     */
    protected $twitterAccounts;

    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Schedule", mappedBy="account_id")
     * 
     */
    protected $twitterTweets;


    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\ScheduleArchive", mappedBy="archive_account_id")
     *
     */
    protected $archiveTwitterTweets;


    /**
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Slot", mappedBy="slot_account_id")
     *
     */
    protected $slotAccount;


    
    public function __construct()
    {

        parent::__construct();

        $this->isActive = true;
        $this->salt = md5(uniqid(null, true));

        $this->twitterAccounts = new ArrayCollection();
        $this->twitterTweets = new ArrayCollection();
        $this->archiveTwitterTweets = new ArrayCollection();
        $this->slotAccount = new ArrayCollection();

    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add twitterAccounts
     *
     * @param \PileUp\TwitterBundle\Entity\TwitterAccount $twitterAccounts
     * @return User
     */
    public function addTwitterAccount(\PileUp\TwitterBundle\Entity\TwitterAccount $twitterAccounts)
    {
        $this->twitterAccounts[] = $twitterAccounts;

        return $this;
    }

    /**
     * Remove twitterAccounts
     *
     * @param \PileUp\TwitterBundle\Entity\TwitterAccount $twitterAccounts
     */
    public function removeTwitterAccount(\PileUp\TwitterBundle\Entity\TwitterAccount $twitterAccounts)
    {
        $this->twitterAccounts->removeElement($twitterAccounts);
    }

    /**
     * Get twitterAccounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTwitterAccounts()
    {
        return $this->twitterAccounts;
    }

    /**
     * Add twitterTweets
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $twitterTweets
     * @return User
     */
    public function addTwitterTweet(\PileUp\ScheduleBundle\Entity\Schedule $twitterTweets)
    {
        $this->twitterTweets[] = $twitterTweets;

        return $this;
    }

    /**
     * Remove twitterTweets
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $twitterTweets
     */
    public function removeTwitterTweet(\PileUp\ScheduleBundle\Entity\Schedule $twitterTweets)
    {
        $this->twitterTweets->removeElement($twitterTweets);
    }

    /**
     * Get twitterTweets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTwitterTweets()
    {
        return $this->twitterTweets;
    }

    /**
     * Add twitterTweetsArchive
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterTweetsArchive
     * @return User
     */
    public function addTwitterTweetsArchive(\PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterTweetsArchive)
    {
        $this->twitterTweetsArchive[] = $twitterTweetsArchive;

        return $this;
    }

    /**
     * Remove twitterTweetsArchive
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterTweetsArchive
     */
    public function removeTwitterTweetsArchive(\PileUp\ScheduleBundle\Entity\ScheduleArchive $twitterTweetsArchive)
    {
        $this->twitterTweetsArchive->removeElement($twitterTweetsArchive);
    }

    /**
     * Get twitterTweetsArchive
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTwitterTweetsArchive()
    {
        return $this->twitterTweetsArchive;
    }

    /**
     * Add archiveTwitterTweets
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $archiveTwitterTweets
     * @return User
     */
    public function addArchiveTwitterTweet(\PileUp\ScheduleBundle\Entity\ScheduleArchive $archiveTwitterTweets)
    {
        $this->archiveTwitterTweets[] = $archiveTwitterTweets;

        return $this;
    }

    /**
     * Remove archiveTwitterTweets
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $archiveTwitterTweets
     */
    public function removeArchiveTwitterTweet(\PileUp\ScheduleBundle\Entity\ScheduleArchive $archiveTwitterTweets)
    {
        $this->archiveTwitterTweets->removeElement($archiveTwitterTweets);
    }

    /**
     * Get archiveTwitterTweets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArchiveTwitterTweets()
    {
        return $this->archiveTwitterTweets;
    }


    /**
     * Add slotAccount
     *
     * @param \PileUp\ScheduleBundle\Entity\Slot $slotAccount
     * @return User
     */
    public function addSlotAccount(\PileUp\ScheduleBundle\Entity\Slot $slotAccount)
    {
        $this->slotAccount[] = $slotAccount;

        return $this;
    }

    /**
     * Remove slotAccount
     *
     * @param \PileUp\ScheduleBundle\Entity\Slot $slotAccount
     */
    public function removeSlotAccount(\PileUp\ScheduleBundle\Entity\Slot $slotAccount)
    {
        $this->slotAccount->removeElement($slotAccount);
    }

    /**
     * Get slotAccount
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSlotAccount()
    {
        return $this->slotAccount;
    }
}
