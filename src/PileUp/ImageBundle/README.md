PileUp - Image bundle
========================

The image bundle handles all the image uploading in the application. Currently it is only able to recieve and validate one
image at a time although in the future the module could be extended to all for multiple images or even FTP functionality.

Controllers
-----------

The image bundle does bnot contain any controllers as all functionality is currently service based and utilised by other bundles

Entity
------

The image bundle contains a single entity called TwitterImage. This entity contains annotations to control methods for
uploading pre persistence and removing temporary image files.

Annotations are used to validate the uploaded file and ensure its filesize and mime type is allowed before processing.


Forms
-----

The image bundle contains the imageUploadForm field which is defined in the services config to be used in any forms that
require an image uploader.

Currently the upload field is used in ScheduleBundle/Form/Type/ScheduleForm which is the base form for all tweet creation.


Services
--------

The image bundle has two services, the upload service which is can be found in /DependencyInjection along with the form field
mentioned above (which can be found in /Form/Type/).

The upload service simply handles the persistence of any instances of the TwitterImage object as all uploading and validation is
controlled by the TwitterImage Entity.



Fixtures
------

The image bundle contains a fixture to generate a single image for testing.

Routing
-------

As there is no direct url access to the bundle functionality there is no routing configured in the bundle.


Public assets
-------------

All images uploaded to the bundle are symlinked to the web directory (/web/bundles/pileupimage/uploads/twitter) which
contains a htaccess file in the parent directory to disallow access to any files other than image files.




