<?php

namespace PileUp\ImageBundle\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use PileUp\ImageBundle\Entity\TwitterImage;


/**
 * Service to persist and Remove the twitter image entity
 *
 * This controller is defined as a service in the services config and does not return any rendered response objects.
 *
 * All upload and route generation is managed by the Entity as per best practice (see: http://symfony.com/doc/current/cookbook/doctrine/file_uploads.html).
 *
 * For more information on the avaialble methods of the TwitterImage Enity please see the TwitterImage documentation as
 * tasks are performed using lifeCycle callbacks that used for validation in any controller that is required to upload
 * or remove an instance of the Entity.
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */
class Upload extends Controller
{

    /**
     * Persistence method to upload and store image references
     *
     * Will attempt to upload, validate and store an image via the preperssit lifecycle callbacks. On success the TwitterImage
     * object will be returned.
     *
     *  If an exception is thrown either by the entity manager or the Entity itself then the method will return null for error
     *  checking in the client code. This is scheduled for improvement and correct error reporting.
     *
     * @param \PileUp\ImageBundle\Entity\TwitterImage $Twitterimage accepts an object of the TwitterImage type for persistence
     * @return \PileUp\ImageBundle\Entity\TwitterImage $Twitterimage|null
     */
    public function UploadAction(TwitterImage $Twitterimage)
    {

       try{

            $em = $this->getDoctrine()->getManager();

            $em->persist($Twitterimage);
            $em->flush();

            return $Twitterimage;
        }
        catch(\Exception $e)
        {
            //todo-Phill - add actual error message reporting from the exception
            return null;
        }
    }



    /**
     * Removal method to remove image from persistence.
     *
     *  Will attempt to remove the provided object from the persistence layer.
     *
     *  If an exception is thrown either by the entity manager or the Entity itself then the method will return null for error
     *  checking in the client code. This is scheduled for improvement and correct error reporting.
     *
     * @param \PileUp\ImageBundle\Entity\TwitterImage $Twitterimage accepts an object of the TwitterImage type for removal
     * @return bool true|null
     *
     */
    public function removeAction(TwitterImage $Twitterimage)
    {
        try{

            $em = $this->getDoctrine()->getManager();

            $em->remove($Twitterimage);
            $em->flush();

            return true;
        }
        catch(\Exception $e)
        {
            //todo-Phill - add actual error message reporting from the exception
            return null;
        }
    }


}
