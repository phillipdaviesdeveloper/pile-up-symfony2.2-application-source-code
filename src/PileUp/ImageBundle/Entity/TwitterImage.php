<?php


namespace PileUp\ImageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="app_schedule_images")
 * @ORM\HasLifecycleCallbacks
 *
 */
class TwitterImage
{

    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private  $id;


    /**

     *
     * @Assert\File(
     *  maxSize="3145728",
     *  mimeTypes={"image/jpeg","image/gif","image/png"}
     * )
     */
    private $twitterImage;


    /**
    * @ORM\Column(type="string", length=255, nullable=false)
    */
    private $path;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $type;

    /**
     *
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\Schedule", mappedBy="twitterImage")
     *
     */
    private $twitterTweet;


    /**
     *
     * @ORM\OneToMany(targetEntity="PileUp\ScheduleBundle\Entity\ScheduleArchive", mappedBy="archive_twitterImage")
     *
     */
    private $archive_twitterTweet;



    public function __construct()
    {

        $this->twitterTweet = new ArrayCollection();

    }



    /** Uploading / property generation from the upload file */

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getTotalAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path.'.'.$this->type;
    }

    public function getDevTotalAbsolutePath()
    {
        return null === $this->path
            ? null
            : 'http://buffer-2.com/'.$this->getUploadDir().'/'.$this->path.'.'.$this->type;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : '/'.$this->getUploadDir().'/'.$this->path;
    }

    public function getTotalWebPath()
    {
        return null === $this->path
            ? null
            : '/'.$this->getUploadDir().'/'.$this->path.'.'.$this->type;
    }

    public function getPlainPath()
    {
        return '/'.$this->getUploadDir().'/';
    }


    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }



    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads';
    }

    /**
     * Sets twitterImage.
     *
     * @param UploadedFile $file
     */
    public function setTwitterImage(UploadedFile $file = null)
    {
        $this->twitterImage = $file;
    }

    /**
     * Get twitterImage.
     *
     * @return UploadedFile
     */
    public function getTwitterImage()
    {
        return $this->twitterImage;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return TwitterImage
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return TwitterImage
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }


    /**
     * Set type
     *
     * @param string $type
     * @return TwitterImage
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }



    /** Relation methods */


    /**
     * Add twitterTweet
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $twitterTweet
     * @return TwitterImage
     */
    public function addTwitterTweet(\PileUp\ScheduleBundle\Entity\Schedule $twitterTweet)
    {
        $this->twitterTweet[] = $twitterTweet;

        return $this;
    }

    /**
     * Remove twitterTweet
     *
     * @param \PileUp\ScheduleBundle\Entity\Schedule $twitterTweet
     */
    public function removeTwitterTweet(\PileUp\ScheduleBundle\Entity\Schedule $twitterTweet)
    {
        $this->twitterTweet->removeElement($twitterTweet);
    }

    /**
     * Get twitterTweet
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTwitterTweet()
    {
        return $this->twitterTweet;
    }


    /**
     * Add archive_twitterTweet
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $archive_twitterTweet
     * @return TwitterImage
     */
    public function addArchiveTwitterTweet(\PileUp\ScheduleBundle\Entity\ScheduleArchive $archive_twitterTweet)
    {
        $this->archive_twitterTweet[] = $archive_twitterTweet;

        return $this;
    }

    /**
     * Remove archive_twitterTweet
     *
     * @param \PileUp\ScheduleBundle\Entity\ScheduleArchive $archive_twitterTweet
     */
    public function removeArchiveTwitterTweet(\PileUp\ScheduleBundle\Entity\ScheduleArchive $archive_twitterTweet)
    {
        $this->twitterTweet->removeElement($archive_twitterTweet);
    }

    /**
     * Get archive_twitterTweet
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArchiveTwitterTweet()
    {
        return $this->archive_twitterTweet;
    }






    /** Lifecycle callbacks */

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->twitterImage) {

            // set the orginal filename
            $this->fileName = $this->getTwitterImage()->getClientOriginalName();

            // do whatever you want to generate a unique name
            $randFileName = sha1(uniqid(mt_rand(), true));

            $this->path = $randFileName;
            $this->type = $this->getTwitterImage()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getTwitterImage()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getTwitterImage()->move($this->getUploadRootDir(), $this->path.'.'.$this->type);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->twitterImage = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getTotalAbsolutePath()) {
            unlink($file);
        }
    }


}
