<?php
// src/PileUp/ImageBundle/DataFixtures/ORM/LoadTestImages.php

namespace PileUp\ImageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use PileUp\ImageBundle\Entity\TwitterImage;

/**
 *
 * Image fixture
 *
 * Generates the initial image data for testing, this will generate a new image record although the application must have
 * the defined path avaialble in ImageBundle/public/uploads/twitter/images as the data fixture will not upload images itself.
 *
 * To generate fixures cd to the application directory in terminal and run: php app/console doctrine:fixtures:load
 *
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */

class LoadImageData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //make some users
        $image = new twitterImage();

        $image->setPath('da984b5cd9e42b14ce92b0d2cb418abcd181533c');
        $image->setFileName('pile-up.jpg');
        $image->setType('jpeg');

        $manager->persist($image);
        $manager->flush();

        $this->addReference('image', $image);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6;
    }

}