<?php

namespace PileUp\ImageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Image upload field
 *
 * Links field to the TwitterImage Entity when generating forms, defined as a service with the keyword 'image_selector'
 *
 * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
 *
 * @since 0.1
 *
 */

class imageUploadForm extends AbstractType
{

    /**
     * Sets the default options for the field class
     *
     * @param OptionsResolverInterface $resolver Sets the default options for the field, these can be overridden on init but must be defined first
     *
     * @return void
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        //sets the default options for the field type, in this case the field's linked entity
        $resolver->setDefaults(array(
            'data_class' => 'PileUp\ImageBundle\Entity\TwitterImage'
        ));
    }

    /**
     *
     * Generates the form field using the form builder interface
     *
     * @param FormBuilderInterface $builder Passes the reference to the parent builder
     * @param array $options Passes in the options set when the field is initiated in the parent builder
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('twitterImage', 'file', array(
            'required' => false,
            'label' => false
        ));
    }

    /**
     *
     * Sets the 'name' of the field for refernce in the builder->add method e.g.
     *
     *          $builder->add('twitterImage',
     *          'image_selector',
     *               array(
     *                  'label' => 'Upload an image:'
     *              )
     *          );
     *
     * @return string Identifier for the service / parent formbuilder
     */
    public function getName()
    {
        return 'image_selector';
    }

}