<?php

namespace PileUp\ErrorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PileUpErrorBundle extends Bundle
{

    /**
     * Defines the parent bundle to extend
     *
     * @author  Phillip Davies <phillipdaviesdeveloper@gmail.com>
     *
     * @since 0.2
     *
     */
    public function getParent()
    {
        return 'TwigBundle';
    }

}
