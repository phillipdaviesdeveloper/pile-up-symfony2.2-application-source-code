<?php

namespace PileUp\ErrorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PileUpErrorBundle:Default:index.html.twig', array('name' => $name));
    }
}
