set :application, "PileUp"

set :serverName,   "162.13.40.229" # The server's hostname
set :repository,   "/Applications/MAMP/htdocs/buffer-2/buffer-symfony/2.1.x-dev/"

set :domain,      "162.13.40.229"
set :deploy_to,   "/var/www/html/staging.pile-up.com/" # Remote location where the project will be stored

set :scm,         :git
set :deploy_via,  :rsync_with_remote_cache
set :user,        "stagingpileup"

set :model_manager, "doctrine"

# Roles
role :web,        domain
role :app,        domain
role :db,         domain, :primary => true

set  :keep_releases,  3 # The number of releases which will remain on the server
set  :use_sudo,       false

# Update vendors during the deploy
set :use_composer, true
set :update_vendors, true


# Set some paths to be shared between versions
set :shared_files,    ["app/config/parameters.ini"]
set :shared_children, [app_path + "/logs", "vendor"]


set :stages,        %w(production staging)
set :default_stage, "staging"
set :stage_dir,     "app/config"

set :dump_assetic_assets, true

set :writable_dirs,       ["app/cache", "app/logs", "web/uploads"]
set :webserver_user,      "apache"
set :permission_method,   :acl
set :use_set_permissions, true


after "deploy:restart", "deploy:cleanup"

logger.level = Logger::MAX_LEVEL

require 'capistrano/ext/multistage'


