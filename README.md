PileUp! - Twitter scheduling client - intro docs
================================================

Welcome to the PileUp! git repository!
--------------------------------------

Pileup is a fully documented tweet scheduling tool built in symfony 2.2 using zurb foundation 4.4, zepto and SASS/compass.

PileUp makes use of the following symfony bundles in addition to the basic symfony install:

*"themattharris/tmhoauth": "0.8.*",

*"friendsofsymfony/user-bundle": "~2.0@dev",

*"knplabs/knp-components": ">=1.1",
*"knplabs/knp-paginator-bundle": "dev-master"

*"doctrine/doctrine-fixtures-bundle": "dev-master",
*"doctrine/doctrine-migrations-bundle": "dev-master",

All external bundles are controlled by composer as per best practice and configuration can be found in the composer.json
file contained in the site root.

PileUp's application bundles are located in the src directory and each bundle contains a documentation README.md file
which details the bundle functionality.

All classes where possible contain detailed DocBlocks and the generated documentation is avaialble in the /docs folder.

Deployment is handled by capifony (capifony.org) and the deplyment configuration can be found in /app/config/deploy.rb
with the separate environment configuration contained in staging.rb and production.rb. Each deployment is tied to
the specific branches in the git repository (master for production, development for staging) to prevent corruption.

Configuring the app locally
---------------------------

1 Create a location in your localhost to clone the repository


2 Check you have the following installed: php 5.4+, ACL enabled, php intl module installed and configured

    > note: On MAMP Pro you can enable ACL in the main MAMP window under server -> php -> server extensions


3 Clone the repository and check you are in the development branch


4 Run composer.phar (or composer if alias is configured) update to make sure everything is up to date


5 Set the required permissions on apache writeable folders

    > note: this can be a nightmare, try the documented version here first:
    >
    > $ rm -rf app/cache/*
    > $ rm -rf app/logs/*
    >
    > $ APACHEUSER=`ps aux | grep -E '[a]pache|[h]ttpd' | grep -v root | head -1 | cut -d\  -f1`
    > $ sudo setfacl -R -m u:$APACHEUSER:rwX -m u:`whoami`:rwX app/cache app/logs
    > $ sudo setfacl -dR -m u:$APACHEUSER:rwX -m u:`whoami`:rwX app/cache app/logs
    >
    > If this fails try the other methods noted here: [Docs](http://symfony.com/doc/current/book/installation.html)
    >
    > If all else fails, simply delete the cache directories from /app/cache

6 Create your database user and table, amend the connection details in /app/config/parameters.yml


7 In terminal, cd to the 2.1.x-dev directory, then run the following to add the default user / data

    php app/console doctrine:fixures:load


8 Finally create a virtual host to point a url of your choice to the /web directory

    > note: with MAMP pro this is super easy, open the main window, click hosts then on the bottom left click the
    > little plus symbol. Add your server name e.g. pileup.com and then select the web directory for your app!


9 Give it a test!


Configuring the app on remote server
------------------------------------

For the production server I have used a rackspace cloud server, you should be able to use a shared host but you must make
sure you have SSH access as well as the appropriate server requirements (acl, php version etc)

As the application is deployed with capifony the process is quite simple, however there are a few pitfalls you should watch out for:

* SSH shell memory limits, as the deploy script uses composer to update all the vendor packages, depending on the size of
the packages this can mean your shell script will run out of memory. Currently in the alpha version a limit of 128MB on the
shell script should allow you to complete the deplyment. To do this you may need to contact your host support team.

* Access to the app when you do not have a domain setup. To view your app you will need to do the following things:

    * Create a virtual host on the server that points to your applications web folder, it will look something like this:

            in /etc/httpd/vhost.d/*your domain name.conf*

            <VirtualHost *:80>
            ServerName staging.pile-up.com
            ServerAlias www.staging.pile-up.com
            DocumentRoot /var/www/html/staging.pile-up.com/current/web
            <Directory /var/www/html/staging.pile-up.com/current/web>
                    Options -Indexes FollowSymLinks -MultiViews
                    AllowOverride All
            </Directory>

            CustomLog /var/log/httpd/staging.pile-up.com-access.log combined
            ErrorLog /var/log/httpd/staging.pile-up.com-error.log

            # Possible values include: debug, info, notice, warn, error, crit,
            # alert, emerg.
            LogLevel warn
            </VirtualHost>


    * Add the server ip and the domain to your hosts file on your machine


            run this line in terminal:
            $ sudo nano /private/etc/hosts

            add this line:
            *server ip address* *your domain*

            and save the file

    * check the domain in your browser, it should now point to your server and the web folder of your app!

    tip: you might need to restart apache on your server for the configuration to take effect.


* File permission issues. You may find that your application fails as it is unable to write to a directory, or clear a cache
 during deployment. This is because the ssh user is different to the apache user. To resolve the issue the deploy file tries
 to set the writeable folders (logs, cache, uploads) to the apache user permissions. This can be different on every server
 (although is generally www-data) to find out your apache user, ssh to your server as root (or your supplied login) and
 follow the instructions here: [linky](http://www.cyberciti.biz/faq/unix-osx-linux-find-apache-user/).

* Production log write issues. As your app is running in production mode you will only see the error messages and not the
 exception profiler as in development mode. Any 500 errors will be written to app/log in your shared directory, which is
 useful for debuging production only issues. This does however cause an issue with deployment and you wil need to manually
 delete your app/logs/prod.log file before deployment.

* Database setup. You will need to add the new database / table details to your parameters.yml file before generating all
 the database data. To do this modify the app/config/parameters.yml file in the development branch and commit the changes
 then tell git to ignore any further to the file with the following command:

    git update-index --assume-unchanged *path to the file + the file name*

 You can then safely add back in your local database settings. if you ever need to add the file back into git with the following:

    git update-index --no-assume-unchanged *path to the file + the file name*


To deploy the app you will just need to modify the app/config/staging.rb and production.rb files to point to the correct
folders on your live server, modify the deploy.rb to point to your local git repo for the app and then run the following in terminal:

    cap setup

Once setup has executed, run:

    cap deploy

This will then copy your local git respository, update all your dependencies, upload to the server, create a revision and clear your cache!

Check the terminal output and resolve any errors displayed, you may find other issues than the ones listed above.


Notes on capifony:

* You should have seperate SSH users for production and staging
* Your ssh users should be added to the apache user group, this should resolve the issues with most file permissions

** There is acutually a bug logged here: https://github.com/everzet/capifony/issues/409 which highlights the permission issues, as a work around for now follow the above steps
and be sure to manually move any files from one release to the next.



Finally, log into your server via ssh and cd to your application root.

Once in the root you can check your configuration:

       php app/check.php

Load the database

    php app/console doctrine:fixtures:load

configure the cron job:

    crontab -l *lists the current cronjobs*

    crontab -e *opens the cron list for editing*

    press i for insert

    *add your cron e.g.*
    */5 * * * * /Applications/MAMP/bin/php/php5.4.10/bin/php /Applications/MAMP/htdocs/buffer-2/buffer-symfony/2.1.x-dev/app/console twitter:run --env=prod

    write out your file to install the new cron

tip: the cron should be set to the production evironement

Once completed you application should be ready to go.

Final Note: The application uses migrations, so after any changes to the application entities you must use cap deploy:migrations to add the database migration
to the live server database.




Symfony Standard Edition - Generic docs
=======================================

Welcome to the Symfony Standard Edition - a fully-functional Symfony2
application that you can use as the skeleton for your new applications.

This document contains information on how to download, install, and start
using Symfony. For a more detailed explanation, see the [Installation][1]
chapter of the Symfony Documentation.

1) Installing the Standard Edition
----------------------------------

When it comes to installing the Symfony Standard Edition, you have the
following options.

### Use Composer (*recommended*)

As Symfony uses [Composer][2] to manage its dependencies, the recommended way
to create a new project is to use it.

If you don't have Composer yet, download it following the instructions on
http://getcomposer.org/ or just run the following command:

    curl -s http://getcomposer.org/installer | php

Then, use the `create-project` command to generate a new Symfony application:

    php composer.phar create-project symfony/framework-standard-edition path/to/install

Composer will install Symfony and all its dependencies under the
`path/to/install` directory.

### Download an Archive File

To quickly test Symfony, you can also download an [archive][3] of the Standard
Edition and unpack it somewhere under your web server root directory.

If you downloaded an archive "without vendors", you also need to install all
the necessary dependencies. Download composer (see above) and run the
following command:

    php composer.phar install

2) Checking your System Configuration
-------------------------------------

Before starting coding, make sure that your local system is properly
configured for Symfony.

Execute the `check.php` script from the command line:

    php app/check.php

Access the `config.php` script from a browser:

    http://localhost/path/to/symfony/app/web/config.php

If you get any warnings or recommendations, fix them before moving on.

3) Browsing the Demo Application
--------------------------------

Congratulations! You're now ready to use Symfony.

From the `config.php` page, click the "Bypass configuration and go to the
Welcome page" link to load up your first Symfony page.

You can also use a web-based configurator by clicking on the "Configure your
Symfony Application online" link of the `config.php` page.

To see a real-live Symfony page in action, access the following page:

    web/app_dev.php/demo/hello/Fabien

4) Getting started with Symfony
-------------------------------

This distribution is meant to be the starting point for your Symfony
applications, but it also contains some sample code that you can learn from
and play with.

A great way to start learning Symfony is via the [Quick Tour][4], which will
take you through all the basic features of Symfony2.

Once you're feeling good, you can move onto reading the official
[Symfony2 book][5].

A default bundle, `AcmeDemoBundle`, shows you Symfony2 in action. After
playing with it, you can remove it by following these steps:

  * delete the `src/Acme` directory;

  * remove the routing entries referencing AcmeBundle in
    `app/config/routing_dev.yml`;

  * remove the AcmeBundle from the registered bundles in `app/AppKernel.php`;

  * remove the `web/bundles/acmedemo` directory;

  * remove the `security.providers`, `security.firewalls.login` and
    `security.firewalls.secured_area` entries in the `security.yml` file or
    tweak the security configuration to fit your needs.

What's inside?
---------------

The Symfony Standard Edition is configured with the following defaults:

  * Twig is the only configured template engine;

  * Doctrine ORM/DBAL is configured;

  * Swiftmailer is configured;

  * Annotations for everything are enabled.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * [**AsseticBundle**][12] - Adds support for Assetic, an asset processing
    library

  * [**JMSSecurityExtraBundle**][13] - Allows security to be added via
    annotations

  * [**JMSDiExtraBundle**][14] - Adds more powerful dependency injection
    features

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][15] (in dev/test env) - Adds code generation
    capabilities

  * **AcmeDemoBundle** (in dev/test env) - A demo bundle with some example
    code

Enjoy!

[1]:  http://symfony.com/doc/2.1/book/installation.html
[2]:  http://getcomposer.org/
[3]:  http://symfony.com/download
[4]:  http://symfony.com/doc/2.1/quick_tour/the_big_picture.html
[5]:  http://symfony.com/doc/2.1/index.html
[6]:  http://symfony.com/doc/2.1/bundles/SensioFrameworkExtraBundle/index.html
[7]:  http://symfony.com/doc/2.1/book/doctrine.html
[8]:  http://symfony.com/doc/2.1/book/templating.html
[9]:  http://symfony.com/doc/2.1/book/security.html
[10]: http://symfony.com/doc/2.1/cookbook/email.html
[11]: http://symfony.com/doc/2.1/cookbook/logging/monolog.html
[12]: http://symfony.com/doc/2.1/cookbook/assetic/asset_management.html
[13]: http://jmsyst.com/bundles/JMSSecurityExtraBundle/master
[14]: http://jmsyst.com/bundles/JMSDiExtraBundle/master
[15]: http://symfony.com/doc/2.1/bundles/SensioGeneratorBundle/index.html
